from distutils.core import setup
setup(name='pygdh',
      version='VERSION',
      description='Grid Discretization Helper',
      author='Kenneth Higa',
      author_email='KHiga@lbl.gov',
      license='BSD-3-Clause-LBNL',
      platforms='All',
      requires=['numpy','scipy'],
      packages=['pygdh'])

