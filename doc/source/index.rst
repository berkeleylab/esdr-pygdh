PyGDH User's Guide
==================

.. toctree::
   :maxdepth: 2

   introduction
   installation
   tutorial
   templates
   citing
   acknowledgements
   copyright_notice
   license
