***************************
Specifying numerical output
***************************

This tutorial has used only GNUPLOT-formatted output, specified by including the ``'GNUPLOT'`` string in the ``output_types`` argument to ``solve_time_independent_system`` and ``solve_time_dependent_system``. However, other output formats are available, and output files can produced in multiple formats simultaneously--this is why the ``output_types`` argument is a list. Currently, the other supported output types are 'CSV' (comma-separated values), 'PICKLE' (a Python-specific format) and 'HDF5' (`Hierarchical Data Format <http://www.hdfgroup.org/>`_, version 5).

The human-readable text file formats (currently 'GNUPLOT' and 'CSV') are suitable for relatively simple problems, and CSV is particularly suitable for use with spreadsheet applications.

The 'PICKLE' and 'HDF5' formats are not human-readable; they store solutions for later access by PyGDH. In particular, this is useful for saving solutions from which simulations can be restarted, and for doing sophisticated postprocessing of numerical solutions. These will be discussed in the upcoming chapter on postprocessing_.

.. _postprocessing: postprocessing.html

Output file names
=================
If a single ``Grid`` object is used in a program, the output file names are have the root specified by the ``filename_root`` argument to ``solve_time_independent_system`` and ``solve_time_dependent_system``, followed by the descriptive ``Grid`` label, and an extension determined by the file format (``.gnuplot`` for ``'GNUPLOT'``, ``.csv`` for ``'CSV'``, ``.pkl`` for ``PICKLE``, and ``.h5`` for ``'HDF5'``).

GNUPLOT format
==============
In the ``'GNUPLOT'`` format, the results for a single timestep occupy contiguous non-blank lines. Results for separate timesteps are separated by two blank lines, so that the ``index`` keyword in a GNUPLOT ``plot`` or ``splot`` statement may be used to select results from specific timesteps. The first timestep in the file, associated with the GNUPLOT index ``0``, reflects the initial condition.

Each non-blank line is either a comment, beginning with ``#``, or consists of a sequence of numbers, separated by spaces. For a ``Grid`` with more than one ``Volume``, the lines of data begin with the ``coordinate`` value(s) of a ``Volume`` in the ``Grid``, and are followed by the solution field values in the order given by the ``field_names`` parameter given at the ``Grid`` object creation. These are followed by additional output field values specified by the user. For a two-dimensional ``Grid``, single blank lines are used to separate data along separate grid lines so that GNUPLOT recognizes the grid layout as such.

For a ``Grid`` with a single ``Volume``, the lines of data consist of the timestep number followed by the simulation time, and then the solution values in the same order as in the previous case. No blank lines are left between results from successive timesteps.

Comment lines are included, and indicate the meanings of the various columns, along with the timestep index and simulation time for time-dependent problems.

CSV format
==========
There is no standard "comma-separated values" format, but PyGDH makes use of the ``csv`` module in the standard Python library. The output files appear to work well with common spreadsheet applications. This format is very similar to the GNUPLOT format, except that numbers are separated by commas, blank lines are not placed between successive grid lines in the two-dimensional case, and only a single separator line is left between results for different timesteps (for ``Grid`` objects with more than one ``Volume``). The separator line contains the timestep number and simulation time in the first two columns; other values occupy the third column onward.

HDF format
==========
The Hierarchical Data Format allows multiple datasets to be stored in a single file much as files are stored on a hard disk. HDF5 output requires installation of the HDF5 library, and the h5py Python interface to the HDF5 library.

HDF5 formatted files are named by appending the extension 'h5' to the ``filename_root`` parameter to ``solve_time_independent_system`` and ``solve_time_dependent_system``.

The HDF5 library and format offer many advantages over the simple text file output formats. However, the use of these files will be transparent to most users of PyGDH, as long as PyGDH is used to store and retrieve results from these files. Also, since exact numerical results are stored in this format, as opposed to the text file formats in which human-readable representations are stored, PyGDH offers the option of continuing a time-dependent simulation from results associated with any timestep in an HDF5 file.

The HDF5 files are not in human-readable form. There are a number of utilities that one may use to extract data from an HDF5 file, but as PyGDH is capable of handling storage and retrieval of its results, most users will not need to use additional tools.

PICKLE format
=============
The PICKLE format is a Python-specific format, and like the HDF5 output option, stores data in a way that is convenient for further processing. However, it is much slower at certain operations, and is platform-specific. This format is meant to provide the ability for sophisticated postprocessing when a user is unable to use the HDF5 library. From the perspective of a PyGDH user, PICKLE files are used by PyGDH exactly as HDF5 files are used, but HDF5 files are access preferentially if both are available.

HDF5 formatted files are named by appending the extension 'pkl' to the ``filename_root`` parameter to ``solve_time_independent_system`` and ``solve_time_dependent_system``.

Adding additional output fields: an example
===========================================
The solution field values are always saved in the output files, but it may be desirable to have additional output variables. For example, a solid mechanics simulation may use displacement as the solution variable, but stress values, which may be computed from displacement, may also be of interest. Here, we discuss an example program that produces additional output. The program is included in the PyGDH archive as  ``pygdh/examples/output/output.py``.

Consider a one-dimensional system, a spherically-symmetric spherical body experiencing linearly-elastic deformation. The solution variable will be the displacement :math:`u`.

Since this system is spherically symmetric, the momentum equation has only one non-zero component, associated with the radial direction. Under the assumption of negligible inertia, this gives an equation for mechanical equilibrium:

.. math::
   \frac{\partial t_r}{\partial r} + \frac{2}{r} (t_r - t_t) = 0

where :math:`t_r` is the normal stress on a radially-oriented surface, and :math:`t_t` is the normal stress on any surface perpendicular to the radial direction. All other stress components are zero. The boundary conditions are

.. math::
   u(0) = 0

   t_r(1) = 0

In order to obtain a discretized form by the finite-volume method, one may integrate over the volume of the sphere contained between radial distances :math:`r_0` and :math:`r_1`. However, since the governing equation is independent of angle, this is equivalent to integrating over radial position only, with an additional factor of :math:`r^2.` But one may multiply the rearranged equation by any factor, so except at :math:`r = 0,` one may simply integrate both sides of the previous equation in :math:`r` to obtain

.. math::
   t_r(r_1) - t_r(r_0) + 2 \left<\frac{t_r - t_t}{r}\right> (r_1 - r_0) = 0

The angle brackets indicate an average over the interval--for simplicity, this will be approximated by the value of the enclosed quantity at the midpoint between :math:`r_0` and :math:`r_1`. No equation will be needed at the origin, because the solution value at the origin will be given.

The stress components are given by

.. math::
   t_r = \lambda (tr \mathbf{E}) + 2 \mu \frac{\partial u}{\partial r}

   t_t = \lambda (tr \mathbf{E}) + 2 \mu \frac{u}{r}

   tr \mathbf{E} = \frac{\partial u}{\partial r} + 2 \frac{u}{r}

   \lambda = \frac{E \nu}{(1+\nu)(1-2\nu)}

   \mu = \frac{E}{2(1+\nu)}

Although :math:`u` will be used as the solution variable, it is clearer to work in terms of the stress components, and it is desirable to include the stress components in the output as well. 

PyGDH requires that additional output variables be arrays with one element for every ``Volume`` of the associated ``Grid`` object. This ensures that any additional output data structures have the same structure as the solution field arrays and will fit neatly into the human-readable text formats.

Declaring global variables
--------------------------
While variables used to hold intermediate values do not necessarily have to be output variables, they should persist throughout the solution process, so that their values can be used and modified by multiple methods. Variables of this sort should be defined in the ``define_field_variables`` method of a ``Grid``-derived object. These methods are called once per ``Grid`` object for an entire simulation, before any numerical solutions are obtained. By defining these methods in ``Grid``-derived classes, these variables are naturally associated with a specific ``Grid``. 

.. The available computer memory is not likely to be a limitation, and in most cases, repeating a calculation takes longer than retrieving earlier results from memory. 

.. so additional data structures will be created in ``define_field_variables`` to store the intermediate values.

..  may be specified.  One option for creating such variables is to define them as members of the object controlling the problem-solving process. 

From the discretized equation, one can see that the interpolated solution field values will be needed at the boundaries of each ``Volume``, and at the centers; however, for consistency with the solution field output, the desired stress component output values correspond to the ``Volume`` ``coordinate`` locations. For ``Volume`` objects on the edges of the domain, the ``coordinate`` locations are associated with the outer boundaries, but for ``Volume`` objects on the interior, the ``coordinate`` locations are associated with the ``Volume`` centers. None of the data structures of intermediate calculations are appropriate, so additional data structures will be created specifically for output. For this example, the ``define_field_variables`` method is written as:

.. include:: ../../pygdh/examples/output/output_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DEFINE FIELD VARIABLES
   :end-before: #### SPHINX SNIPPET END DEFINE FIELD VARIABLES

The first six variables defined above will be used to hold the results of intermediate calculations. The last two variables are used to temporarily store additional output values.

Calculation of global variable values
-------------------------------------
Intermediate calculations can be performed in a ``calculate_global_values`` method (defined in a ``Problem``-derived class) which PyGDH calls before evaluating the discretized equation methods over the problem domain. This is the appropriate place to make calculations and store intermediate results, especially when they are used in the discretized equation methods of more than one ``Volume``. Here, the intermediate values (stress components, in this case) are calculated in a ``calculate_global_values`` method and stored in the newly-defined data structures:

.. include:: ../../pygdh/examples/output/output_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START CALCULATE GLOBAL VALUES
   :end-before: #### SPHINX SNIPPET END CALCULATE GLOBAL VALUES


These methods are called before the governing equation methods on every iteration of the nonlinear solver.

Alternatively, global values may store the results of limited postprocessing performed on numerical solutions. This may be done by defining a method named ``process_solution``, within a ``Problem``-derived class. A method of this name is automatically called once after a solution is obtained (for a given timestep, in a time-dependent problem). In this example, although the stress component values needed for output are a subset of those calculated as intermediate values, the ``calculate_global_values`` does not copy them to the output data structures because this would be done on every iteration of the iterative solver used by PyGDH, even though only the final iteration for a given timestep produces the desired values. Instead, this copying is done in a ``process_solution`` routine, called once after a solution is obtained:

.. include:: ../../pygdh/examples/output/output_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROCESS SOLUTION
   :end-before: #### SPHINX SNIPPET END PROCESS SOLUTION

Declaring additional output variables
-------------------------------------
Additional output fields are specified in the ``output_fields`` member of a ``Grid`` object. This member is a dictionary, and is empty by default (however, the solution field values are automatically written to output). The ``output_fields`` dictionaries may be modified (or overwritten) in a user-defined method named ``set_output_fields``, defined in each ``Grid``-derived class. This method must take only ``self`` as an argument. 

Entries in an ``output_fields`` dictionary must consist of descriptive strings as keys and NumPy arrays as values, and as mentioned in the previous paragraph, the arrays must have one element for every ``Volume`` in the ``Grid``, to ensure consistency with the solution output written into the same files. PyGDH includes all of the specified arrays when writing to the output files after every timestep of a time-dependent process, or after the solution is found for a time-independent problem.

In this example, the last two variables defined in ``define_field_variables`` are marked as output variables. The descriptive keys in the ``output_fields`` dictionary are used to describe the data in output files.

.. include:: ../../pygdh/examples/output/output_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START SET OUTPUT FIELDS
   :end-before: #### SPHINX SNIPPET END SET OUTPUT FIELDS

The ``scipy.optimize.fsolve`` solver used by PyGDH is an iterative solver, which means that the discretized equation methods over the domain are evaluated many times in order to refine the numerical solution. The ``calculate_global_values`` method is called once at the start of each iterative step, and the ``process_solution`` method is called after the final iterative step has been made and an acceptable numerical solution has been obtained. The solver then evaluates the equations one last time to ensure that the error is within the desired tolerance. For this reason, the ``calculate_global_values`` method is called one final time with the numerical solution to be returned, and so calculations that it performs are consistent with the returned solution, and suitable for use as output.

Final comments
--------------
The rest of the program is similar to programs from previous chapters. As in the earlier ``PDE`` example, one of the boundary conditions must be introduce in ``set_boundary_values``, while the other must be introduced through a separate governing equation method for the ``Volume`` on the corresponding boundary.

.. The method corresponding to the discretized equation may now be written as

.. include: : ../../pygdh/examples/output/output_annotated.py
..   :lines: 159-166

.. The displacement at the origin must be zero; the appropriate boundary condition is

.. math: :
..   u(0) = 0

.. The radial stress at the outer surface will be given, so while :math:`u` at the outer boundary is unknown, the discretized equation for the outermost ``Volume`` will be

.. math: :
..   t_r^* - t_r(r_0) + 2 \left<\frac{t_r - t_t}{r}\right> (r_1 - r_0) = 0

.. where :math:`t_r^*` is given. The corresponding method is:

.. include: : ../../pygdh/examples/output/output_annotated.py
..   :lines: 167-174

The additional output fields are associated with the solution output for the same ``Grid`` object. For HDF5 files, this is simply an implementation detail, but for the CSV and GNUPLOT formats, one output file is created for each ``Grid``, and each output field occupies a different column at a given timestep (each row corresponds to a position in the domain, with the ``Volume`` position indicated by the first column, or columns in two dimensions). Since the additional output fields are specified in the ``output_fields`` dictionary, and the order in which entries are placed into dictionaries does not necessarily indicate the order in which they are stored internally, header lines or comment lines are automatically added to the CSV and GNUPLOT text files so that the meaning of each column is clearly defined, even if the order in which they are stored by PyGDH is not evident. The ordering of columns will depend on the Python implementation used. One should always check the first lines of the output files for this information.

As it turns out, the analytical solution is particularly simple--:math:`u` is linear in :math:`r`:

.. math::
   u(r) = -\frac{p}{3 \lambda + 2 \mu} r

where :math:`p` is the pressure at the surface.

This is in agreement with the numerical result, taking :math:`p = \lambda = \mu = 1` for convenience:

.. image:: ../../pygdh/examples/output/output_u.png

As a consequence, the stress components are constants (and identical to each other, as they must be at the origin, due to symmetry), equal in magnitude to the pressure at the surface:

.. image:: ../../pygdh/examples/output/output_stress.png

The GNUPLOT script used to generate these plots is:

.. include:: ../../pygdh/examples/output/output.gpl
   :literal:
		
Suggested exercises
===================
1. Modify the example program to produce a CSV output file as well, and import into a spreadsheet program and produce a plot. Are the results consistent with the GNUPLOT output?

2. Modify the example program to produce HDF5 output file as well, and use a HDF5 browser (for example, hdfview) to look through its contents.
