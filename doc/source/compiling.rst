***************************************
Compiling with Cython to increase speed
***************************************

PyGDH was designed for ease-of-use and flexibility, with little emphasis on program speed, because many fast and powerful, but complex, software packages are already available. However, simple models often grow into more complicated models, so there is sometimes a need to make PyGDH programs run faster.

One important approach to increasing program speed is to compile programs to produce instructions in the computer's native language. The `Cython <http://cython.org>`_ compiler enables this for programs written in a language very similar to the Python language. Typically, a Cython program is first written and tested as a Python program, and then slight modifications are made to provide the Cython compiler with additional information that allows it to produce a faster compiled program.

This additional information might tell the Cython compiler that particular variables will only refer to quantities of a specific type, such as integers, floating point numbers, or NumPy arrays. Hints like these help the compiler to optimize the resulting program.

Overview
========

Like in situations requiring :doc:`postprocessing`, users wanting to use Cython should create one text file (called, for example, ``body.py``) containing most of the Python code that describes the mathematical problem to be solved, and multiple text files that ``import`` this code and do high-level things such as telling PyGDH to test the problem formulation, solve the problem or to retrieve and postprocess previously computed solutions (these might be called ``test.py``, ``solve.py`` and ``postprocess.py``). By using such a strategy, data structures are defined only once in ``body.py`` but can be easily accessed from any of the higher-level Python source code.

During the debugging and testing phase, users will typically compute solutions on relatively small domains in order to rapidly obtain test results. In this phase, users can take advantage of the flexibility of a full Python environment, which allows users to interactively probe the operation of a running program. However, once this phase is complete, users may want to compute solutions on relatively large domains and program speed may become important. It is at this point that one might want to make use of the Cython compiler.

In order to do this, a text file containing Cython source code must be created. Typically, one starts by making a copy of ``body.py`` in our example, and naming the new file with a ``pyx`` extension ( ``body.pyx``), the extension for a Cython source code file. One then makes the desired changes to the new file to provide hints to the Cython compiler and then runs the Cython compiler to produce a compiled library file, which for the purposes of the Python ``import`` statement can be used just as the original ``body.py`` file. In fact, if both are present in the same directory, the compiled library file will be automatically used in preference to the original Python file. Typically, the ``test.py``, ``solve.py`` and ``postprocess.py`` will run without any modifications.

PyGDH itself makes use of Cython; the PyGDH package contains both Python and Cython source code files, and during the default installation, the Cython source files are automatically compiled and installed alongside the Python source files if Cython and a C compiler are available.

An example
==========

All of the examples in this tutorial are very small problems relative to the amount of computing power available to typical users. However, for illustration, we will revisit the problem in :doc:`system` and increase the computational cost by increasing the grid size, decreasing the timestep size, and increasing the number of timesteps beyond what would be desirable in practical terms. We begin by copying the source code file and splitting it into a ``system_solve.py`` and a ``system_body.py``, and then modify ``system_solve.py`` in order to increase the problem size:

.. include:: ../../pygdh/examples/compiling/system_solve_annotated.py
   :code:
      
Note that one must now indicate that the ``System`` class is in the imported ``system_body`` module.

Profiling
---------

Profilers provide a detailed look at program performance, which can be extremely helpful when optimizing programs. It is a well-known programmer's rule of thumb that 90% of program running time is spent executing instructions representing 10% of its source code, so it is in the programmer's interest to spend most of their optimization effort on improving the performance of this critical 10%.

It is also frequently said that premature optimization is the "root of all evil," in part because the importance of any given piece of source code to the overall program running time is only fully known once the entire program is running correctly. So users are advised to ensure that their Python programs are producing correct results before transforming them into Cython programs.

The Python Standard Library provides profiling capabilities. In the present example, ``system_solve.py`` can be modified to find the 15 functions that take the most time:

.. include:: ../../pygdh/examples/compiling/system_profile_annotated.py
   :code:

yielding:

.. include:: ../../pygdh/examples/compiling/system_uncompiled_profile.log
   :literal:
      
The functions toward the top of this listing and which are in files written by the user (particularly ``system_body.py`` here) are the most important to optimize. Unsurprisingly, these are methods that perform mathematical calculations and which are repeatedly called. 

.. before and after producing the compiled library. Comparing the two files will reveal the effect of compilation on performance. One can then experiment with the Cython version of the source code, adding or removing hints to the Cython compiler and rerunning the profiler in order to see the effects of changes on the running times of the corresponding methods.

 
Cython modifications
--------------------

We now introduce Cython-specific program modifications, concentrating on the methods that are the most important to optimize. Typically, the compilation step involves its own debugging and testing phase. More details and deeper insights are available in the Cython `documentation <http://docs.cython.org/index.html>`_. In practice, not all Cython hints improve performance; some worsen performance, which may be a consequence of the small problem size. So alternating between profiling and adding Cython-specific changes is important. 

.. include:: ../../pygdh/examples/compiling/system_body_annotated.pyx
   :code:

Compilation with Cython is discussed in the Cython `documentation <http://docs.cython.org/src/userguide/source_files_and_compilation.html>`_. One method for using the Cython compiler is to use Python's Distutils mechanism. This involves creating another Python program, named ``setup.py`` by convention, that describes the compilation process and here is adapted from the program provided in the Cython documentation:

.. include:: ../../pygdh/examples/compiling/setup_annotated.py
   :code:

This is then run in the command-line environment::

    python setup.py build_ext --inplace

This creates a compiled library file. Rerunning ``system_profile.py``, this compiled library file is automatically used in preference to the ``system_body.py`` file. This produces:

.. include:: ../../pygdh/examples/compiling/system_compiled_profile.log
   :literal:
      
.. The compiled methods are no longer visible to the profiler as separate methods, but the methods that call the compiled methods are still visible and the total running time has decreased.

It should be noted that measured running times depend on the present state of the computer and will therefore vary if measured at different times.

Unfortunately, the improvement in program speed was very small in this example, although compilation can prove a substantial benefit in mathematically-intensive problems, in which the time spent calculating values is large compared to the overhead of controlling program flow.
		    
Maintaining consistency between Python and Cython source code files
-------------------------------------------------------------------

Even mature PyGDH programs, for which Cython versions have already been developed, might see further development. In this case, it is helpful to make changes in only one version of the source code files, rather than duplicating effort and potentially introducing mistakes in the process. Since further development of a model typically involves returning to the debugging and testing phase, it is reasonable to work with the Python version of the source code, but it is desirable to avoid redoing all of the work originally done to create a Cython version for the modified Python program.

This same challenge was encountered in the development of PyGDH itself; both Python and Cython versions of the source code are provided. Based on our experience, we suggest the following procedure (in a GNU/Linux or similar environment):

#. Produce a mature set of Python source code files.
#. Transform these into a set of Cython source code files, based on profiling information.
#. Use the ``diff`` utility to produce a "unified patch" file containing the changes in the Cython version relative to the Python version::

    diff -u body.py body.pyx > body.patch

#. Modify and test the Python version
#. Use the ``patch`` utility to apply the saved changes to the Python version, producing a new Cython version (and saving a backup)::

    patch -b -o body.pyx body.py body.patch

#. Examine the new Cython version to ensure that the changes are reasonable.
#. Test the new Cython version, adding new modifications if desired.
#. Repeat from step 3 if further changes to the model are necessary.

Suggested exercises
===================
1. Profile the two-dimensional Laplace equation solver, create a Cython version, and run it.
   
