***************
Coupled domains
***************

This chapter demonstrates a problem for which two ``Grid`` objects are created, with separate equations simultaneously solved over each ``Grid`` and with coupling between the solutions. 

Employing multiple ``Grid`` objects, as introduced in this chapter, can be useful in many other situations. For example, as discussed in an earlier chapter, a ``Grid`` object containing a single spatial unit can be used to solve time-dependent ODEs. With multiple grids, one can solve problems that couple PDEs and time-dependent ODEs.

This example revisits the problem of the PDE with a flux boundary condition. The same problem will be solved in this chapter, but the domain will be divided into two halves, each represented by separate ``Grid`` objects, in order to demonstrate how one may couple the solutions on each domain.

Spatial domains
===============
For convenience, the left and right spatial domains will be represented by objects belong to the same user-defined ``Domain`` class. This is desirable because these objects share many similarities. For more flexibility, one could define two different classes, perhaps both derived from an intermediate user-defined class.

The ``Domain`` class in this example is very similar to that of the earlier PDE example, but the associated spatial coordinate values are now provided through a parameter to ``__init__``, as they will differ between the left and right spatial domains.

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START GRID
   :end-before: #### SPHINX SNIPPET END GRID

The ``__init__`` method of the ``Problem``-derived class now creates two ``Grid`` objects. A list naming these objects is passed to ``initialize_problem``. The order in which they are listed implicitly associates each with a numerical label; here, ``'left_domain'`` is associated with the index ``0``, and ``'right_domain'`` is associated with the index ``1``.

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROBLEM INIT
   :end-before: #### SPHINX SNIPPET END PROBLEM INIT

Discretized equations, part 1
=============================
Most of the ``Volume`` objects are associated with the same discretized equation used in the original example, although separate ``Grid`` objects may associated with separate thermal diffusivities, and one must be careful to reference the solution field values of the appropriate ``Grid`` by providing the appropriate index to the ``grid`` array:

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py 
   :code:
   :start-after: #### SPHINX SNIPPET START LEFT PDE
   :end-before: #### SPHINX SNIPPET END LEFT PDE

		
.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START RIGHT PDE
   :end-before: #### SPHINX SNIPPET END RIGHT PDE

Boundary conditions
===================
Similarly, the equation at the right boundary of the right domain, at which there is a flux boundary condition, is the same as that in the original example, except with the appropriate value of the thermal diffusivity and using the index corresponding to the second ``Grid``:

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START RIGHT BOUNDARY PDE            
   :end-before: #### SPHINX SNIPPET END RIGHT BOUNDARY PDE             

The value of the dependent variable at the left boundary of the left domain is given in the problem formulation, and so is treated in the same way as in the original example.

However, the interface between the two domains requires particular attention. The two domains are continuous, and as described in the earlier chapter on discretization, the dependent variable values associated with the ``Volume`` objects at the ends of each domain are "located on" the domain boundaries. The dependent variable value of the rightmost ``Volume`` of the left domain should therefore be identical to that on the leftmost ``Volume`` of the right domain. This value should be determined by the combined behavior within the "half-volumes" on either side of the interface.

The shared interface can be implemented in many ways. In this example, the governing equation will be solved on the rightmost ``Volume`` of the left domain (while still accounting for the behavior on both sides of the interface), and the value at the adjacent boundary of the right domain will be "given" by the solution on the left side of the interface. As usual, the boundary values will be set through ``set_boundary_values``

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START BC
   :end-before: #### SPHINX SNIPPET END BC

This arrangement is consistent with the ``declare_unknowns`` method that is common to both ``Domain`` objects, and which is identical to that in the original example

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DECLARE UNKNOWNS
   :end-before: #### SPHINX SNIPPET END DECLARE UNKNOWNS

.. To enforce this relationship, the value associated with this ``Volume`` will be treated as "unknown" on one ``Grid`` and "known" on the other, and ``set_boundary_values`` will be used to copy the value computed for the "unknown" side onto the "known" side, so that it can be accessed by the interpolation routines of the adjacent ``Volume`` objects on the same ``Grid``. A governing equation will be associated with the "unknown" side, but not the "known" side, just as with more familiar boundary ``Volume`` objects.

.. For this example, the "unknown" ``Volume`` will be that on the right ``Grid``, and the "known" ``Volume`` will be that on the left. As in the original example, the boundary value is prescribed on the far left boundary. The value associated with the ``Volume`` on the right ``Grid``, with index ``1``, is copied to the ``Volume`` on the left ``Grid``, with index ``0``.

Discretized equations, part 2
=============================
As discussed in the previous section, as implemented, the "known" solution value on the rightmost ``Volume`` of the left ``Grid`` is copied from the computed value of the leftmost ``Volume`` of the right ``Grid``. The ``Volume`` on the right ``Grid`` is associated with a discretized equation that has contributions from both sides of the interface.

In order to obtain this discretized equation, we retrace our steps in the earlier formulation on a single ``Grid``, but recognizing this time that the parameter :math:`\alpha` may take different values :math:`\alpha_l` and :math:`\alpha_r` on the left and right sides of the interface. The left edge of the left ``Volume`` will be located at position :math:`a`, the interface at `b`, and the right edge of the right ``Volume`` at :math:`c`.

Integrating the governing equation over the entire region gives

.. math::
   \int_a^c \frac{\partial y}{\partial t} \ dx - \alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b - \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c = 0.

Using the average value of the integrand, this is

.. math::
   \left<\frac{\partial y}{\partial t}\right> (c - a) - \alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b - \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c = 0.

As an approximation, the average value of the time derivative is now taken as the value of the time derivative at the interface:

.. math::
   \frac{\partial y_b}{\partial t} (c - a) - \alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b - \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c \approx 0.

Discretization in time by the first-order backward difference scheme then gives

.. math::
   \frac{y\left(t_0 + \Delta t, b\right) - y\left(t_0, b\right)}{\Delta t} (c-a) - \alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b - \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c \approx 0.

In the physical interpretation of the heat equation, the product of :math:`\alpha` and the spatial derivative the negative of the flux, which is continuous in the present case. Therefore, the terms corresponding to positions immediately adjacent to the interface should cancel in principle, and we can conclude that

.. math::
   \frac{y\left(t_0 + \Delta t, b\right) - y\left(t_0, b\right)}{\Delta t} (c-a) - \alpha_r \frac{\partial y}{\partial x}(t_0+\Delta t,c) + \alpha_l \frac{\partial y}{\partial x}(t_0+\Delta t,a) \approx 0.

Note that this suggests that the method implementing this equation must use information taken from both neighboring ``Grid`` objects. This is implemented as follows:
   
.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START INTERFACE PDE
   :end-before: #### SPHINX SNIPPET END INTERFACE PDE

Assigning equations
===================
The assignment of the governing equation methods to the ``Volumes`` of the two domains is straightforward, although one must be careful to use the appropriate numerical labels when referencing data structures

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ASSIGN EQUATIONS
   :end-before: #### SPHINX SNIPPET END ASSIGN EQUATIONS 

Solution
========
Although the two thermal diffusivities can be specified independently, they will both be given the value used in the original example in order to check consistency with the original numerical solution:

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

The old and new solutions are plotted below at the first few timesteps:

.. image:: ../../pygdh/examples/multiple_grids/multiple_grids.png

This was generated with the following GNUPLOT script:

.. include:: ../../pygdh/examples/multiple_grids/multiple_grids.gpl
   :literal:
      
Exercises
=========
1. Modify this program so that at the interface, the value on the left domain is copied from right domain. Due to the presence of ``declare_unknowns`` in the ``Domain`` definition, some large modifications are needed. Two suggested approaches are to:

  a. pass an additional parameter to ``__init__`` to indicate which set of ``unknown_field`` values should be used for a given ``Domain`` object, or

  b. remove ``declare_unknowns`` from ``Domain`` and create two new classes with ``Domain`` as a base class. Each of these classes will have a separate definition of ``declare_unknowns``. The left and right spatial domains will now be associated with objects created from these two different classes (which share the same base class).
