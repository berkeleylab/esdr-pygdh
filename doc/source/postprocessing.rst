**************
Postprocessing
**************

Sometimes, the standard human-readable output formats are too limiting--every dataset contains one entry for each ``Volume`` in a ``Grid``, identified by the ``coordinate`` value(s) of the ``Volume``. For this reason, PyGDH includes postprocessing capabilities, with which users can write programs to extract and manipulate data stored in HDF5 or PICKLE files.

The postprocessing programs take stored solutions and restore them into the same data structures that were used in the original solver programs. This strategy ensures that postprocessing with PyGDH follows naturally from writing solver programs; few new concepts are required. Also, it provides a simple mechanism for restarting time-dependent simulations from stored results.

The existing GNUPLOT or CSV output facilities remain available to postprocessing programs, but users may generate human-readable results in their own formats; the postprocessing facilities allow users to work with the numerical solutions in a more flexible way than one can by defining a ``process_solution`` method and setting additional output fields.

Program organization
====================
In each of the examples given in previous chapters, the problem class definition is followed by statements that create an object of that class and direct that object to find solutions. However, postprocessing should be performed by a separate program, because there is no need to recompute the solution when only the postprocessing step is modified. Since the PyGDH postprocessing strategy is to read saved solutions into the same data structures with which they were originally computed, the data structures must be defined in both the original solver program and in the corresponding postprocessing program. Rather than maintaining two similar source code files, it is recommended that users put almost all of the source code into a common file (in this example, ``pygdh/examples/postprocessing/PDE_body.py``). Small additional files (in this example, ``pygdh/examples/postprocessing/PDE_solve.py`` and ``pygdh/examples/postprocessing/PDE_postprocess.py``) can then access this common source code and use it to either compute numerical solutions or to postprocess saved solutions. This strategy will also be useful when making use of the `Cython <http://cython.org>`_ compiler, as described in an :doc:`upcoming chapter <compiling>`.

As an illustration, the program presented in the previous chapter can be split into two source code files. The first, called ``pygdh/examples/postprocessing/PDE_body.py``, defines the entire mathematical problem except for some high-level parameter values:

.. include:: ../../pygdh/examples/postprocessing/PDE_body_annotated.py
   :code:
      
The second, called ``pygdh/examples/postprocessing/PDE_solve.py`` merely imports the common source code, creates a ``pygdh.Problem``-derived object, defines parameter values, and tells the object to solve the mathematical problem:

.. include:: ../../pygdh/examples/postprocessing/PDE_solve_annotated.py
   :code:

Note that one must now indicate that the ``PDE`` class is in the imported ``PDE_body`` module, and that the ``output_types`` list has been changed to include the ``'HDF5'`` option. The resulting data file will be used by the postprocessing program.

Another benefit of this arrangement is that the small files like ``PDE_solve.py`` can serve as records of the parameters used to obtain a stored solution.

An example using matplotlib
===========================
Postprocessing programs can make use of any capabilities available through Python. This examples makes use of the `matplotlib <http://matplotlib.org>`_ library, which allows sophisticated plots to be created from within the Python environment.

As in ``pygdh/examples/postprocessing/PDE_solve.py``, the postprocessing program, ``pygdh/examples/postprocessing/PDE_postprocess.py`` begins by creating a ``pygdh.Problem``-derived object, which in turn creates a ``pygdh.Grid``-derived object of a size that is compatible with that used to obtain the stored solutions. The ``PDE`` object is then instructed to load the HDF5 or PICKLE file containing saved solutions, and then asked to restore its internal state to that immediately after the solution for the second timestep was computed. 

.. include:: ../../pygdh/examples/postprocessing/PDE_postprocess_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START LOAD
   :end-before: #### SPHINX SNIPPET END LOAD
		 
The specified solution is then accessible just as it was when it was first computed by the solver. Creating a simple plot with matplotlib can involve little more than specifying the data to be plotted:

.. include:: ../../pygdh/examples/postprocessing/PDE_postprocess_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PLOT
   :end-before: #### SPHINX SNIPPET END PLOT

This produces the following image:

.. image:: ../../pygdh/examples/postprocessing/PDE_postprocess.png
   :scale: 75%

Restarting time-dependent simulations
=====================================
Since postprocessing in PyGDH involves restoring ``pygdh.Problem``-derived objects to their states following the calculation of saved numerical solutions, restarting time-dependent simulations from any saved solution is a simple matter. Restoring an object to a saved state restores not only the "present" solution, but the "past" solutions that are also needed to compute future solutions according to the timestepping scheme used. The following program computes three additional timesteps for the PDE example:

.. include:: ../../pygdh/examples/postprocessing/PDE_restart_annotated.py
   :code:

PyGDH knows to restart the simulation from the specified timestep because a saved solution file has been loaded. The saved and new solutions are shown in the following figure:

.. image:: ../../pygdh/examples/postprocessing/PDE_restart.png

Below is the GNUPLOT script used to generate this figure:

.. include:: ../../pygdh/examples/postprocessing/PDE_restart.gpl
   :literal:

HDF5 output files are automatically used in preference to PICKLE files if both are available because the HDF5 format can be accessed much more efficiently. However, the same capabilities are available through the PICKLE format. This is an example using files with slightly different names, so that the HDF5 file generated earlier is not used in preference to the PICKLE file.

.. include:: ../../pygdh/examples/postprocessing/PDE_pickle_solve_annotated.py
   :code:

.. include:: ../../pygdh/examples/postprocessing/PDE_pickle_restart_annotated.py
   :code:

.. include:: ../../pygdh/examples/postprocessing/PDE_pickle_restart.gpl
   :literal:

.. image:: ../../pygdh/examples/postprocessing/PDE_pickle_restart.png
		    
Suggested exercises
===================

#. Revisit the large-deformation solid mechanics problem
   
 #. Split the source code into two files, one containing a program that solves the mathematical problem by making use of the source code imported from the second file, which contains source code that will be needed by the postprocessing program as well.
 #. The output was originally written in terms of the material positions, the positions of material "particles" before displacement. Write a postprocessing program that reads in the saved solution and writes out the same data in written in terms of the spatial positions, the displaced particle positions.
 #. Modify the postprocessing program to produce a plot of spatial positions as a function of material positions.

.. Much of the original user class definition will remain unchanged, but several additions will be made in order to illustrate the use of the postprocessing interface.

.. This method is called only once by PyGDH. Defining data structures here and modifying their elements in frequently-called methods is more efficient than creating data structures as needed. Typically, these data structures hold values of intermediate calculations, with one entry per ``Volume``. These intermediate calculations may be needed in more than one ``Volume``, making it efficient to calculate the values once and use them repeatedly. A common example is of variables that are not solution field variables, but for which spatial derivatives must be computed.

.. Each user-defined field variable that is to be stored in an output file must be associated with one of the ``Grid`` objects.
