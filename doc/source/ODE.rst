************************************************
An ODE with position as the independent variable
************************************************

This chapter provides a detailed look at a Python program that uses PyGDH to solve a simple example problem, consisting of an ODE with given boundary values:

.. math::
   \frac{d^2 y}{d x^2} = - \alpha^2 y(x)

   y(0) = 0

   y(1) = 1

The example problem is provided as ``pygdh/examples/ODE/ODE.py`` within the PyGDH archive. While there are many details, users do not need to write programs from scratch--program templates for typical mathematical problems are provided with PyGDH.

Program structure
=================
Three major steps must be performed in any program that uses PyGDH to solve equations. First, one must define the spatial domain on which the governing equations are to be solved. Second, one must describe the rest of the structure of the mathematical problem, including the equations to be solved, boundary conditions, and, for a time-dependent problem, initial conditions. Finally, one must create an object that represents the specific problem to be solved (declaring the values of all model parameters, where needed), and direct this object to solve the mathematical problem and report results.

The statements

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START IMPORT
   :end-before: #### SPHINX SNIPPET END IMPORT

appear at the top of this file, and indicate that the Python interpreter will need access to PyGDH and `NumPy <http://numpy.scipy.org>`_ (for matrix math) in order to run this program. These are "packages" or "modules" that, in effect, contain additional Python source code. By creating packages and modules, programmers simplify the process of making their source code available for use by other programmers.

Spatial domain definition
-------------------------
The spatial problem domains in PyGDH on which the equations are to be solved (a one-dimensional region in this example) are described with ``Grid``-derived objects. Each of these objects describes a one-dimensional or two-dimensional spatial region on which equations are to be solved. For simplicity, PyGDH works only with regions based on rectangular grids in user-defined coordinate spaces.

The ``Grid`` class is defined by PyGDH, and includes many data structures and methods needed to represent a spatial problem domain. Objects should not be created directly from the ``Grid`` class defined by PyGDH. Instead, the ``Grid`` base class from the ``pygdh`` module should be specialized for the specific problem of interest by creating a derived class (named ``Domain`` in the example problem). The ``Grid`` class serves as a template and helps to organize problem-solving programs. The user-defined spatial domain class can be given any name that is acceptable to Python; in this example, it is called ``Domain``.

The ``Domain`` class definition begins with a line naming the class, 

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START GRID
   :end-before: #### SPHINX SNIPPET END GRID

followed by indented text that describes various aspects of the class, and ends when another line with the same indentation as the first line is encountered, or otherwise upon encountering the end of the file. The expression ``pygdh.Grid`` in the first line of the class definition is a reference the ``Grid`` class contained within the ``pygdh`` package (which was made accessible by the earlier ``import`` statement). Its appearance in the class definition indicates that the new ``Domain`` class will be a derived class that builds on the base class ``Grid``. Any object of the ``Domain`` class type is therefore also an object of the ``Grid`` class.

Users should define an ``__init__`` method. PyGDH itself does not access the ``__init__`` routines of ``Grid``-derived objects, so users may customize these methods to the needs of the problem. However, the user-defined ``__init__`` routines in these derived classes must call the ``initialize_grid`` method defined by the ``Grid`` base class, which is needed to set up data structures for calculations.

The parameters supplied to the ``initialize_grid`` method establish the problem domain. This example applies the "finite volume method" on a one-dimensional spatial domain. From this perspective, a one-dimensional spatial domain consists of adjacent, non-overlapping, one-dimensional "volumes". Each of these volumes is associated with a unique coordinate value, which will be called the "position" of the volume. The set of these coordinate values defines the one-dimensional coordinate grid. For convenience, each volume is also given an integer label. The domain used in this example is illustrated below:

.. image:: _static/one_dimensional_grid.png

Please note that the volumes on the boundaries are half the width of the interior volumes. This is done so that the coordinate positions associated with these volumes are also the positions of the domain boundaries.

The call to ``initialize_grid`` can be written as

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START INITALIZE GRID                
   :end-before: #### SPHINX SNIPPET END INITALIZE GRID                        

As the user-defined class has not also defined an ``initialize_grid`` method, the method defined by the base class is still accessible as ``self.initialize_grid``; the derived class has "inherited" this property. This method is called with a number of parameters, some of which have the appearance of assignment statements. These "keyword arguments" have several benefits. They can improve clarity when passing multiple parameters, and they may be specified in any order, since the Python interpreter no longer needs to rely on the order in which parameter values are given in order to make initial assignments for argument variables.

Please note that this particular statement spans multiple lines in order to keep the line widths within 80 characters, which is encouraged in Python programming practice, as some users are limited to this screen width. Simple (as opposed to compound) Python statements are usually restricted to single lines. However, splitting a statement in this way is permitted when the line breaks occur within pairs of parentheses or square or curly brackets.

The first parameter to ``initialize_grid`` is a descriptive label for the new object, represented as a string. This will be used to label this domain in the program output, and can be used within the program for clarity.

The ``field_names`` parameter is a list of strings, which give descriptive names to the dependent variables for which solutions are to be obtained. As with any list object, the order in which these names are given implicitly assigns a unique integer label to each of these variables. These numerical labels will be the primary means by which the various variables will be later referenced. In this example, there is only one dependent variable, named ``'y'``. Its position in the ``field_names`` parameter implicitly associates it with the index ``0``.

The ``coordinate_values`` parameter is a dictionary, which associates descriptive names (in the form of strings) for independent spatial variables with a sequence of corresponding coordinate values. Here, ``'x'`` is the only key, and it is associated with the values obtained by evaluating::

    numpy.linspace(0.0, 1.0, volume_count)

.. This process of specialization will be discussed in the following section; this section will focus on the creation of the ``Grid``-derived class.

Here, the ``linspace`` function from the ``numpy`` package creates an array containing a sequence of numbers (the number of which is given by the value of ``volume_count``) that are evenly-spaced between ``0.0`` and ``1.0``, inclusive. This results in the sequence of volume positions illustrated in the diagram above.

A ``Grid``-derived spatial domain is assembled out of smaller units. The ``unit_classes`` parameter is a sequence, with one element per unit making up the spatial domain. Each element refers to a class that defines the type of spatial unit object that will be associated with the corresponding volume in the domain. This allows for a great deal of flexibility, and in two-dimensional domains, this parameter further describes the physical layout of the domain. For the present example, the ``Domain`` class definition uses units described by a user-defined ``Volume`` class, which is a derived class of the ``pygdh.Volume`` class (in the ``pygdh`` package and so distinct from the ``Volume`` class defined in the present program), and starts with the line:

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START VOLUME
   :end-before: #### SPHINX SNIPPET END VOLUME

Like the ``pygdh.Grid`` class, the ``pygdh.Volume`` class is defined by PyGDH and intended as a template from which users can create derived classes with the same basic structures.

Mathematical problem definition
-------------------------------
The description of the mathematical problem is expressed in a user-defined derived class of the ``Problem`` class, which is also provided by PyGDH.  Here, the derived class has been given the user-defined name ``ODE``. Any object created based on the ``ODE`` class is also an object of the ``Problem`` class. A user should define an ``__init__`` method for this derived class, as in this example

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROBLEM INIT    
   :end-before: #### SPHINX SNIPPET END PROBLEM INIT

As with the user-defined ``Domain`` class, PyGDH does not access ``__init__`` in the present class definition, so the method may be customized for the needs of the problem. However, it should call the ``initialize_problem`` method defined by the ``Problem`` base class. This method must be provided with a single parameter, a sequence of user-defined ``Grid``-derived objects that describe the spatial domains on which equations are to be solved. The order in which these objects appear in this sequence implicitly associates each object with an integer label, with the first having label ``0``, the second having label ``1``, and so on.

To support code readability and reuse, one can also use the ``name`` parameters specified in the calls to the ``Grid.initialize_grid`` method as keys in the ``Problem.grid_number_with_name`` and ``Problem.grid_with_name`` dictionaries in order to retrieve corresponding ``Grid`` sequence numbers and ``Grid`` objects.

Setting prescribed value boundary conditions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The spatial domain of this example problem was defined in terms of the coordinate ``'x'`` and runs from ``0.0`` to ``1.0``. Typically, information about the solution at the domain boundaries is needed in order to completely specify the problem to be solved. The boundary conditions for this example are

.. math::
   y(0) = 0

   y(1) = 1

PyGDH allows the user to strictly enforce solution values on a boundary. This is accomplished by defining a method in the ``Problem``-derived class named ``set_boundary_values``, which takes only the ``self`` argument:

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START BOUNDARY VALUES
   :end-before: #### SPHINX SNIPPET END BOUNDARY VALUES

The first line defines a temporary variable for clarity, with the same name as the descriptive label given to ``initialize_grid``. The complicated-looking expression will be explained in the next chapter; for now, it is enough to know that ``'y'`` is a one-dimensional array that contains one value for each volume element in the domain. The first (0th) element of this array corresponds to the volume at the boundary ``0.0``, and the last element corresponds to the volume at the boundary ``1.0``, in the same diagram:

.. image:: _static/one_dimensional_grid.png

The 0th element of the NumPy array ``y`` is referenced by following the variable name with the integer label ``0`` in square brackets. Since this is done on the left side of an assignment statement, the specified array element is given the value of the expression on the right side of the assignment statement (``0.0``).

The second assignment is similar, and references the end of the array by using the negative index notation, which provides an alternative method of labeling the volumes:

.. image:: _static/one_dimensional_indexing.png

In situations such as this, it is much more convenient to use these negative indices, rather than having to be aware of the total number of elements in the array when using a non-negative index.

Declaring unknowns
^^^^^^^^^^^^^^^^^^
PyGDH must be made aware of the locations in the domain at which the solution is unknown and therefore should be computed. This should be done within a user-defined method named ``declare_unknowns`` in the ``Problem``-derived object. This method must take only ``self`` as an argument:

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DECLARE UNKNOWNS
   :end-before: #### SPHINX SNIPPET END DECLARE UNKNOWNS

Here, the local variable ``vol`` is defined (and re-defined) for clarity. 

.. Python variables (and programming language variables in general) can be reassigned to different values and different data structures.

The associated expressions involve both the dot operator and the index operator. The operations associate from left-to-right, so the first definition is equivalent to:

::

   vol = ((self.grid[0]).unit_with_number)[0]

That is, the newly-created local variable ``vol`` is given the value of the 0th element of the ``unit_with_number`` member of the ``self`` object.

The member ``unit_with_number`` is a sequence that is automatically defined for any ``Grid``-derived object. Each element of this sequence is a ``Volume`` object that represents a single volume in the domain. The first ``Volume`` object in this list is associated with the most negative value (and therefore one domain boundary) in the appropriate entry of the ``coordinate_values`` argument to ``initialize_grid``. The last ``Volume`` object in ``unit_with_number`` is associated with the other domain boundary, and the most positive value in the ``coordinate_values`` argument.

Each ``Grid`` object contains a list named ``unknown``, each element of which corresponds, in order, to corresponding ``Grid`` sequence number. Each of these elements is itself an NumPy array, with the first index corresponding to the position in the list of variables named by the ``field_names`` argument to ``initialize_grid``, and the second index corresponding to the corresponding position in ``unit_with_number``.  In this example, only one field variable was given, with the descriptive string ``'y'``. As the first and only element in the list of field variables, it was implicitly assigned the label ``0``. This same label is used to reference this variable throughout the data structures used by PyGDH, including in the ``unknown`` lists. Therefore, ``unknown[0]`` corresponds to the status of the field named ``'y'`` on the ``Volume`` of interest.

Each element of ``unknown`` is given the value ``True`` if the corresponding field value is unknown, or ``False`` otherwise. The ``unknown`` value associated with a variable on a given ``Volume`` should only be ``False`` if the ``set_boundary_values`` method provides a value for that variable on that particular ``Volume``. In this particular case, boundary values were given on both ends of the domain, so PyGDH should not attempt to solve for values at these locations.

By default, the ``unknown`` element corresponding to every field variable is set to ``True`` on every ``Volume`` object, so it is only actually necessary to specify where it should instead be ``False``. However, for clarity, the assignments are made here for every ``Volume``.

The ``unit_count`` member of objects belonging to the ``Grid`` class is automatically determined by ``initialize_grid``, and gives the total number of spatial units associated with the ``Grid``-derived object. This is the total number of elements in ``unit_with_number`` sequence, with numbers ``0`` through ``unit_count-1``. The for-loop here is written to visit all but the first and last elements in ``unit_with_number``, which involves numbers ``1`` through ``unit_count-2``. This list of successive values is produced by the expression ``range(1, self.unit_count-1)``.

Describing equation dimensions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PyGDH must be made aware of how many scalar equations are represented by each user-defined governing equation method. This is done in a required method named ``set_equation_scalar_counts``, in which the user creates a new dictionary, named ``equation_scalar_counts``, as a member of the ``Problem``-derived object. This dictionary associates methods representing governing equations, with integers indicating the number of scalar equations that they represent. Functions are themselves objects, and the name of a function, written alone, is a reference to the function object itself. In this particular case, there will be only one governing equation method, named ``ode`` and defined in the ``ODE`` class, and it will be associated with a single scalar equation. For the present, it is sufficient to note that this method is a discretized representation of the given governing equation.

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START SCALAR COUNTS
   :end-before: #### SPHINX SNIPPET END SCALAR COUNTS

In more complicated situations, it might be convenient to have a method that represents a vector equation, in which case the associated number of scalar equations must reflect the vectorial nature of the returned value.

Associating equations with spatial units
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Just as it was necessary to specify the unknown field variables associated with each ``Volume``, it is necessary to specify which equations govern the behavior of the variables on each ``Volume``. This is done in a user-defined method of the ``Problem``-derived class. This method must be named ``assign_equations``, and it must take only ``self`` as an argument.

In the example, one can see that the structure of this method is reminiscent of the ``declare_unknowns`` method of the ``Grid``-derived class. 

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START_ASSIGN EQUATIONS
   :end-before: #### SPHINX SNIPPET END ASSIGN EQUATIONS        

This method is part of the ``Problem`` class because the methods representing the governing equations are also part of the ``Problem`` class, which simplifies interactions among multiple ``Grid`` objects.
		
The automatically-created ``equations`` member of the ``Problem``-derived object is a sequence, with one element for each ``Grid``-derived object supplied to the ``initialize_problem`` method. The elements of the ``equations`` sequence in turn correspond to the ``Grid``-derived objects in the list provided to ``initialize_problem``, and are numbered in the same sequence. In this example, there is only one ``Grid`` object, describing the spatial domain and having the index ``0``. Here, a local variable ``domain_equations`` is used to clarify references to the corresponding part of the ``equations`` member.

The elements of the ``equations`` sequence are themselves sequences, with one for each unit object associated with the corresponding ``Grid`` object, and ordered in the same way as the ``unit_with_number`` sequence of the same ``Grid`` object. Each element of these sequences is a list of references to functions, each of which describes one or more governing equations for the corresponding unit. In this example, the first and last units, with indices ``0`` and ``-1``, have values that are known from the ``set_boundary_values`` method (and as noted in the ``declare_unknowns`` method), and so these are associated with empty lists. Dependent variable behavior on all other units, on the interior of the domain, is governed by the ``ode`` method, which corresponds to a single scalar equation, as noted in ``equation_scalar_counts``.

Since this method is a member of the ``Problem`` object, and not a ``Grid`` object, the ``self`` variable here refers to the ``Problem`` object. In order to provide convenient access to ``Grid`` object(s), a ``Problem``-derived object automatically defines a member named ``grid``, which is a list of associated ``Grid``-derived objects. This list object is identical in content and order to the argument originally passed to the ``initialize_problem`` method. In this example, there is only one ``Grid`` object, so it is implicitly assigned the index ``0``. This number is now used to reference the one ``Grid`` object in the list ``grid``. Then, as before, the ``unit_count`` member of the ``Grid`` object may be accessed directly.

.. The ``set_equations`` method should define a new list, named ``equations``, for each ``Volume`` object in the entire problem domain. This list should contain all of the methods corresponding to equations that govern the behavior of the dependent variable values associated with the corresponding ``Volume``; the ordering of equations does not matter. In this case, the ``ode`` method governs the behavior on the interior of the domain. The solution values at the boundaries are given, so no governing equation methods should be considered at these locations. Square brackets enclosing only blank characters or no characters yield an empty list--a list object containing no objects--are used to indicate this.

.. Specifying a discretized equation
.. ---------------------------------
.. Discretized equations for a finite volume method can be determined from the original equations in a number of ways. In the finite volume method, they represent an approximation to the integration of the original equations over volumes of finite size. The process of discretizing the original equations and expressing the discretized equations to PyGDH are detailed in later sections. At present, please simply note that the ``ODE.ode`` method, in conjunction with the interpolant and differencing operations defined in ``Domain.define_interpolants``, is a representation of the linear differential equation

.. .. math : :
..    \frac{\partial^2 y}{\partial x^2} = - \alpha^2 y(x).

Solver specification and execution
----------------------------------
Now that the mathematical description is complete, one may create an object of the ``ODE`` class, automatically invoking the ``ODE.__init__`` method to initialize data structures and set the simulation parameters of interest. This object is then instructed to solve the mathematical problem that it describes and to report the results:

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

The ``solve_time_independent_system`` method, defined in the ``Problem`` class, accepts a descriptive string from which it will determine the output filename(s), as well as a list of strings indicating the file formats in which the output should be written. The method then solves the discretized problem and writes the result to the output file(s).

At the computer's command-line interface, if ``ODE.py`` is in the present directory, the program can be run by typing::

	python ODE.py

In this example, the resulting numerical solution is automatically saved a human-readable text file named ``ODE_domain.gnuplot``. The contents can then be further processed or plotted. A few different file formats are available, and the present format is a simple one that is understandable by many different plotting programs, including GNUPLOT, a common plotting program on GNU/Linux systems. 

GNUPLOT can be provided with a sequence of commands stored in a text file, which guide the program to produce plots. Here, the following GNUPLOT script:

.. include:: ../../pygdh/examples/ODE/ODE.gpl
   :literal:

is stored in a file named ``ODE.gpl`` and executed by typing::

	gnuplot ODE.gpl

at the computer's command-line interface. This produces the following plot of the numerical solution along with the exact solution:

.. image:: ../../pygdh/examples/ODE/ODE.png

where the exact solution for this problem is given by

.. math::
	y(x) = \frac{\sin(\alpha x)}{\sin(\alpha)}
	:label: analytical

for :math:`\alpha=1`.

Please note that on some systems, the ``png`` "terminal" (specifying the output format) will not be available, and another terminal must be used. For example, the first line might be replaced with ``set terminal postscript color``. Running GNUPLOT in interactive mode by typing ``gnuplot`` at the system's command-line interface, then typing ``set terminal`` at the ``gnuplot>`` prompt will provide a list of available options. One may type ``exit`` or ``quit`` to leave GNUPLOT and return to the system's command-line interface.
   
Suggested exercises
===================
1. Run the code and plot the results.

2. Although the source code has not been explained in detail, try to read through the comments (lines starting with the ``#`` character) and try to modify the program to instead use :math:`\alpha = 10`. Do your results continue to agree well with the analytical solution :eq:`analytical`? If you are using GNUPLOT, a text editor may be used to modify the GNUPLOT script to plot the appropriate analytical solution values.

3. Modify the program to use a domain divided into 6 volumes. Do your results continue to agree well with the analytical solution? What if 21 volumes are used?

4. Change the boundary values, so that

.. math::
   y(0) = 1

   y(1) = 0

Do you get what you expect?


.. 3. Solve the original problem, but change ``ODE.__init__`` to accept ``sqrt_alpha`` :math:`= \sqrt{\alpha}` rather than ``alpha`` :math:`= \alpha`.

.. The definition of this method is determined by the programmer, providing flexibility over how these are initialized. However, this is frequently an appropriate place to define the spatial domain of the problem, and to call the ``initialize_problem`` method of the base class. 

.. These first of these will be described in an upcoming section.

.. The ``initialize_problem`` method is defined in the ``Problem`` base class of the ``pygdh`` module, and so is accessible to the user-defined derived class ``ODE``. This routine must be passed a list of ``Grid`` objects that represent the spatial problem domain.


.. User-controlled initialization
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. PyGDH does not access the ``__init__`` routine in ``ODE`` object. The definition of this method is entirely up to the programmer, giving the programmer flexibility over how the ``Problem``-derived objects are initialized. However, the programmer will typically want to use the ``__init__`` routine to define the spatial domain of the problem, and to call the ``initialize_problem`` method of the base class. 

.. These first of these will be described in an upcoming section.

.. The ``initialize_problem`` method is defined in the ``Problem`` base class of the ``pygdh`` module, and so is accessible to the user-defined derived class ``ODE``. This routine must be passed a list of ``Grid`` objects that represent the spatial problem domain.
