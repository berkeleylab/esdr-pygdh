****************************************
Solving an ODE with an initial condition
****************************************

Using PyGDH to solve an ODE with time as the independent variable is typically simpler than using it to solve an ODE with position as the independent variable. However, this tutorial addressed boundary-value problems earlier because the motivation behind much of the design of PyGDH is clearer from the perspective of solving boundary value problems. Solving an ODE with time as the independent variable reuses the same machinery, much of which might appear to be excessive without the broader context.

An ODE with no spatial dependence may be solved by creating a ``Grid`` object containing only a single ``Volume``. The spatial aspects of this ``Volume`` are not relevant, but its data structures will be used to store solution values. Time-discretized equations describing the evolution of these values are then specified, and these equations are then solved for a series of timesteps, giving numerical solutions to the discretized equations. Unlike with discretization of the spatial domain, as described in the previous chapter, the user is responsible for explicitly providing the time-discretized equations.

A future chapter discusses the simultaneous solution of problems that make use of multiple ``Grid`` objects. This approach can be used to simultaneously solve PDEs and ODEs with time as the independent variable.

This chapter will consider the equation

.. math::
   \frac{dy}{dt} = -\alpha y

with initial condition

.. math::
   y(0) = 1

"Spatial" domain
================
Since this problem has no spatial dependence, the ``pygdh.Volume`` class may be used directly--there is no need to first create a derived class because there is no need to perform interpolation. Nor is it necessary to declare independent spatial variables to ``initialize_grid`` in a problem with no spatial dependence. The ``__init__`` routine of the ``Grid``-derived object can then be simply defined as:

.. include:: ../../pygdh/examples/ODE_in_time/ODE_in_time_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START GRID INIT
   :end-before: #### SPHINX SNIPPET END GRID INIT
		 
Discretization of a time-dependent equation
===========================================
This section will discuss a simple approach to discretization of an ODE in time. The discretized equation obtained here will suggest additional machinery needed to obtain a numerical solution.

Computer solutions of time-dependent problems typically involve finding approximate numerical solutions at successive moments in time, called "timesteps". Taken together, these approximately describe the time-dependent solution. Typically, the quality of the approximation can be improved by using smaller intervals between timesteps (the "timestep size").

Under these "time discretization" schemes, time is not a continuous variable, so quantities such as time derivatives are approximated by comparing solutions at different timesteps. For this reason, solutions calculated at previous timesteps are generally needed in order to compute approximate solutions to time-dependent problems. 

For simplicity, this example will use a simple first-order backward difference in time (note that there are many well-known schemes that are both more accurate and more complicated). Under this scheme, the differential equation

.. math::
   \frac{\partial y}{\partial t}(t) = g(t,y(t))

is approximated by

.. math::
   \frac{y(t_0 + \Delta t) - y(t_0)}{\Delta t} \approx g(t_0 + \Delta t,y(t_0 + \Delta t))

This is called a first-order scheme because the error in approximating the time derivative will be proportional to the size of the timestep to the first power, when the timestep is sufficiently small. 

.. This notion is typically expressed by including an error term:

.. math: :
..   \frac{y(t_0 + \Delta t) - y(t_0)}{\Delta t} = g(t_0 + \Delta t,y(t_0 + \Delta t)) + \mathcal{O}(\Delta t).

Moving all terms to one side gives an expression in the form expected by PyGDH:

.. and dropping the error term 

.. math::
   F(t,y) = \frac{y(t_0 + \Delta t) - y(t_0)}{\Delta t} - g(t_0 + \Delta t,y(t_0 + \Delta t)) \approx 0.

Since there is no spatial dependence in this problem, it is unnecessary to integrate this expression in order to obtain a finite-volume form, as was done in the previous chapter.

From this, it is apparent that it will be necessary to incorporate information at two different points in time, :math:`t_0` and :math:`t_0 + \Delta t`. PyGDH must be notified that it must keep track of both the present solution and the solution at the previous timestep. This is declared to PyGDH through the parameter ``stored_solution_count`` to the ``initialize_problem`` method. By default, PyGDH only tracks the solution at the present time, but in this case, PyGDH must provide storage for one previous solution:

.. include:: ../../pygdh/examples/ODE_in_time/ODE_in_time_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROBLEM INIT   
   :end-before: #### SPHINX SNIPPET END PROBLEM INIT   

As before, the discretized equation is described to PyGDH by defining a method:

.. include:: ../../pygdh/examples/ODE_in_time/ODE_in_time_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ODE
   :end-before:	#### SPHINX SNIPPET END ODE

In this method, local variables are defined for clarity. The data structures that they reference will be discussed now in detail.

Solution data storage
---------------------

The definition of the local variable ``y`` references a number of entities. As described earlier, the dot and index operators associate from left-to-right, so the definition is equivalent to
::

   y = ( ( ( ( self.grid ) [0] ) . field ) [0] ) [0,0]

Reading this from the innermost parentheses to the outermost parentheses, this can be interpreted in the following way:

1. Look at the ``grid`` member of ``self`` (a ``Problem``-derived object). The ``grid`` member is a list of ``Grid`` objects, identical in composition and order to the list passed to the ``initialize_problem`` method called in ``ODE_in_time.__init__``. In this example, only one ``Grid`` object is used.

2. Look at this first (0th), and only, element of ``self.grid``, which is a ``Grid`` object. As there is only one ``Grid`` object used in this problem, as determined by the parameter of ``initialize_problem``, it has the index ``0``.

3. Look at the ``field`` member of the ``Grid`` object ``self.grid[0]``. The ``field`` member is a "double-ended queue", or "deque", which is a sequence that may be accessed just as a list is accessed. It is faster for certain operations, at the cost of requiring more memory. The ``field`` member has one entry for every stored numerical solution; in this case, there are two, as indicated by the ``past_solution_count`` parameter to ``initialize_problem``. These correspond to solutions at the present and previous timestep.

4. Look at the value of the first (0th) element of the deque ``self.grid[0].field``. PyGDH automatically manages the ``field`` object, so that the index ``0`` always corresponds to the present timestep, and negative indices can be used to intuitively access past solutions; ``-1`` corresponds to the previous timestep, and so on. In this case, the solution at the present timestep is desired. Each element of ``self.grid[0].field``, such as the one presently being considered, is a two-dimensional array of solution values. 

5. Look at the element ``[0,0]`` in the two-dimensional solution array ``self.grid[0].field[0]``. The element at position ``(i,j)`` in a two-dimensional array (named, for example, ``M``) can be accessed as ``M[i,j]``, and as usual, index numbering begins at ``0``. The first array index indicates the independent variable, numbered according to its position in the ``field_names`` keyword parameter passed to ``initialize_grid``. In this example, there is only one, with the name ``y``. The second index to the two-dimensional array indicates the ``Volume`` corresponding to the spatial position at which the selected variable has the stored value. In this example, the ``Grid`` object has only one ``Volume``, which does not have spatial information, but rather is used only to provide storage for solution values within the framework provided by PyGDH.

6. Associate the local variable named ``y`` with the value of ``self.grid[0].field[0][0,0]``, the solution value at the present time.

The local variable ``y_1`` is defined similarly, except that it accesses the ``-1`` element of the ``field`` deque, so that ``y_1`` is associated with the solution value from the previous timestep.

.. The method representing the discretized equation directly accesses the solution variable values associated with the single ``Volume`` object, at the current and previous timestep. These are the sole elements contained in :math:`1 \times 1` NumPy arrays contained in the ``field`` deque:

.. include: : ../../pygdh/examples/ODE_in_time/ODE_in_time_annotated.py
..   :lines: 34-42

.. Unlike in previous examples, the local variables defined for clarity are variable values, rather than arrays, accessed using the index operator with both indices.

Finally, the ``inverse_timestep_size`` member is automatically calculated by PyGDH from the timestep size provided to ``solve_time_dependent_system``. Multiplication by the inverse timestep size is preferable to division by the timestep size because floating-point multiplication is the faster operation. Alternatively, one could instead multiply the entire expression by the ``timestep_size`` member, also automatically defined by PyGDH.

Declaring equations
===================

As usual, PyGDH must be made aware of any method describing a discretized governing equations. The governing equation in this example describes a single solution variable associated with a single ``Volume`` object on a single ``Grid``:
.. must be 
.. This discretized equation describes the evolution of a single dependent variable associated with a single ``Volume`` object:

.. include:: ../../pygdh/examples/ODE_in_time/ODE_in_time_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ODE
   :end-before:	#### SPHINX SNIPPET END ODE

..  directly accesses the solution variable values associated with the single ``Volume`` object, at the current and previous timestep. These are the sole elements contained in :math:`1 \times 1` NumPy arrays contained in the ``field`` deque:


.. Unlike in previous examples, the  are variable values, rather than arrays, accessed using the index operator with both indices.

Initial condition
=================

Since the example problem is time-dependent, the initial condition must be provided. This must be done in a user-defined method named ``set_initial_conditions`` within the definition of ``Problem``-derived class. This method must have ``self`` as its only parameter.

The ``set_initial_conditions`` methods should set the values for the present solution, with index ``0`` in the ``field`` deque. It should set the values of the dependent variables at all locations at which they are unknown.

The initial value of this problem is set for the single solution value associated with the single ``Volume``. As before, this value is accessed directly by using both array indices.

.. include:: ../../pygdh/examples/ODE_in_time/ODE_in_time_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START IC
   :end-before: #### SPHINX SNIPPET END IC

Solving a time-dependent problem
================================

As in the first example, an object of the ``Problem``-derived class is created, and it is directed to obtain solutions. For a time-dependent system, a solver method that recognizes the time-dependent nature of this problem must be used and information about the simulation interval must be provided.

.. include:: ../../pygdh/examples/ODE_in_time/ODE_in_time_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

As a first-order timestepping scheme was used, a relatively small timestep size is needed to obtain a faithful numerical solution. The results are plotted below against the analytical solution,

.. math::
   y(t) = \exp(-\alpha t)

.. image:: ../../pygdh/examples/ODE_in_time/ODE_in_time.png

The plot above was generated with the following GNUPLOT script:

.. include:: ../../pygdh/examples/ODE_in_time/ODE_in_time.gpl
   :literal:
      
Suggested exercises
===================
1. Halve and double the timestep size to observe the effect of truncation error.

2. Solve the same problem, but use a second-order trapezoidal rule, for which a differential equation of the form

.. math::
   \frac{dy}{dt} = f(t,y)

has the discretized approximate form

.. math::
   \frac{y(t+\Delta t) - y(t)}{\Delta t} = \frac{f(t,y(t)) + f(t+\Delta t,y(t + \Delta t))}{2} + \mathcal{O}(\Delta t^2)

The method describing the governing equation should evaluate and store

.. math::
   y(t+\Delta t) - y(t) -\frac{f(t,y(t)) + f(t+\Delta t,y(t + \Delta t))}{2} \Delta t,

which asks PyGDH to find :math:`y(t+\Delta t)` so that the above expression evaluates to zero.

Confirm that the result is consistent with the analytical solution. Halve the timestep size to observe the effect of truncation error.

3. Solve the same problem, but using a second-order Runge-Kutta scheme. Traditionally, for a differential equation of the form

.. math::
   \frac{dy}{dt} = f(t,y),

a second-order Runge-Kutta scheme is typically expressed in a form such as:

.. math::
   k_1 = f(t,y) \Delta t 

   k_2 = f(t + \Delta t / 2, y(t) + k_1 / 2) \Delta t

   y(t+\Delta t) = y(t) + k_2 + \mathcal{O}(\Delta t^3)

For implementation with PyGDH, this can be arranged into a form such as

.. math::
   y(t+\Delta t) - y(t) - k_2 = 0.

The intermediate results :math:`k_1` and :math:`k_2` can be stored in locally-defined variables for clarity.

Confirm that the result is consistent with the analytical solution. Halve the timestep size to observe the effect of truncation error.
