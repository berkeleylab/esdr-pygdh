########
Tutorial
########

This tutorial introduces Grid Discretization Helper through a series of examples.

.. toctree::
   
   introductory_remarks
   Python
   ODE
   ODE_in_time
   discretization
   PDE
   output
   nonlinear
   system
   postprocessing
   multiple_grids
   coordination
   two_dimensions
   multiply_connected
   compiling

