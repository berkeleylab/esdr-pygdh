***********************************************************
Discretization by the finite volume method in one dimension
***********************************************************

PyGDH assists the user in obtaining numerical solutions to differential equations (or systems of differential equations) defined on some domain. Currently, PyGDH is only designed to support the finite volume method. In this approach, the problem domain is divided into a finite number of non-overlapping volumes that, taken together, completely fill the domain. One may then seek an approximate solution in which a single value per volume is used to characterize the behavior of any one solution variable.

.. This approximation by a finite number of volumes and values is necessary because computers are only capable of storing and processing a finite number of values.

In the previous chapter, a diagram of a typical one-dimensional domain was shown:

.. image:: _static/one_dimensional_grid.png

More generally, the grid can have irregular spacing, as in:

.. image:: _static/irregular_one_dimensional_grid.png

Each volume is associated with a unique coordinate value, stored as the ``coordinate`` member of the corresponding ``Volume`` object. These coordinate values were given in the ``coordinate_values`` parameter value when the containing ``Grid`` object was created. As shown, volume boundaries are located midway between these coordinate values. Domain boundaries also coincide with volume boundaries for the volumes adjacent to the domain boundaries.

.. In the case of a one-dimensional domain, the ``Volume`` ``coordinates`` give the locations of the centers of each ``Volume``, for volumes in the interior of the domain. Boundaries between ``Volume`` objects are located midway between the ``coordinates`` of each pair of neighboring ``Volume`` objects. The ``coordinates`` of ``Volumes`` at the domain boundaries are the positions of the domain boundaries.

Methods representing discretized equations
==========================================
Governing equations are represented by user-defined methods defined in a ``Problem``-derived class, such as in the example:

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ODE
   :end-before: #### SPHINX SNIPPET END ODE

The discretized equations are first expressed in the form :math:`F(x,y(x)) = 0`, where :math:`x` represents the independent variable(s), and :math:`y(x)` represents the unknown dependent variable(s). Then the corresponding discretized equation method must then compute the value of :math:`F(x,y(x))` and store the appropriate number of scalar results (which must be consistent with the corresponding entry in ``equation_scalar_count``) as the elements of sequence passed as the second explicit parameter (called ``residual`` here). 

.. For the benefit of curious users, this approach was used because modifying values in an existing data structure in this way is substantially faster than returning multiple values.

As seen in the description of the user-defined ``assign_equations`` method in the previous chapter, each ``Volume`` object is associated with methods that describe the equations that govern the solution values over the corresponding volume. This approach provides the programmer with great flexibility, although it is inconvenient to define separate equation methods for each ``Volume``. Instead, PyGDH is designed in a way that makes it relatively simple to describe discretized governing equations without the explicit use of discretization formulas. This makes it possible to define a single governing equation method that can be applied to each ``Volume``, regardless of location.

A governing equation method must accept a unit object as its first explicit parameter. The unit object provides location information and typically defines operators that may be used by the governing equation method in place of explicit discretization formulas.

Discretization of a time-independent equation by the finite volume method
-------------------------------------------------------------------------
Under the finite volume method, discretized equations are obtained by integration over volumes of finite size. Consider the governing equation from the first example program, describing a one-dimensional domain:

.. math::
   \frac{d^2 y}{d x^2} = - \alpha^2 y(x).

This is rearranged into the form :math:`f(x,y(x)) = 0`, where :math:`x` represents the independent variable, and :math:`y(x)` is the solution:

.. math::
   f(x,y(x)) = \frac{d^2 y}{d x^2} + \alpha^2 y(x) = 0.

One then integrates both sides of the equation over a region with lower bound :math:`a` and upper bound :math:`b`, to obtain, in general:

.. math::
   \int_a^b f(x,y(x)) \ dx = \int_a^b 0 \ dx = 0

   F(x,y(x)) = 0

where

.. math::
   F(x,y(x)) = \int_a^b f(x,y(x)) \ dx

For the first example, this is:

.. math::
   F(x,y(x)) = \int_a^b \left(\frac{d^2 y}{d x^2} + \alpha^2 y(x)\right) \ dx = 0

   F(x,y(x)) = \left[\frac{d y}{d x}\right]_a^b + \alpha^2 \int_a^b y(x) \ dx = 0.

A method representing a discretized equation to PyGDH computes an approximation to the function :math:`F(x,y(x))` and stores the results. As each ``Volume`` is associated with only a single value of each solution field variable, interpolation over these values is typically needed to approximate integrals.

The integrals can be approximated in a number of ways. Interpolants of various orders may be used over volumes to approximate the spatial variation of fields, and the values of different factors within a single integrand may be obtained by different interpolation formulas--in the simplest case, a factor might be approximated by a constant value over the volume. Various methods of numerical integration may be used. The objects defined by PyGDH assists the user in representing the discretized equations, but the method of discretization is left completely to the user, and has consequences for solution speed, accuracy, and stability. 

In the example, the integrand is replaced by a constant, its average value over the volume (indicated by angle brackets). This yields

.. math::
   F(x,y(x)) = \left[\frac{d y}{d x}\right]_a^b + \alpha^2 \left<y\right> (b - a) = 0

As a very simple approximation, the average value of the integral will be replaced by the value of the integral at the center of the volume:

.. math::
   F(x,y(x)) \approx \left[\frac{d y}{d x}\right]_a^b + \alpha^2 y_{\mathrm{center}} (b - a) \approx 0
.. F(x,y(x)) \approx \left[\frac{d y}{d x}\right]_a^b + \alpha^2 y\left(\frac{a+b}{2}\right) (b - a) \approx 0

Representation as a method
^^^^^^^^^^^^^^^^^^^^^^^^^^
This final form is then represented as Python source code in the ``ode`` method. As required by PyGDH, this method accepts a unit object of interest (here, a ``Volume``) as the first explicit argument ``vol``, and an array ``residual``. The user-defined operators computed for each ``Volume`` in the ``define_interpolants`` method will be used to obtain the needed values of the independent variable and its derivatives. The governing equation method is then a direct translation of the approximate form for :math:`F(x,y(x))` into a Python expression:

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ODE
   :end-before: #### SPHINX SNIPPET END ODE

The local variable ``y`` is created for clarity. The data structure that it references will be discussed in the next chapter, but ``y`` itself is a one-dimensional array that holds one value of the independent variable for every ``Volume`` in the domain. This is the form of the required input for the interpolation routines. These were defined and given the names ``dx_right``, ``dx_left``, and ``interpolate`` in ``Volume.define_interpolants``. As described in the next section, these operators approximately compute the derivatives at the right and left volume boundaries, and the value of the solution variable at the center of the volume.

The ``self.alpha`` variable, as discussed in an earlier chapter, is a member of the user-defined ``Problem``-derived object. Its value is assigned directly from the ``alpha`` argument variable in the ``__init__`` method. This value was stored as an object member because the argument variable of a method does not exist after a method has finished running. An object member, on the other hand, remains accessible as long as the object exists, and is accessible by the other object methods.

The addition, subtraction, multiplication, and division operators have the same precedence, or order of operations, as in algebra. In Python, exponentiation is represented by the ``**`` operator. It has a higher precedence than multiplication and division operators. The exponent is an integer, rather than a floating-point number (such as ``2.0``) because exponentiation with an integer power is performed more quickly (through multiplication), rather than though calculation by logarithms.

Since the user-defined ``Volume`` class is derived from ``pygdh.Volume`` as defined by PyGDH, each ``Volume`` object has an automatically-defined member named ``volume`` which gives the size of the associated volume in the dimensional space represented by the ``Volume`` object. In one dimension, this ``volume`` is just the length associated with the ``Volume`` object (here, :math:`b-a`), whereas in two dimensions, ``volume`` is the area associated with the ``Volume`` object.

Finally, in the ``assign_equations`` method, ``ode`` was associated with a single scalar value in the ``equation_scalar_counts`` dictionary. This was appropriate, because the solution is governed by a single scalar equation at any point in the domain. Therefore, the array associated with the second explicit parameter ``residual`` (which is really a portion of a much larger array) contains only one element. The first element of an array has index ``0``, so the result of calculating :math:`F(x,y(x))` is stored in ``residual[0]``. In order to keep the line length to under 80 characters, the entire expression is enclosed within parentheses, which allows the expression to be split over multiple lines.

.. this is an easy place to make a mistake by leaving off the index operator

.. In the example problem, the unit objects belong to the user-defined ``Volume`` class

.. include: : ../../pygdh/examples/ODE/ODE_annotated.py
..  :lines: 4-20

.. The details of defining interpolants will be left to a later section. For now, it is enough to note that a unit class can define operators with names of the user's choosing.

.. Also, as shown in the previous examples, all of these methods must be included as dictionary keys in the ``equations_scalar_count`` dictionary of the user-defined ``Problem``-derived class, with the number of returned scalar values stored as corresponding dictionary values.

.. It is associated with an array of values of the solution variable named ``'y'``, and an index into this array is a reference to the integer label of a ``Volume`` object. 

Interpolation in one dimension
------------------------------
Interpolation is frequently needed because for a given solution variable, only one value is used to characterize the solution behavior associated with any given ``Volume`` object. The ``Interpolant`` objects can calculate interpolants (and derivatives of a specified order) to a specified accuracy, at particular positions. These are typically defined in the ``Volume.define_interpolant`` methods. This allows formulas to be determined once, when they are automatically created as part of a ``Grid`` object, and then used repeatedly in the solution process. By computing and storing the interpolation formulas once, before any other equations are solved, this approach saves a very significant amount of computing time later.

Normalized relative position
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When defining interpolation operators and describing governing equations, spatial position is indicated to PyGDH in terms of a coordinate system that is local to the ``Volume`` of interest. In one dimension, the normalized relative position ranges from ``-1.0`` to ``1.0`` over a one dimensional region associated with a ``Volume`` object. The "left" boundary, associated with a smaller coordinate value, corresponds to ``-1.0``, and the "right" boundary corresponds to ``1.0``.

For an ``Volume`` object on the interior of a ``Grid``, the ``coordinate`` position of the associated ``Volume`` corresponds to ``0.0``. Since this position is not necessarily located midway between the "left" and "right" boundaries (see the diagram of an irregular grid), the true distances associated with positive and negative normalized relative positions of equal magnitude may not be equal. However, the true positions vary linearly with the normalized relative positions in their respective directions away from ``0.0``.

.. image:: _static/irregular_one_dimensional_grid.png

In one dimension, for a ``Volume`` adjacent to a domain boundary, the normalized relative position ``0.0`` corresponds to the midway point between the "left" and "right" boundaries, which is different from the ``coordinate`` position. The domain boundaries coincide with the ``Volume`` boundaries.

.. ``-1.0`` refers to the "left" boundary of the volume, corresponding to the smallest coordinate value. ``1.0`` refers to the "right" boundary, and ``0.0`` refers to the "center" of the volume, located at the coordinate position of the volume (see earlier diagrams), and which is not necessarily its geometrical center.

Defining interpolants
^^^^^^^^^^^^^^^^^^^^^
In order to facilitate position-independent representations, it is helpful to define consistently-named methods (across multiple ``Volume`` objects) that compute a certain type of result within the corresponding volume. For example, one might wish to compute an approximate derivative at the right boundary of each ``Volume``. The appropriate formula will be different for each ``Volume``, as each ``Volume`` has a different set of neighbors, but each ``Volume`` can store the appropriate formula as a method with the same name. In this way, the governing equation methods can be written very generally, as functions of the ``Volume`` object of interest, and the appropriate derivative formula can be accessed as a consistently-named method of that object. This technique was used in the first example program to define a single governing equation method, ``ode``, for use throughout the problem domain and without the need to explicitly use discretization formulas.

These methods should be defined in the ``define_interpolants`` method within the ``Volume`` class definition:

.. The ``define_interpolants`` method will typically include a for loop, as in this example:

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DEFINE INTERPOLANTS    
   :end-before: #### SPHINX SNIPPET END DEFINE INTERPOLANT        

..  Interpolation is necessary in the finite volume method; for a given solution variable, this method uses a single value per volume to approximate the true behavior of the solution variable over the region. Typically, this single value is the value at the unique coordinate value with which the volume is associated. As one increases the number of volumes used to represent a given domain, the accuracy of the approximate solution typically increases, but at the cost of additional computing time.


.. In this loop, the body is executed once for each element in ``self.volume_from_number`` (each of which corresponds to one ``Volume`` of the domain). On successive loops, the ``vol`` variable is assigned successive element values.

Here, three user-defined members of the ``Volume`` object are created. These new members store functions (functions are objects, too) that compute the first spatial derivatives at the right and left boundaries, and the interpolated function value at the center of the corresponding volume.

In this way, each ``Volume`` object has methods named ``dx_right``, ``dx_left``, and ``interpolate``, which have consistent meanings with respect to the corresponding spatial regions. Other methods may use these routines without detailed knowledge of the underlying numerical details.

.. As with the complicated expressions encountered in the previous example, the expressions that produce the interpolation functions associate from left-to-right; the first assignment statement may be rewritten as

.. ::

.. 	vol.dx_right = ( ( vol.define_interpolant ) (1.0,1,1) ) . value

The ``define_interpolant`` method built into the ``pygdh.Volume`` class creates and returns an ``Interpolant`` object, which behaves according to the provided parameter values. While an object of this class contains a method named ``value`` which actually computes the value of interest, it is unwieldy to write expression such as ``vol.dx_left.value(y)``. Instead, the ``Interpolant`` objects allow a special notation, in which the values of interest can be computed by appending the function call or indexing operators to the Interpolant object name, as in ``vol.dx_left(y)`` or ``vol.dx_left[y]``. The former has been used the the ``ode`` method.

The ``define_interpolant`` method requires three parameter values. The first of these is the normalized relative position in the corresponding volume at which the value is to be computed.
.. This requires some explanation, which is left to the next subsection.

In one dimension, the second parameter value gives the order of the derivative to be computed. The interpolated solution function value is ``0``, the first spatial derivative is ``1``, and so on.

The third parameter value specifies the accuracy of the interpolation formula. For those familiar with the derivation of difference formulas, this is the exponent of the highest-order term in the Taylor series expansion that is used in the coefficient calculations. This value should be at least as large as the order of the derivative to be computed. For the first example, it is safe to take this number to be ``2`` for the interpolated function value, and ``1`` for a first spatial derivative.

Suggested exercises
===================
1. Solve the original governing equation, but for :math:`1 < x < 2`, and with the boundary conditions

.. math::
   y(1) = 0

   y(2) = 1

Do you get what you expect?

2. Modify the original example code to solve

.. math::
   \frac{d^2y}{dx^2} = -\alpha^2 y + \beta x
   
   y(0) = 0

   y(1) = 2

for :math:`\alpha = \beta = 1` and :math:`0 \le x \le 1`. Hint: As a rough approximation, use

.. math::
   \int_a^b \beta x \ dx \approx \beta x_{\mathrm{center}} (b - a),

where, in this one-dimensional spatial problem, :math:`x_{\mathrm{center}}` can be obtained as the ``coordinate`` member of a ``Volume`` object is a scalar giving the position of the grid point associated with the ``Volume``. 

The results should agree well with the analytical solution

.. math::
   y(x) = \frac{\sin(\alpha x)}{\sin(\alpha)} + \frac{\beta x}{\alpha^2}

.. 2. Solve the original example problem on an irregular grid, such as that shown in the diagram.

3. Modify the original example code to solve

.. math::
   x^2 \frac{d^2 y}{dx^2} + 2 x \frac{dy}{dx} - 2 y = 0

   y(1) = 1

   y(2) = 1

for :math:`1 \le x \le 2`. Note the change in the spatial domain. 

Hint: Use rough approximations such as

.. math::
   \int_a^b x \frac{dy}{dx} \ dx \approx x_{\mathrm{center}} \int_a^b \frac{dy}{dx} \ dx.

The results should agree well with the analytical solution

.. math::
   y(x) = \frac{3 x}{7} + \frac{4}{7 x^2}.
