******************************************
A nonlinear problem and validation testing
******************************************

PyGDH was originally created for the purpose of solving nonlinear systems, and uses a nonlinear solver from NumPy by default.  The approach used to solve a nonlinear problem with PyGDH is the same as that used with the linear problems from the previous examples; the discretized equation methods may be linear or nonlinear in the solution variable values.  The first half of this chapter will use PyGDH to solve a nonlinear problem.

The second half of this chapter will discuss numerical validation.  Validation of solutions should be considered a necessary step in solving problems because errors can arise throughout the solution process--the original problem formulation, the discretized equations, and the computer implementation (including the underlying libraries like PyGDH) are all possible sources of error.  

A nonlinear problem
===================

This example extends the previous example to large (and nonlinear) displacements within a spherically-symmetric domain (ignoring any inconsistency in using  Hooke's law in conjunction with large-deformation mechanics). The code is included in the PyGDH archive as ``pygdh/examples/nonlinear.py``.  Since displacements may be large, one must be careful to specify whether a function of position (actually, a function associated with a particular material "particle") is written in terms of the present, displaced position of the material particle, or the initial, undisturbed position of the particle.  The first of these is the "spatial", or "Eulerian", description, and the second is the "material", or "Lagrangian", description.

PyGDH does not directly support calculations on domains that vary in shape or size over the course of a calculation; the coordinate values of all grid lines cannot be changed.  However, it is still possible to solve problems on domains that are not fixed, simply by formulating the problems appropriately--all that is necessary is to map the changing domain to a fixed domain.  In this case, one way to achieve this is to transform equations written in the spatial description to equations written in the material description.  The resulting equations are then solved as functions of position on the original domain.

It turns out that performing this transformation for the stress balance gives an equation with the same form:

.. math::
   \frac{\partial t_r}{\partial r} + \frac{2}{r} (t_r - t_t) = 0

where the symbol :math:`r` now refers to the initial radial position of the material particle in question, and the stress components take a more complicated form:

.. math::
   t_r = \left(1+\frac{u}{r}\right)^2 \left\{\lambda (\mathrm{tr} \mathbf{E^a}) + \mu \left(1 - \frac{1}{\left(1 + \frac{\partial u}{\partial r}\right)^2}\right) \right\}

   t_t = \left(1+\frac{u}{r}\right)^2 \lambda (\mathrm{tr} \mathbf{E^a}) + \left(1+\frac{\partial u}{\partial r}\right) \mu \left(1+\frac{u}{r} - \frac{1}{1 + \frac{u}{r}}\right)

   \mathrm{tr} \mathbf{E^a} = \frac{3}{2} - \frac{1}{2\left(1 + \frac{\partial u}{\partial r}\right)^2} - \frac{1}{\left(1 + \frac{u}{r}\right)^2}

Under the assumptions that :math:`|u/r| << 1` and :math:`|\frac{\partial u}{\partial r}| << 1`, one may show that the small-deformation equations may be recovered.

The discretized equation retains the same overall form as in the previous example:

.. math::
   t_r(r_1) - t_r(r_0) + 2 \left<\frac{t_r - t_t}{r}\right> (r_1 - r_0) = 0

where again, the angle brackets indicate an average over the interval--for simplicity, this will be approximated by the value of the enclosed quantity at the midpoint between :math:`r_0` and :math:`r_1`.  No equation will be needed at the origin, because the solution value at the origin will be given as before.

The implementation will be similar to the small-deformation implementation, with two significant additions. First, the ``calculate_global_values`` method is modified to include the large deformation calculations described above:

.. include:: ../../pygdh/examples/nonlinear/nonlinear_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START CALCULATE GLOBAL VALUES
   :end-before: #### SPHINX SNIPPET END CALCULATE GLOBAL VALUES	      
		
Second, a method named ``set_initial_guess`` is defined in the ``Grid``-derived class in order to aid the nonlinear solver. PyGDH uses an iterative solver provided by the SciPy package. In general, iterative solvers of this type are not guaranteed to find the solution to a system of nonlinear algebraic equations, such as those produced by PyGDH. Providing a suitable initial guess of the solution can be critical to success of the solver routine. The ``set_initial_guess`` method gives the user an opportunity to set the "initial" solution data (associated with position ``0`` in the ``field`` deque) to an initial guess:

.. include:: ../../pygdh/examples/nonlinear/nonlinear_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START SET INITIAL GUESS        
   :end-before: #### SPHINX SNIPPET START SET INITIAL GUESS        

In this case, the solution to the linear, small-deformation problem turns out to be sufficiently close to the solution for the large-deformation problem to enable convergence of the iterative solver.

The displacement differs somewhat from the small-deformation solution, but the stress components are almost the same:

.. image:: ../../pygdh/examples/nonlinear/nonlinear_u.png

.. image:: ../../pygdh/examples/nonlinear/nonlinear_stress.png

Validation
==========

Finding a numerical solution to the discretized equations, as expressed to FVM builder, does not guarantee that the mathematical problem that one intended to solve has indeed been solved.  Discretization schemes may be unacceptably inaccurate, and discretized equations and computer programs can contain errors; this is why results must be checked.

In the previous examples, the numerical results were plotted against analytical solutions.  However, analytical solutions are frequently unavailable, especially for nonlinear problems--this is why numerical techniques are often needed.  Sometimes, a limiting case of a difficult problem will yield an analytical solution for comparison with numerical results, but errors in the solution to the more general problem may go undetected.

One might instead perform additional numerical calculations to check a numerical solution.  For example, one might check how well the numerical results satisfy other discretized forms of the original problem.  While this cannot guarantee that the solution is free of error, it is helpful for detecting errors in many situations.

The methods for customizing output, as introduced in the previous chapter, can be used for this validation process.  Array variables holding one element per ``Volume`` in a ``Grid``, representing the error for some field one that ``Volume``, are defined in the ``define_field_variables`` method.  These variables are then marked for output in the ``set_output_fields`` method.  Finally, the ``process_solution`` method is defined.  This method is automatically called after a solution is computed; the computed solution is used to evaluate an alternative set of discretized equations, and the results are stored in the arrays created for error output.  The results are automatically saved to the output files along with the solution field variable values.

One can obtain an alternative set of discretized equations in any number of ways.  One might revisit the finite volume formulation, making more or less accurate approximations in various terms of the governing equations.  Or one could use a different approach to discretization.  For example, the "finite difference method" approximates an equation with values at a finite set of points.  Derivatives in time and space are approximated by "finite difference" formulas that incorporate information from neighboring points.  Although the finite volume method also makes use of finite difference formulas, solution values in the finite volume method represent the behavior of functions throughout volumes, and integration over volumes can lead to very different discrete forms of the governing equations than would be obtained by the finite difference method.

Discretized equations for the finite difference method are typically obtained by replacing spatial derivatives in the differential equation with finite difference formulas that approximate these spatial derivatives.  Time discretization is performed in the same way as with the finite volume method.

The alternative set of discretized equations can be expressed to PyGDH in a similar way to the original set of discretized equations.  As before, PyGDH provides methods that approximate spatial derivatives.  Time discretization is the responsibility of the user, although PyGDH manages storage of past solutions needed in time-differencing formulas.

Although it is tempting to use intermediate values computed by ``calculate_global_values`` in this validation process, these intermediate calculations may also contain errors.  For this reason, it is best to recompute any intermediate values needed for the alternative discretized equations, with a completely different body of code that is not copied from the finite volume method implementation.  While this doesn't eliminate the possibility of error in the intermediate calculations, it is unlikely that the same minor errors will be made in both cases.

The terms of the discretized equation are rearranged as in the finite volume equations used to obtain the solution, with all nonzero terms collected on one side.  The ``process_solution`` implementation, including the copying of computed stresses to the output arrays is:

.. include:: ../../pygdh/examples/nonlinear/nonlinear_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROCESS SOLUTION
   :end-before: #### SPHINX SNIPPET END PROCESS SOLUTION

On a ``Volume`` for which the value of the solution field is prescribed, there is no reason to evaluate the alternative form of the governing equation--the numerical method has no freedom to modify the value associated with the present ``Volume``, and no equation was ever evaluated on this ``Volume``.  In this example, the value of the displacement at the origin is prescribed, so the error at the ``Volume`` at the origin is simply set to zero.

The error in satisfying the governing equation, as discretized by the finite difference method, when using the numerical solution computed by the finite volume method, is plotted below:

.. image:: ../../pygdh/examples/nonlinear/nonlinear_validation.png

A detailed analysis of the expected error magnitude is rather involved, but errors computed by this method should be much smaller than the contribution from the smallest term in the discretized equation.  Where terms cancel, as in this particular case, terms contained within these terms should be examined.  In this case, the stresses have the magnitude of the given pressure, and the radial distances have a maximum value of 1, so it is clear that the computed errors are reassuringly small.  
		
Suggested exercises
===================
1. Perform a similar validation for the original ODE example.
