Background and Acknowledgements
===============================

This work was supported by the Assistant Secretary for Energy Efficiency and Renewable Energy, Office of Vehicle Technologies of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231 under the Batteries for Advanced Transportation Technologies (BATT) Program and the Advanced Battery Materials Research (BMR) Program.

PyGDH was initially developed in the course of BATT-funded and BMR-funded research by Kenneth F. Higa under the guidance of Venkat Srinivasan, within the Energy Storage and Distributed Resources Department (now the `Energy Storage and Distributed Resources Division <http://eetd.lbl.gov/about-us/organization/energy-storage-and-distributed-resources>`_) in the Environmental Energy Technologies Division (now the `Energy Technologies Area <http://eetd.lbl.gov/>`_) of `Berkeley Lab <http://www.lbl.gov>`_ (Lawrence Berkeley National Laboratory), a `U.S. Department of Energy <http://www.energy.gov/>`_ laboratory operated by the `University of California <http://www.universityofcalifornia.edu/>`_. "Bringing Science Solutions to the World" is Berkeley Lab's mission.

PyGDH continues to receive updates from KFH under supervision of Vincent Battaglia at Berkeley Lab, supported through funding from the Vehicle Technologies Office (VTO) and Advanced Manufacturing Office (AMO) within the U.S. Department of Energy.

KFH thanks the developers of the `Python <http://www.python.org/>`_ programming language and its standard libraries, the developers of the `NumPy <http://numpy.scipy.org/>`_, `SciPy <http://www.scipy.org/>`_, `h5py <http://www.h5py.org/>`_, and `HDF5 <http://www.hdfgroup.org/>`_ libraries (and the underlying libraries), the developers of the `Sphinx <http://sphinx.pocoo.org/>`_ Python Documentation Generator, the `GNUPLOT <http://www.gnuplot.info/>`_, `matplotlib <http://matplotlib.org>`_, the `Cython <http://cython.org>`_ compiler, LaTeX, and developers throughout the Open Source community in general.

KFH also thanks Ashlea Patterson, Sunil Mair, Anne Suzuki, Fabiola Lopez, Andrew Veenstra, Tiffany Ho and Fiona Stewart for improving the user experience by providing valuable feedback on early versions of the PyGDH documentation and software.

While great effort has been made to eliminate bugs, they undoubtedly exist. The PyGDH source code is open for all to examine. Users are encouraged to send bug reports and fixes to KFH <KHiga AT LBL DOT gov> for inclusion in the library.

In the spirit of scientific openness and in the interest of strengthening public confidence in the work of the scientific community, KFH respectfully encourages researchers receiving public funding to make their source code publicly available when publishing scientific results. It is hoped that PyGDH will encourage this by keeping problem-specific source code to a reasonable size. KFH thanks Greg Wilson of `Software Carpentry <http://software-carpentry.org>`_ for promoting openness in software development within the scientific community and providing the inspiration to release this work as open-source software.

Some of the concepts underlying PyGDH are described in the excellent introductory text:

Patankar, Suhas V. *Numerical Heat Transfer and Fluid Flow*, Hemisphere, 1980.

KFH expresses appreciation for the work of the Journal of Open Source Software and thanks reviewers Jamie J. Quinn and Wei Zhao for their detailed and helpful advice in improving this package, and editor Melissa Weber Mendonça for guiding this submission through the peer review process. 
