#!/bin/sh
VERSION=`cat ../../version.txt`
YEARS=`cat ../../years.txt`
OWNER=`cat ../../owner.txt`
sed "s/VERSION/${VERSION}/;s/YEARS/${YEARS}/;s/OWNER/${OWNER}/;/^.*sphinx\.ext\.autodoc/a\\    'sphinx.ext.napoleon'," conf_template.py > conf.py
cat license_template.rst ../../LICENSE > license.rst
cat copyright_notice_template.rst ../../copyright_notice.txt > copyright_notice.rst
sed "s/VERSION/${VERSION}/" installation_template.rst > installation.rst
