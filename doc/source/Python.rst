*************************
An introduction to Python
*************************

This chapter introduces a number of Python concepts, primarily through an example program that will be examined in detail in later chapters. Readers seeking a deeper understanding of the Python language are encouraged to read the `official Python documentation <https://docs.python.org/>`_.

Running Python programs
=======================
Programs written in the Python programming language are stored in the form of text files with names ending in ``".py"``. The contents of these text files are called "source code." The Python system reads the source code and directs the computer to act according to the program instructions. Many of the code samples included here appear in our first example program, located at ``pygdh/examples/ODE/ODE.py`` in the PyGDH archive.

In order to execute source code written in the Python language, a user must ask the computer to start the Python system and direct it to read and carry out the instructions in the Python source code. Typically, both of these steps may be achieved by entering, for example,
::

	python program.py

at a command-line interface (see :ref:`command-line`, if needed), where ``program.py`` is the name of the Python program file. The Python system stops once the program has successfully finished, or if it detects an error in the program instructions. Program output might be displayed on the screen, or saved in computer files.

While Python programs are typically saved as text files so that they can be run and edited at will, one can also start the Python system (typically, by typing ``python`` in the system's command-line interface) and enter statements in the Python language manually. This interactive mode can be useful for exploring the Python language. From the Python interpreter, the ``import`` statement can be used to load and execute a Python source code. To run the program in a file named ``program.py``, stored in the current directory, one may type
::

   import program

To exit the Python system, one may type ``exit()`` or ``quit()``

Python program structure
========================
Python programs consist of simple statements that can be written in a single line, and compound statements occupying multiple lines. The Python system acts on program statements sequentially, whether they are taken from a text file or entered in interactive mode.

Successive simple statements share the same indentation. A compound statement involves lines that control its overall behavior, with the same indentation as preceding simple statements. Each of these controlling lines can be followed by sequences of associated statements, which are further indented.

This example of an "if-statement" demonstrates the use of indentation::

     # Preceding the if-statement  
     print 'This is first'

     # Beginning of the if-statement
     if True:

     	print 'This is second'

	# Nested if-statement
	if False:
	   print 'This is never executed'
	else:
	   print 'This is third'

     # "Else" clause of the if-statement
     else:

        print 'This is never executed'

     # Exited from the if-statement
     print 'This is fourth'

The lines beginning with ``if`` and ``else`` control the compound statement, and a second if-statement has been nested in the outer if-statement. The indentation indicates which statements are part of the compound statement, and which are not.

The character ``#`` indicates the beginning of a comment. The Python interpreter ignores all characters that follow on the same line. It is good programming practice to provide comments wherever they are likely to be helpful. 

A note on variable assignment
=============================
Upon encountering a line with a variable name, followed by an assignment operator (an equal sign) and a value, as in::

     a = 1

the Python system will create a variable with this name ``a`` and assign to it the given value ``1``, if the variable has not yet been defined, or if a variable of this name has already been defined, it will be reassigned the value ``1``.

The value on the right side is evaluated before the assignment is made. Therefore, it is legal to write statements such as::

    a = a + n

which modifies the value of ``a`` according to the current value of ``a`` (variable names evaluate to their assigned values). In fact, this sort of statement is so common that there is an abbreviated form::

      a += n

Note that Python is "case-sensitive", which means that capitalization matters. For example, ``a`` and ``A`` are completely distinct names.

Real numbers, floating-point numbers, and integers
==================================================
Numerical calculations on computers are typically done with floating-point numbers, which are approximations to real numbers. The error in this approximation can sometimes produce significant "round-off" errors. Error-checking will be discussed in a later chapter.

Computers typically work with both floating-point numbers and integers. In Python, ``1.0`` and ``1e0`` (Python's notation for :math:`1 \times 10^0`) are floating-point numbers, while ``1`` is an integer, and the two types of quantities are distinct. Integers take up less space in computer memory and mathematical operations on integers are faster than on floating-point numbers. However, programmers should be cautious, because using integers in numerical programs can sometimes lead to unexpected and undesirable results. For example, ``1 / 2`` might evaluate to ``0`` because division of one integer by another may be defined to produce another integer.

.. , but will not discuss the language in detail. However, there are some critical points that should be highlighted:

.. #. Integers and floating-point numbers are distinct. For example, ``1` and ``1.0`` do not have the same meaning to the Python interpreter. 

.. #. As with many other computer languages, the operator that assigns a value to a variable is different from that which asserts equality of two values, even though the equal sign is typically used for both purposes in mathematics. For example, ``a = 1`` assigns the value ``1`` to the variable ``a``, but ``a == 1`` is a test of "equality" and has a value of ``True`` or ``False``.

.. #. The very notion of "equality" can be a complicated question in the context of computing. Users are strongly advised to read the official Python documentation if checking for equality of anything that is more complicated than integer scalars, although for the purpose of using PyGDH, this situation is unlikely to occur.

.. #. But making an assignment to an element of a data structure named by a variable sets the value of the element, rather than changing the variable's assignment.

.. #. Objects may contain other objects as members. In order to refer to the member object ``grid``, contained within the object ``self``, the "dot" operator is used between the names of the container object and the containing objects, as in ``self.grid``

.. #. By convention, the variable ``self``, as used by a method belonging to an object, refers to that object. Methods belonging to an object may need to refer to other members of the same object. Fellow object members must still be referenced explicitly--the previous example of ``self.grid`` might be used by a method belonging to ``self`` to reference another member, ``grid``.

.. #. Variables can be created by simply writing an assignment statement, such as ``volume_count = 11``, within a function. Variables created within a function are generally recognized only within the definition of that function. An important, frequently-used exception to this is that assignments can be made to members of objects that exist independently of the function. For example, ``self.alpha = alpha`` creates the ``alpha`` member of the ``self`` object if necessary, and gives it the value held by the (local) variable ``alpha``. The ``self`` object, to which the method belongs, already exists and will continue to exist after the method has finished running.

.. #. There are several different types of data structures that associate indices with an object. In most cases, the object can be accessed by following the data structure object name with the indices, in square brackets. For example, for a data structure object ``grid``, ``grid[0]`` refers to the object associated with index ``0``.

..  #. A ``str``, or string, associates integer indices with characters. Strings are "immutable", which means that their contents cannot be changed. A string can be created by enclosing a sequence of characters in single quotes or double quotes; as an example, ``'GNUPLOT'`` is a string of seven characters.

..   #. A ``list`` is a type of data structure that associates integer indices with objects. The first object stored in a list has index ``0``. If there are ``N`` objects in a list, the last object has the index ``N-1``, and can also be referenced by the index ``-1``. A ``list`` can be constructed by enclosing, in square brackets, a comma-separated list of objects. For example, ``['GNUPLOT']`` is a ``list`` containing a string as a single object.

..   #. A ``tuple`` is like a ``list``, but its contents cannot be modified--like strings, they are immutable. They can be constructed by enclosing a comma-separated list of objects in parentheses. In order to distinguish these parentheses from those specifying ordering of operations, a comma must be placed after the single object when creating a ``tuple`` containing only a single object.

..   #. NumPy is a Python library for matrix calculations, and used extensively in PyGDH. A NumPy ``ndarray`` is is a multidimensional array, and associates a comma-separated list of integer indices (one for each dimension) with objects of a single type, typically floating-point numbers. As with a ``list``, the first elements in a given dimension are associated with index ``0``, and the last elements can be referenced by the index ``-1``.

..   #. A ``dict``, or dictionary, is a data structure that can take many different types of objects as generalized indices, and associate them with other objects. A dictionary can be created by enclosing, in curly brackets, a comma-separated list of pairs of objects, written as the index, or "key" object followed by a colon and the "value" object. For example, the following dictionary that associates strings with a ``list`` (containing an object named ``volume_coordinates``)  and two integers
.. ::

..   { 'coordinates_list': [volume_coordinates], 'field_count': 1, 'past_solution_count': 0 }

Strings
=======
Variables can be associated with non-numerical data, such as strings. A string is an ordered sequence of characters (such as letters, digits, punctuation marks, and spaces), and is typically used to represent human-readable information. A literal string may be created in a few different ways; the simplest involves enclosing a sequence of characters within two single quotation marks, or two double-quotation marks, as in::

   filename_root = 'ODE'

A note on functions
===================
Python functions are sequences of Python instructions that are performed, or "called", as a unit when needed. Typically, a Python function is called by writing the function name, followed by one or more parameter values in parentheses. A function may yield, or "return", a result, which can be understood to take the place of the function call expression in the calling statement.

For example, one might have a function named ``add`` which takes two numbers as parameter values and returns their sum. If this function is called in the statement:

::

   s = subtract(1,2)

the Python interpreter calls the function ``subtract`` with ``1`` and ``2`` as the first and second parameter values. The function ``subtract`` executes, performing a calculation, and returns the result ``3``. The Python interpreter then evaluates the original assignment statement, but as if the function call expression had been replaced by its returned result:

::

   s = -1

By recognizing frequently-used patterns of instructions as such and grouping them into functions, programmers can avoid repetition and the errors associated with repetition. Grouping instructions into functions with clearly-defined tasks also helps to keep programs organized, reducing programmer effort.

Before their first use, functions are defined in function definitions. The ``subtract`` function might have been defined in the following way:

::

   def subtract(a,b):
       return a - b

The first line specifies the function name, and indicates that it is to be called with two parameter values. Unlike many other programming languages, Python does not ask the programmer to specify the type of argument expected (integer, floating point, etc.). The body of the function follows, indented relative to the first line. Any statements in this body are executed in order. Within this body, the argument variables specified in the first line of the function definition are initially assigned the parameter values given in the function call expression, in the order given (in the example above, the parameter order is not interchangeable). These variables exist only while this function is running, and they are distinct from any other variables of the same name that may appear elsewhere in the program.

A function finishes running if the end of the function body is reached, or if a "return statement" is encountered. As with other compound statements, the function definition ends just before the next statement with the same indentation as the first line of the function definition (or at the end of the file if there are no further statements). Return statements consist of the word ``return``, followed by an expression is evaluated by the Python interpreter and returned to the calling statement as the function's "return value".

A brief discussion on object-oriented programming
=================================================
Before proceeding further, "object-oriented" programming concepts should be discussed because of their importance in PyGDH and the underlying Python programming language. This section is meant to provide a brief introduction to these concepts.

"Object-oriented programming" is an approach to organizing computer programs, in which closely-associated data and computer instructions are organized into entities called "objects". Many readers may be more familiar with "procedural programming", in which computer instructions are organized into "procedures", "functions", or "subroutines" that manipulate data. Procedural programming is used within object-oriented programming, with objects providing an additional layer of organization by grouping instructions with related data. In the language of object-oriented programming, the data and computer instructions contained within an object are often called "members" of the object, and the instructions (in the form of functions) are called "methods". 

One benefit of this approach is that the potentially complicated interactions among data and computer instructions can be contained within a single object. This is called "encapsulation", and this practice allows programmers to make use of an object's capabilities without detailed knowledge its internal details.

A "class" is a category, or "type", of object. A class definition tells the computer how instructions and data are grouped to form a particular type of object. A program might simultaneously make use of multiple objects that "belong" to the same class, which means that they all have the same basic structure as specified in the class definition. However, different objects belonging to the same class may contain different data and may possess different features in addition to those specified in their shared class definition.

A "derived" class is a class that is defined as a modification of an existing class; the existing class from which it is derived is called the "base" class. An object created according to the derived class is also an object of the base class type, as it possesses the same basic qualities. In this way, the properties of the base class are said to be "inherited" by the derived class. 

The structure of PyGDH fits nicely with these object-oriented concepts. PyGDH offers a flexible but simple approach to solving equations by helping users to express discretized equations in the form of a Python program. Users describe mathematical problems by defining derived classes, building on base classes defined by PyGDH. The structure inherited from the base classes helps users to express the problem descriptions.

Classes merely describe objects; in order to obtain numerical solutions with PyGDH, objects of the derived class types are then created (with storage space for data allocated in the process) and directed to obtain numerical solutions to specific problems.

Methods and object initialization
---------------------------------
Methods are the computer instructions associated with objects, expressed as functions defined as part of a class. They typically appear as function definitions indented relative to their corresponding class statements. Among other things, they can serve as mechanisms for interacting with objects of that class.

All methods must be defined to accept at least one parameter, which by convention is called ``self``. When a method accepts multiple parameters, the ``self`` parameter must be the first of these. Whenever a method is called, the object to which the method belongs is implicitly and automatically passed to the method as this first argument ``self``. Values explicitly specified in method call expressions determine, in order, the values of the remaining argument variables in the method definition.

Here's an example of a class definition with a single method defined::

  class Volume(pygdh.Volume):

      # Defining this method gives users an opportunity to define their own
      # mathematical operators, for later use in describing governing equations.
      def define_interpolants(self):

          # Operator for estimating first derivative at left boundary of volume
          self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
          # Operator for estimating function value at the volume center
          self.interpolate = self.default_interpolant(0.0, 0, 2)
        
          # Operator for estimating first derivative at right boundary of volume
          self.dx_right = self.default_interpolant(1.0, 1, 1)
  
The first line of the class definition names the class, and subsequent indented lines describe the variables and methods that are class members. Every object of this class contains an independent set of variables with the specified names, and can be accessed through the specified methods.

The ``__init__`` method has special meaning to Python. This method is automatically called immediately after a new object of the class type is created, but before the new object is returned. This gives the programmer the opportunity to prepare the new object for use. 

An object of a class is created by using an expression in which the class name is used like a function name, returning a new object of the class type. As with any other method, the (newly-created) object is itself implicitly passed as the ``self`` parameter, and the remaining argument variables are assigned the explicitly provided parameter values. For instance, in the example program, an object of the ``ODE`` class is created, and two parameters are explicitly passed::

  problem = ODE([grid_obj], 1.0)
   
.. In the ``__init__`` method of the ``Domain`` class, the variable ``unit_count`` is given the value provided as the first parameter, ``11``, and the variable ``alpha`` is given the value provided as the second parameter, ``1.0``. These variables exist only while the ``__init__`` method is running. The new ``ODE`` object is returned and assigned to the variable ``problem``. The ``problem`` variable is defined outside of any function or class, and so continues to exist as long as the program in ``ODE.py`` is running.

Object members and the dot operator
-----------------------------------
Now that an object of the ``ODE`` class has been created, one can interact with its members by using the "dot" operator. For example, writing::

    problem.set_boundary_values()

instructs the Python interpreter to find and run the ``set_boundary_values`` method defined in the class to which ``problem`` belongs. Like all methods, ``set_boundary_values`` implicitly receives its containing object as its first argument, called ``self``. This method was defined to accept only this implicit argument, so in the calling expression, its name is followed by an empty pair of parentheses containing no explicit parameter values. However, the parentheses are needed to indicate that the Python interpreter is to call the named method, rather than return the method (which is itself an Python object).

Data members are also accessible with the dot operator. For example, within a method, the assignment::

  self.alpha = alpha

is understood in the following way: the expression ``self.alpha`` refers to the entity ``alpha`` that is contained within the entity ``self``. As noted earlier, ``self`` refers to the object (of class ``ODE`` in the example program) to which the present method belongs. As the object ``self`` does not already possess a member named ``alpha``, the Python interpreter automatically creates a new member variable with this name; this is typically how data members belonging to an object are created. Had ``self.alpha`` already existed, it would have been reassigned this new value.

Unlike in some other object-oriented languages, the name of an object member must always be used with the dot operator in order to access the object--Python does not offer a shortcut in definitions of methods belonging to the same class.

Scope
-----
The parameter variable ``alpha`` in the following the ``__init__`` method in the example program

.. include:: ../../pygdh/examples/ODE/ODE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROBLEM INIT
   :end-before: #### SPHINX SNIPPET END PROBLEM INIT
	     
is distinct from ``self.alpha``. The ``alpha`` variable of the ``__init__`` routine is a "local" variable that is created when the ``__init__`` method is called and is discarded when the method execution has completed; it exists only while the method is running. In other words, the "scope" of the ``alpha`` variable (the part of the program in which the variable is accessible) is limited to the ``__init__`` method.

The ``self.alpha`` variable is a member of the object (named ``self`` within its own methods) and exists as long as the object itself exists. On a practical level, this means that multiple methods of the ``self`` object may use and make assignments to ``self.alpha``, which becomes a useful tool for exchanging information among the short-lived function calls. In this case, ``self.alpha`` provides permanent storage for a user-specified simulation parameter.

Lists, arrays, and the index operator
=====================================
Objects can contain data in the form of other objects. A list is a Python data structure that can hold an ordered sequence of arbitrary Python objects. A list may be modified--its elements can be added, removed, and redefined. Typically, PyGDH uses list objects as parameters when the user is allowed to specify an arbitrary number of related objects. 

A literal list may be written by enclosing zero or more expressions (which evaluate to Python objects) in square brackets, with expressions separated by commas.

This defines an empty literal list::

  domain_equations[0] = []

and this defines a literal list containing a single element, a string::

  output_types = ['GNUPLOT']

Elements of a list may be accessed by writing the name of the variable associated with the list, followed by an index. An index can be as simple as an integer (or an expression evaluating to an integer) that describes the numerical position of an element in a list. The first element of a list has index ``0``, the second has index ``1``, and so on. This is called "zero-indexing". From the most recent example::

	 output_types[0]

has the string value ``'GNUPLOT'``. 

These index expressions can be used to either retrieve the values of list elements, or to set them. One could change the value of the first (and only) element of ``output_types`` by writing::

    output_types[0] = 'HDF'

The index expressions can also be used for some other types of Python objects that contain other objects arranged in a specific order. In the example program::

  y[0] = 0.0

references the first element of ``y``, even though ``y`` in this particular case is a "`NumPy <http://numpy.scipy.org>`_ array," and not a list. NumPy arrays can be multidimensional and are typically faster than lists at the cost of requiring their contents to be of a single type (such as floating point numbers).

Negative indices may also be used with lists and arrays. They indicate element positions, counting backward from the last element. The last element can be referenced with the index ``-1``, the second to last with the index ``-2``, and so on, as in::

  y[-1] = 1.0

This can be a convenient because it offers a way to refer to the end of such an object without explicitly indicating how many elements the object contains.

Dictionaries
============
Sometimes, it is more natural to store objects associated with more general "keys" rather than numerical indices. A dictionary is an object that associates pairs of "keys" and "values". The values can be arbitrary Python objects, and many types of Python objects can be used as keys. While dictionaries are more flexible than lists, the elements of a list have a well-defined order, but the entries in a dictionary do not have a guaranteed order. Dictionaries are also slower than lists.

A literal dictionary can be created by enclosing key/value pairs of the form ``key:value`` within curly brackets, with pairs separated by commas. For example::

  self.equation_scalar_counts = {self.ode: 1}

defines a dictionary with a single key/value pair, associating ``self.ode`` (an object representing the ``ode`` method belonging to the ``self`` object) with the integer ``1``.

As with lists, a value in a dictionary may be referenced with an index expression. This is done by writing the dictionary name, followed by the corresponding key enclosed in square brackets. As each key in a dictionary must be unique, the dictionary returns a single value. For example::

   self.equation_scalar_counts[self.ode]

has the value ``1``. Similarly, one can associate a key in a dictionary with a new value by using the same form on the left side of an assignment statement, as in::

    self.equation_scalar_counts[self.ode] = 2

If the key does not already exist in the dictionary, the Python interpreter automatically creates a new entry. If the key already exists, it is associated with the new value.

For loops and Boolean values
============================
It is frequently useful to repeatedly run sequences of instructions, with each sequence differing only slightly from the last. This can often be achieved with a looping statement.

A Python for-loop executes its body (indented relative to the first line) once for each element of a sequence, with a specified variable assigned to successive element in successive loop iterations. For example, in::

  for vol_number in range(1, self.grid[0].unit_count-1):
      vol = self.grid[0].unit_with_number[vol_number]
      self.unknown[0][0,vol.number] = True

the variable ``vol_number`` takes on successive element values from the value of ``range(1, self.grid[0].self.unit_count-1)``. The ``range`` function, used in this way, returns a list of consecutive integers beginning with the first parameter ``1`` and ending at the last element, ``self.grid[0].self.unit_count-1`` in this case, minus one. Here, the first statement inside the for-loop assigns the element of ``set.unit_with_number`` (which happens to be an array) with the index ``vol_number``, to the variable ``vol``. In this way, the for-loop body successively acts on many elements of ``self.unit_with_number``.

The quantities ``True`` and ``False`` are "Boolean" values and are frequently used to represent quantities that have one of two possible values, in particular the results from logical operations such as::

    a == 1

where ``==`` is the equality operator, which is distinct from the assignment operator, ``=``, described at the beginning of this chapter.

List comprehensions
===================
There is frequently a need to use a for-loop to generate a new list based on an existing sequence, so a special abbreviated notation, called a "list comprehension", is provided for this purpose. This is used in the following example::

  unit_classes=[ Volume for i in range(volume_count) ]

The entire expression is enclosed in square brackets. Each successive element of the new list is obtained by evaluating the first expression (here, ``Volume``, which happens to be an Python object representing the ``Volume`` class definition, not shown here) with the loop variable (here ``i``) assigned to successive values of the given sequence (here, obtained by evaluating ``range(volume_count)``, which provides a list of successive integers from ``0` to ``volume_count-1``). In this particular case, the first expression is not dependent on the loop variable, so this list comprehension simply evaluates to a list with ``volume_count`` elements, each one associated with the object representing the ``Volume`` class definition.

Suggested exercises
===================
1. Write a ``for`` statement (being sure to indent appropriately after the first line) which separately prints the integers from 7 through 16. Hint: a ``print`` statement such as ``print i`` prints the value of a variable ``i`` on the screen. Also, a blank line is used to signal the definition of a compound statement given in the command-line interface.

2. Write a ``for`` statement which separately prints the even integers from 4 through 18.

3. Write a list comprehension that returns the same result as ``range(2,10)``

4. Write a function ``print_elements`` which separately prints each element contained in a list provided as a parameter. As with a ``for`` statement, be sure to indent appropriately after the first line. Call this function on the literal list ``['zero', 1, 2.0]``.

