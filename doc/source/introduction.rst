**********************************************************
Introduction: Is Grid Discretization Helper right for you?
**********************************************************

Grid Discretization Helper (abbreviated as PyGDH, and probably most easily pronounced as "pigged") simplifies the process of obtaining numerical solutions (presently by the finite volume method) to systems of (potentially nonlinear and time-dependent) discretized differential equations (and other types of equations) on one-dimensional and some two-dimensional domains. By managing many of the numerical details, PyGDH frees the user to concentrate on formulating the discretized mathematical problem.

There are many libraries and software packages with similar goals. PyGDH is not particularly efficient, nor is it suited to solving equations at high resolution or on domains with complicated shapes. However, it aims to be simple to use while providing substantial flexibility, and has already been used to solve fairly sophisticated problems.

PyGDH was developed for relatively small problems, for which the amount of computer time the can be saved by using more capable software is negligible compared to the amount of human time that can be saved by the simple approach that it offers. Some users may find it to be a useful tool for quickly getting intuition about a difficult problem before turning to more sophisticated packages.

In contrast with packages which work directly with the original mathematical problem statement, PyGDH requires users to provide the discretized form of the problem. Discretized forms suitable for PyGDH are often easy to obtain (see the sections on discretization), and working directly with the discretized equations gives users control over many numerical details. The discretized problems are represented as programs written in the Python programming language.

Prior programming experience is helpful, but not necessary, as the tutorial is meant to be a self-contained introduction to working with PyGDH. PyGDH is designed so that problem-solving programs can be nearly self-documenting and can be quickly understood and updated by a succession of users as the needs of a project evolve. The underlying Python programming language is popular and easy to learn, and remains fully-accessible to the user, making it possible to use PyGDH in very flexible ways.

As its name implies, PyGDH is limited to solving problems on domains that can be mapped to a rectangular grid (that is, with boundaries and divisions along which one spatial coordinate does not vary), and presently in one or two spatial dimensions. This allows spatial domains to be described in a simple way, but comes at the cost of flexibility, as PyGDH cannot be applied to domains of arbitrary shape.

While PyGDH is not yet used widely in the scientific community, it relies heavily on mature Python libraries and comes with a test suite that performs validation against analytical solutions.
