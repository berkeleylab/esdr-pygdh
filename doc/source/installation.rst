************
Installation
************
Windows, Mac OS X, GNU/Linux, and many other operating systems provide command-line interfaces. Installation and use of PyGDH, as described in this tutorial, will require limited use of your computer's command-line interface (CLI). For Microsoft Windows, this is the Command Prompt. For GNU/Linux, Mac OS X, and similar systems this might be a Terminal or xterm.

.. _command-line:

Introduction to command-line interfaces
=======================================
CLIs allow users to interact with computer systems by typing commands. The system prompts the user for a command, the user responds by typing a command and pressing the "Enter" key, and the computer attempts to accomplish the given task. This may involve doing calculations and displaying results on the screen or writing results into a file. If the computer is unsuccessful, which can occur for a variety of reasons, it typically displays an error message. Whether successful or unsuccessful, once the attempt is complete, the computer returns to the start of the cycle and prompts the user for another command.

In the CLIs provided with many operating systems, simply giving the name of a program as a command causes the system to "run" the named program by starting the program and giving it control of the interface. 

As an example, running the Windows "Command Prompt" program causes a window containing a CLI to appear. The system shows a command prompt that typically looks like::

   C:\Users\UserName>

The Python program is typically named ``python``, although this will vary among installations. If Python is already installed on your system, entering this in the Windows CLI starts Python in interactive mode::

   C:\Users\UserName>python
   Python 2.7.6 (default, Nov 10 2013, 19:24:18) [MSC v.1500 32 bit (Intel)] on win32
   Type "help", "copyright", "credits" or "license" for more information.	
   >>> 

Python prompts the user for commands as well, within the same interface. The ``>>>`` is Python's command prompt, which is completely distinct from those typed at the Windows command prompt. Commands entered here are instructions to Python, rather than to the Windows Command Prompt. For example, the following commands ask Python to load "libraries" that enhance its capabilities::

    >>> import scipy
    >>> import h5py

If these commands do not result in error messages, the required and suggested libraries are already installed on your computer, and you can skip the next section. 

To exit from Python and return to the Windows command prompt, one may type ``exit()`` at the Python prompt::

   >>> exit()
   C:\Users\UserName>   

The ``exit()`` command tells Python that its task is complete. When the operating system sees that a task has completed, it once again offers its command prompt to the user.

Python and required libraries
=============================
PyGDH is compatible with both Python 2 and Python 3.

For users of various versions of Microsoft Windows and Mac OS X, there are Python distributions such as `Python(x,y)`_, `Anaconda <https://www.continuum.io/why-anaconda>`_, and `Enthought Canopy <https://www.enthought.com/products/canopy/>`_ that contain Python along with a number of common libraries. Some of these distributions are quite large. 

PyGDH has been tested on Python 2.7.3 as packaged in `Python(x,y)`_ 2.7.3.1 on a computer running Microsoft Windows 7. The `Python(x,y)`_ website contains installation details.

.. _Python(x,y): http://python-xy.github.io/

However, users of GNU/Linux may prefer to install only the necessary packages through a package manager. For example, on the system on which PyGDH was initially developed (running a 64-bit PC installation of `Debian GNU/Linux <http://www.debian.org/>`_ 6.0), the required and suggested Python packages can be installed by running from the command line (with sufficient permissions)::

  apt-get install python-scipy python-h5py

or for Python 3, the following might be needed::
  
  apt-get install python3-scipy python3-h5py  

On a `CentOS <http://www.centos.org>`_ 6 system, these packages can be installed by running from the command line (with sufficient permissions)::

  yum install scipy h5py

For users who wish to download and install the necessary packages individually, software may be downloaded from the Python_ and SciPy_ websites. NumPy_ is included with SciPy_.

The HDF5_ library and the h5py_ Python interface to this library are optional, but are recommended for efficient access to stored results. Please note that installation of HDF5_ on Mac OS X is quite involved.

.. somewhat involved. Please see this_ guide. Neither the imaging libraries, SZIP, or HDF4 are needed. 

It is strongly recommended that users also install:

#. A "programmer's" text editor, such as `Emacs <http://www.gnu.org/software/emacs>`_ or `Notepad++ <http://notepad-plus-plus.org>`_ that understands Python code and provides syntax highlighting.
#. A plotting program, such as `GNUPLOT <http://www.gnuplot.info/>`_, which is used in this tutorial, and optionally, `matplotlib <http://matplotlib.org>`_ for generating plots from within Python.
#. `Cython <http://cython.org>`_, a Python-to-C compiler, which will make the PyGDH library faster and help users to make their code faster. Cython installation instructions are available `here <http://docs.cython.org/src/quickstart/install.html>`_, and as the page describes, a C compiler, such as `gcc <http://gcc.gnu.org>`_ or `Clang <http://clang.llvm.org>`_, must also be present (the Cython compiler produces C source code, from which the C compiler then produces a program in the native language of the computer).

.. _Python: http://www.python.org/
.. _NumPy: http://numpy.scipy.org/
.. _HDF5: http://www.hdfgroup.org/
.. _h5py: http://www.h5py.org/
.. _SciPy: http://www.scipy.org/

.. _this: http://cdx.jpl.nasa.gov/documents/technical-design/accessing-hdf-data-from-python-on-mac-os-x

PyGDH installation
==================
The PyGDH distribution comes in two compressed formats: ``pygdh-0.4.3.zip`` for Windows, and ``pygdh-VERSION.tar.gz`` for most other systems. 

Unpack the downloaded file into a directory (also known as a "folder"). This will create a subdirectory named ``pygdh-0.4.3``. Make note of the location of this directory. On a Windows computer, this might be ``C:\Users\UserName\Downloads\pygdh-VERSION``

Open your computer's CLI. Interactions with the CLI are always understood to be taking place "within" a present directory location, and it is most convenient to interact with files located in the present directory. Commands can be given to the CLI to change the present directory location. On Windows, Mac OS X, and GNU/Linux systems, the ``cd`` (Change Directory) command can be used to move to another directory. Under Windows, the appropriate command will be similar to::

     cd C:\Users\UserName\Downloads\pygdh-0.4.3

while on Mac OS X or GNU/Linux, the appropriate command will be similar to::

      cd /home/UserName/Downloads/pygdh-0.4.3

Once in the directory with the installation files, you may use a command such as ``dir`` (on Windows) or ``ls`` on Linux or Mac OS X to see a list of files in the directory::

 08/14/2014  11:33 AM    <DIR>          .
 08/14/2014  11:33 AM    <DIR>          ..
 08/14/2014  11:33 AM    <DIR>          pygdh
 08/13/2014  12:14 PM               248 PKG-INFO
 06/25/2014  04:12 PM                16 setup.cfg
 07/22/2014  09:51 AM               397 setup.py
                3 File(s)            661 bytes

The ``setup.py`` is used to tell Python about how PyGDH should be installed. If you wish to perform a system-wide installation, and you possess sufficient administrative privileges to do so, type the following at the command line to install::

     python setup.py install

Alternatively, you can install PyGDH within your own user directories, for which no special administrative access is needed, by running::

     python setup.py install --user

If you have installed Cython and a C compiler, and you wish to make use of the faster Cython-compiled PyGDH libraries, then you may also type::

     python cython_setup.py install

or::

     python cython_setup.py install --user

At present, this is not known to work on Mac OS X installations with Xcode.
     
To check that PyGDH has been properly installed, start Python as before (by typing ``python`` at the command line interface), and at the Python prompt, type::

   import pygdh

If no errors were encountered, PyGDH is ready to use. The directory into which the installation files were unpacked is no longer needed and may be removed.

.. Otherwise, if you installed PyGDH within your own user directories, it may be necessary to set an environmental variable to notify the Python interpreter of the location of the PyGDH files. This location will be listed in the output produced by the setup process.

.. The location is stored in the "environment variable" ``PYTHONPATH``. Under Microsoft Windows 7, type "Environment Variables" in the Start Menu search dialog box, and select "Edit environment variables for your account". Create a new variable named ``PYTHONPATH``, with the full directory command shell, this can be achieved by typing at the Command Prompt::
  
..  set PYTHONPATH=<full directory name ending in pythonX.Y>;%PYTHONPATH%

.. where ``X.Y`` are the first two parts of the version number reported by running from the command line::

..  python -V

.. Alternatively, Control Panel>System>Advanced Settings>Environmental Variables...

.. In a Bourne Shell-like environment (typical for GNU/Linux systems), one may type the following at the command line::

..  PYTHONPATH=~/.local/lib/pythonX.Y:$PYTHONPATH

.. In a C Shell-like environment (typically including Mac OS X), one instead writes::

..   setenv PYTHONPATH ~/.local/lib/pythonX.Y:$PYTHONPATH

.. Environment variables are lost when the shell program ends. One can either define ``PYTHONPATH`` before every session using PyGDH, or one can have it automatically defined through a number of system-dependent mechanisms. One option is to save commands for shell programs in files that are later read and executed. 

.. Under Microsoft Windows, this is done with a batch file (with extension ``.bat``), and on UNIX and similar systems, this is done with shell scripts. Some of these files are automatically executed when shell program start.

.. As an example, under Debian GNU/Linux, for which ``bash`` is the standard shell program, one may add the line::

..  export PYTHONPATH=~/.local/lib/pythonX.Y:$PYTHONPATH

.. to the ``.bashrc`` file in one's home directory.

.. at a command line (and press enter). If the prompt ``>>>`` appears, then Python is already installed, and is currently running. The first printed line shows the Python version in use.  If this worked, type the following::

..      import scipy

.. If no error message appears, the required Python library is already installed. Finally, type::

..      import h5py

.. If no error message appears, the suggested Python library is already installed. You may exit Python by typing::

..      quit()

.. If an appropriate Python interpreter and the required and suggested libraries are installed, you can safely skip the next subsection.

