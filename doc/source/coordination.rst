*************************************************
Coordinating the solution of multiple subproblems
*************************************************

When solving problems that might involve multiple equations, spatial domains, or timescales, one may choose to solve subproblems in a staggered fashion in order to reduce computational cost or improve code reusability. However, doing so will typically come at the cost of reduced overall accuracy, as solutions to equations that simultaneously hold in the mathematical sense are no longer solved simultaneously in the numerical sense.

``Coordinator`` objects can be used to coordinate the staggered solution of different subproblems. We will return to the earlier example of the problem solved across coupled domains, and divide it into two subproblems that correspond to the two domains.

A common base class
===================

The numerical problem to be solved takes the same form within all volumes in the interiors of the two domains, as they are not directly influenced by values at the domain boundaries. It description is identical to that used in solving the earlier problem in which solutions in the two regions are obtained simultaneously. We take advantage of this similarity and the object-oriented capabilities of the Python language by defining a base class that describes the common components of the problems:

.. include:: ../../pygdh/examples/coordination/coordinated_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START BASE
   :end-before: #### SPHINX SNIPPET END BASE

We note that the ``Grid`` objects are created separately and passed as parameters to ``__init__`` rather than being created within ``__init__`` as in the earlier implementation. This is done because the ``Grid`` objects are also passed to the ``Coordinator`` object in order to generate corresponding output files.
		
Each subproblem is then represented by a derived class that describes components that are specific to each subproblem. The discretized equations that correspond to the left and right ends of the combined domain are the same as those used in the earlier problem in which the two regions are solved simultaneously.

The interface
=============

For direction on how to describe the interface between the two regions, we return to the development of the discretized equation that represents the boundary in the earlier coupled domain problem. It was determined that

.. math::
   \frac{y\left(t_0 + \Delta t, b\right) - y\left(t_0, b\right)}{\Delta t} (c-a) - \alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b - \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c \approx 0.

But for a region bounded by :math:`a` and :math:`b`,

.. math::
   \frac{y\left(t_0 + \Delta t, b\right) - y\left(t_0, b\right)}{\Delta t} (b-a) - \alpha_l \left(\frac{\partial y}{\partial x}(t_0+\Delta t,b) - \frac{\partial y}{\partial x}(t_0+\Delta t,a)\right) \approx 0.

We would like to solve the last equation numerically. One can rewrite the first equation as

.. math::
   \frac{y\left(t_0 + \Delta t, b\right) - y\left(t_0, b\right)}{\Delta t} (b-a) - \alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b \frac{b-a}{c-a} - \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c \frac{b-a}{c-a}\approx 0.

Subtracting this from the second equation gives
   
.. math::
   - \alpha_l \left(\frac{\partial y}{\partial x}(t_0+\Delta t,b) - \frac{\partial y}{\partial x}(t_0+\Delta t,a)\right) + \alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b \frac{b-a}{c-a} + \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c \frac{b-a}{c-a}\approx 0.

.. math::
   -\alpha_l \left[\frac{\partial y}{\partial x}\right]_a^b \frac{c-b}{c-a} + \alpha_r \left[\frac{\partial y}{\partial x}\right]_b^c \frac{b-a}{c-a}\approx 0.

.. math::
   -\frac{\alpha_l}{b-a} \left[\frac{\partial y}{\partial x}\right]_a^b \approx -\frac{\alpha_r}{c-b} \left[\frac{\partial y}{\partial x}\right]_b^c.
   
This suggests that to describe the region bounded by :math:`a` and :math:`b`, as a separate subproblem, one could solve

.. math::
   \frac{y\left(t_0 + \Delta t, b\right) - y\left(t_0, b\right)}{\Delta t} (b-a) + \left\{ -(b-a) \frac{\alpha_r}{c-b} \left[\frac{\partial y}{\partial x}\right]_b^c \right\} \approx 0,

or
   
.. math::
   \frac{y\left(t_0 + \Delta t, b\right) - y\left(t_0, b\right)}{\Delta t} + \left\{ -\frac{\alpha_r}{c-b} \left[\frac{\partial y}{\partial x}\right]_b^c \right\} \approx 0,
   
with the value of the term in curly brackets simply provided as a "boundary condition." Ideally, this would be taken from the domain on the right where a submodel solution is simultaneously obtained. However, for a staggered solution, we must settle for solutions that are not simultaneously obtained, but more likely obtained in the previous iteration.

In order to promote encapsulation and avoiding the need for the two subproblems to know the internal structure of each other, we will need a mechanism for passing this additional information to each subproblem. This is provided in part by the ``add_boundary_source`` methods that are used to pass methods describing the boundary terms in each subproblem to the other subproblem:

.. include:: ../../pygdh/examples/coordination/coordinated_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START BOUNDARY SOURCE
   :end-before: #### SPHINX SNIPPET END BOUNDARY SOURCE

Unlike many of the methods encountered earlier, the ``add_boundary_source`` method is completely user-defined; its definition does not represent the override of methods defined by PyGDH. The ability to create arbitrary methods that can play important roles in the numerical implementations is another example of the benefit of implementing PyGDH as a Python package that leaves the Python interpreter completely exposed to users.

Following the above derivation, the derived classes that describe the full subproblems define equation methods that describe behavior adjacent to the interface and which incorporate boundary information from the other subproblem. Each subproblem also contains a method that makes this boundary information available to the other subproblem.

.. include:: ../../pygdh/examples/coordination/coordinated_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START SUBPROBLEMS
   :end-before: #### SPHINX SNIPPET END SUBPROBLEMS

By passing only these methods between the subproblems, neither subproblem requires detailed information about the internal operation of the other subproblem, supporting the object-oriented strategy of encapsulation.

Note that the declaration of locations of unknown values and equation assignments indicate that in both subproblems, the values at the interface are to be solved. Since the solver steps are staggered, these values are not obtained simultaneously and are likely to be slightly inconsistent.

Differencing formulas
=====================

Unlike in the earlier implementation, each subproblem must operate without detailed knowledge of the other, and in particular, the derivatives at the interface must be determined using only information available within a single subdomain. However, the low-order derivative operators in volumes adjacent to boundaries might not vary from one end of a volume to the other due to presence of the interface. This can produce inaccurate behavior. In order to address this problem, higher-order derivative operators are generated at the volume edges away from the interface:

.. include:: ../../pygdh/examples/coordination/coordinated_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START VOLUMES
   :end-before: #### SPHINX SNIPPET END VOLUMES

The derivative operators used throughout most of this Guide are sufficient for many applications, but PyGDH provides access to these details so that users have additional flexibility when needed, as in the present situation.

These specialized boundary ``Volume`` objects are incorporated into the ``Grid`` objects corresponding to the present subdomains:

.. include:: ../../pygdh/examples/coordination/coordinated_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START GRID
   :end-before: #### SPHINX SNIPPET END GRID

Finally, the ``Grid`` objects are created independently and passed to the separately created ``Problem`` objects, boundary information methods are exchanged, and a ``Coordinator`` object is created and informed of the ``Grid`` objects for which it must product corresponding output, as well as the ``Problem`` objects that are two be looped over in the specified order. The ``Coordinator`` object is then directed to run through the full problem by making alternating steps with the ``Problem`` objects.

.. include:: ../../pygdh/examples/coordination/coordinated_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

The output, generated from

.. include:: ../../pygdh/examples/coordination/coordinated.gpl
   :literal:

shows results that are close to those produced by the earlier implementation in which the subdomain calculations are performed simultaneously. The slight mismatch near the interface is a reflection of the error incurred by obtaining alternating subproblem solutions that are slightly inconsistent with each other.

.. image:: ../../pygdh/examples/coordination/coordinated.png
