*************************************************
A more complicated two-dimensional problem domain
*************************************************

PyGDH can solve equations on spatial domains with more complicated shapes.  This chapter will make use of much of the material in the previous chapter, with small modifications.

Defining the domain shape
=========================

PyGDH is limited to solving equations on domains bounded by grid lines along which only one spatial coordinate varies.  While the ``'coordinate_values'`` parameter to ``initialize_grid`` defines the maximum size of the domain, parts of the grid can be excluded from the problem domain.  These excluded parts must also have boundaries along which only one spatial coordinate varies.

As in the previous chapter, each coordinate n-tuple (obtained by taking values from the ``'coordinate_values'`` entries in the order given in the ``'coordinate_order'`` entries) is associated with a region of the grid.  In the interior of the grid, the boundaries between regions are located at coordinate values midway between the provided coordinate values.  The first and last values of any coordinate sequence are interpreted as boundaries of the regions at the edge of the grid.  Each region can also be referenced by its "grid indices", the n-tuple of indices into the ``'coordinate_values'`` sequences that give the coordinates associated with the region.

In the previous chapter, all of these regions are associated with ``Volume`` object.  However, regions may be excluded from the problem domain, so that there is no associated ``Volume`` object.  No equations are solved in these regions, and no degrees of freedom are associated with these regions.  ``Volume`` objects adjacent to these excluded regions are then associated with the domain boundaries, for which boundary conditions must be supplied.

Regions may be excluded from the problem domain by simply entering ``None`` in place of a ``Volume``-derived class in the ``unit_classes`` parameter to ``initialize_grid``. In this example, this is done with nested ``for`` loops:

.. include:: ../../pygdh/examples/multiply_connected/multiply_connected_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START GRID INIT
   :end-before: #### SPHINX SNIPPET END GRID INIT

This excludes the regions associated with nine intersections formed by three grid lines in both directions, creating a "hole" in the interior of the grid.  Actually, a slightly larger area is excluded from the problem domain, because the regions associated with the adjacent, newly-defined boundary ``Volume`` objects must be redefined.  The ``coordinate`` member of a ``Volume`` object associated with a boundary is interpreted as a location on that boundary, while is interpreted as the center of a ``Volume`` object in the interior.

There are restrictions on excluding regions.  Every ``Volume`` must have a neighboring ``Volume`` in each direction.  This is a requirement in order to use even the linear interpolation routines, and under the interpretation of the ``coordinate`` of a ``Volume`` indicating a location on a boundary for boundary ``Volume`` objects, a ``Volume`` with no neighbors in one direction occupies no space.  Also, the problem domain must be contiguous.

Please note that as a consequence of this approach to defining domain shapes, the boundaries formed by excluding regions have positions that are determined by the ``'coordinate_values'`` entries used to initialize ``Grid`` objects.

Defining domain boundaries
==========================

The boundaries defined in the previous chapter will be retained, and the newly-formed boundaries on the interior of the grid must be defined as well.

.. include:: ../../pygdh/examples/multiply_connected/multiply_connected_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DEFINE UNIT SETS
   :end-before: #### SPHINX SNIPPET END DEFINE UNIT SETS
      
Declaring unknowns and assigning equations
==========================================

The solution values will be prescribed on the "new" boundaries as well as on the "old" ones:

.. include:: ../../pygdh/examples/multiply_connected/multiply_connected_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ASSIGN UNKNOWNS AND EQUATIONS
   :end-before: #### SPHINX SNIPPET END ASSIGN UNKNOWNS AND EQUATIONS
      
Discretized equation
====================

Since PyGDH allows discretized equations to be written without details of the discretization formulas, the discretized equation method will be identical to that used in the previous chapter:

.. include:: ../../pygdh/examples/multiply_connected/multiply_connected_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PDE
   :end-before: #### SPHINX SNIPPET END PDE


Boundary values
===============

The boundary values are defined by the same mechanism as before, but the user is again encouraged to make use of the set definitions:

.. include:: ../../pygdh/examples/multiply_connected/multiply_connected_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START BC
   :end-before: #### SPHINX SNIPPET END BC

Solution
========

The solution is obtained as before.

.. include:: ../../pygdh/examples/multiply_connected/multiply_connected_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

The results are plotted below:

.. image:: ../../pygdh/examples/multiply_connected/multiply_connected.png

As before, a finite-difference approximate was used to validate the finite-volume solution, and the results are plotted below:

.. image:: ../../pygdh/examples/multiply_connected/multiply_connected_validation.png	   

The errors are much smaller than the terms appearing in the equations.

Output was produced with the following GNUPLOT script:

.. include:: ../../pygdh/examples/multiply_connected/multiply_connected.gpl
   :literal:
