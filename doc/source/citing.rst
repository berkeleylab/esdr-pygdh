************
Citing PyGDH
************

Development and packaging of PyGDH required significant time and effort, and it is hoped that PyGDH will save significant time and effort for other researchers. An accompanying article appears in the `Journal of Open Source Software <https://joss.theoj.org>`_. The article is available at `https://doi.org/10.21105/joss.02744`_. Please cite this article to reference PyGDH.
