********************************************
Solving a PDE with a flux boundary condition
********************************************

A time-dependent PDE may be solved by combining the techniques used previously to describe ODEs in time and space.

The example program is included as ``pygdh/examples/PDE/PDE.py`` within the PyGDH archive. It begins with 

.. include:: ../../pygdh/examples/PDE/PDE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START IMPORT
   :end-before: #### SPHINX SNIPPET END IMPORT

where the ``math`` module contains common mathematical functions.

This example will solve the one-dimensional heat equation,

.. math::
   \frac{\partial y}{\partial t} = \alpha \frac{\partial^2 y}{\partial x^2}

with the boundary conditions

.. math::
   y(0) = 0

   \alpha \frac{\partial y}{\partial x}(1) = 1.

Discretization
==============
This section will discuss the discretization of a PDE with a solution that varies in both space and time, using the one-dimensional heat equation as an example. 

.. The discretized equation obtained here will suggest additional machinery needed to obtain a numerical solution.

.. When using PyGDH, discretization in time is the responsibility of the user.

Using the backward difference formula introduced in the previous chapter, the differential equation

.. Computer solutions of time-dependent problems typically involve finding approximate numerical solutions at discrete moments in time, called "timesteps". Quantities such as time derivatives are approximated by comparing solutions at different timesteps. For this reason, solutions calculated at previous timesteps are generally needed in order to compute approximate solutions to time-dependent problems. For simplicity, this example will use a simple first-order backward difference in time. Under this scheme, 

.. math::
   \frac{\partial y}{\partial t}(t,x) = g(t,x,y(t,x))

is approximated by

.. math::
   \frac{y(t_0 + \Delta t,x) - y(t_0,x)}{\Delta t} \approx g(t_0 + \Delta t,x,y(t_0 + \Delta t,x)).

.. This is called a first-order scheme because the error in approximating the time derivative will be proportional to the size of the timestep to the first power, when the timestep is sufficiently small. This is written as

.. math: :
..   \frac{y(t_0 + \Delta t,x) - y(t_0,x)}{\Delta t} = g(t_0 + \Delta t,x,y(t_0 + \Delta t,x)) + \mathcal{O}(\Delta t).

As in the first example problem, we now obtain the finite-volume form by moving all nonzero terms to one side and integrating both sides over a one-dimensional volume ranging from :math:`a` to :math:`b` gives

.. math::
   \int_a^b \frac{\partial y}{\partial t} \ dx - \alpha \left[\frac{\partial y}{\partial x}\right]_a^b = 0.

Using the average value of the integrand, this is

.. math::
   \left<\frac{\partial y}{\partial t}\right> (b - a) - \alpha \left[\frac{\partial y}{\partial x}\right]_a^b = 0.

As an approximation, the average value of the time derivative is now taken as the value of the time derivative at the center of the volume:

.. math::
   \left(\frac{\partial y}{\partial t}\right)_{\mathrm{center}} (b - a) - \alpha \left[\frac{\partial y}{\partial x}\right]_a^b \approx 0,

which is the same thing as the time derivative of the value at the center of the volume:

.. math::
   \frac{\partial y_{\mathrm{center}}}{\partial t} (b - a) - \alpha \left[\frac{\partial y}{\partial x}\right]_a^b \approx 0.

Discretization in time by the first-order backward difference scheme then gives

.. math::
   \frac{y\left(t_0 + \Delta t, \frac{b+a}{2}\right) - y\left(t_0, \frac{b+a}{2}\right)}{\Delta t} (b-a) - \alpha \left(\frac{\partial y}{\partial x}(t_0+\Delta t,b) - \frac{\partial y}{\partial x}(t_0+\Delta t,a)\right) \approx 0.

As in the previous chapter, the ``stored_solution_count`` parameter to the ``initialize_problem`` method will be used to notify PyGDH that storage space must be provided for both the present solution and a past solution:

.. include:: ../../pygdh/examples/PDE/PDE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROBLEM INIT
   :end-before: #### SPHINX SNIPPET END PROBLEM INIT

.. Providing storage for past solutions
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``Grid``-derived objects manage the storage of solution information. In the first example program, it was only necessary to provide space for a single, time-independent solution. In order to set aside space for additional solutions, ``pygdh.Grid.initialize_grid`` can be called with an additional keyword parameter, called ``past_solution_count``, which indicates the number of additional solutions to be stored. In this example, the ``__init__`` routine of the user-defined ``Grid``-derived class is now

.. include: : ../../pygdh/examples/PDE/PDE_annotated.py
..    :lines: 26-49

.. This is similar to the ``__init__`` routine of the user-defined ``Grid``-derived class of the first example, except for the new keyword parameter, and a parameter ``alpha`` which will be used in setting the initial conditions. When ``past_solution_count`` is not given, like in the first example program, its value defaults to ``0``. This parameter does not merely allocate memory for storage; it determines the number of previous approximate solutions that are automatically stored by PyGDH.

.. In time-independent problems, as in the first example, no previous solutions are required (or even exist), so the default setting of ``past_solution_count = 0`` was kept. In this time-dependent problem, a very simple "timestepping" scheme will be used, which requires only one past solution in order to compute the present solution.

Due to the boundary condition in this example, there are two methods representing governing equations; this section will focus on the first, named ``pde``, and discussion of ``pde_boundary`` will be left to a later section.

As in the previous examples, the Python method computes a single scalar value and can be written as a simple transcription of the discretized equation. It makes use of the same operators and object data members, along with the timestep size, which is available as the automatically-defined ``Problem``-derived object member ``self.timestep_size``:

.. include:: ../../pygdh/examples/PDE/PDE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PDE
   :end-before: #### SPHINX SNIPPET END PDE

The local variables ``y0`` and ``y_1`` represent the current and past solutions. As in the previous chapter, they are used to reference a hierarchy of data structures. The definition of the local variable ``y0`` is equivalent to
::

   y0 = ( ( ( ( self.grid ) [0] ) . field ) [0] ) [0]

Reading this from the innermost parentheses to the outermost parentheses, this definition is seen to be almost identical to that in the previous chapter, through step 4. That is, ``self.grid[0].field[0]`` is a two-dimensional array of solution values at the present (0th) timestep on the single (0th) ``Grid`` describing the spatial problem domain. As before, the first array index indicates the independent variable (corresponding to the list of independent variable names in the ``field_names`` keyword parameter passed to ``initialize_grid``), and the second index indicates the ``Unit`` associated with the position at which the selected variable has the stored value.

However, unlike in the previous chapter, only one index is given for this two-dimensional solution value array. When only one index is provided to a two-dimensional array, it specifies a value for the first index, and a one-dimensional section of this array (consisting of all elements with the provided index as their first index) is returned; in effect, a two-dimensional array may be accessed as a one-dimensional array of one-dimensional arrays.  The index ``0`` given here corresponds to the only independent variable, ``y``. 

The resulting one-dimensional array can be accessed by a single index that is equivalent to the second integer index into the original two-dimensional array, so that an index into this array identifies a unique ``Unit`` object, although that is not explicitly done here, because the interpolation methods take one-dimensional arrays of this type as input. These methods are already aware of the spatial arrangement of the solution values in these arrays.

The integer label identifying a ``Unit`` object is automatically determined by PyGDH and may be accessed as the ``number`` member of a ``Unit`` object. In the other direction, the automatically-generated ``unit_with_number`` member of a ``Grid`` object is an array that stores the unit object associated with a given numerical label. In the simple case of a one-dimensional domain, the unit objects are simply numbered according to their spatial position. One may take ``0`` to correspond to the "left" end of the domain, with the smallest coordinate value, and the index ``-1`` as corresponding to the "right" end of the domain, with the largest coordinate value.

.. This statement is overly-complicated for this simple example problem because the data structures used by PyGDH are designed to be applied to much more complicated problems. However, it was decided that maintaining a consistent interface was more important than expressing simplest problems in a compact way.

.. Multidimensional arrays
.. .......................
.. A multidimensional array is a "rectangular" block of elements. As with one-dimensional NumPy arrays, specific elements are referenced by using the index operator, but with one integer index for each array dimension. When the index operator is used with fewer integer indices than the number of array dimensions, a section of the original array is returned, itself a NumPy array.

.. In this example, PyGDH defines each element of the ``field`` ``deque`` to be a two-dimensional array in which the first (left) integer index corresponds to the integer label of one of the solution variables. The solution variables are implicitly assigned unique integer labels by their positions in the ``field_names`` parameter used to initialize the ``Grid`` object. In this example, there is only one solution variable, so it has the index ``0``. 

.. The second (right) integer index corresponds to the integer label of one of the ``Volume`` objects belonging to the ``Grid`` object. PyGDH automatically assigns unique integer labels to each ``Volume`` object in a given ``Grid`` object. The ``volume_from_number`` member of the ``Grid`` object is a list of ``Volume`` objects, and the integer index used to reference a particular element is the unique integer label of that particular ``Volume``.

.. The expression ``field[0][0]`` that appears in this example therefore refers to the present solution of the solution variable with the descriptive label ``'y'``. But ``field[0]`` is a two-dimensional NumPy array, and ``field[0][0]`` provides only a single integer index into this array. Therefore, ``field[0][0]`` does not refer to an element of the array, but rather to a one-dimensional section of the array that is associated only with ``'y'``. Since the second index into the original array identifies the ``Volume`` object of interest, the first (and only) index into the new one-dimensional section identifies the ``'y'`` value in the ``Volume`` object of interest. 

.. One could have instead used the index operator with two integer indices into the two-dimensional ``field[0]`` array, and written 

.. ::

.. 	self.grid[0].field[0][0,0] = 0.0

.. This should be slightly more efficient than than version in the example, because the one-dimensional section of the array does not have to be identified.

Boundary conditions
===================
One of the boundary conditions (at the left boundary) is identical to that used in the first example problem, so the relevant entries in ``declare_unknowns``, ``set_boundary_values``, and ``assign_equations`` need no further explanation.

The condition at the right boundary is a flux condition, which applies to the derivative of the independent variable, rather than its value. The user-defined ``set_boundary_values`` method is only used at the boundaries at which the value is prescribed; other boundary conditions must be incorporated into governing equation methods.

For this boundary condition,

.. math::
   \alpha \frac{\partial y}{\partial x}(1) = 1

a second method representing the same governing equation is introduced for use with the corresponding boundary volume only. The boundary condition can be substituted directly into the discretized equation obtained earlier:

.. math::
   \frac{y\left(t_0 + \Delta t, \frac{b+a}{2}\right) - y\left(t_0, \frac{b+a}{2}\right)}{\Delta t} (b-a) - \alpha \left(\frac{1}{\alpha} - \frac{\partial y}{\partial x}(t_0+\Delta t,a)\right) \approx 0

   \frac{y\left(t_0 + \Delta t, \frac{b+a}{2}\right) - y\left(t_0, \frac{b+a}{2}\right)}{\Delta t} (b-a) - 1 + \alpha \frac{\partial y}{\partial x}(t_0+\Delta t,a) \approx 0

The Python representation is then given by:

.. include:: ../../pygdh/examples/PDE/PDE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PDE BOUNDARY
   :end-before: #### SPHINX SNIPPET END PDE BOUNDARY

As the boundary condition at the "right" side of the domain gives the value of the derivative of the solution variable, rather than prescribing its value, the value of the solution variable at this boundary is unknown and must be calculated by PyGDH. For this reason, the corresponding ``unknown`` value is set to ``True`` in the ``declare_unknowns`` method.

Finally, this new method must be noted in ``equation_scalar_counts``, and the boundary equation must be associated with the volume at the right boundary. As with the solution arrays, the elements of the ``equations`` structure are sequences with elements corresponding to ``Volume`` objects, numbered as in the ``unit_with_number`` members of the ``Grid`` objects.

.. include:: ../../pygdh/examples/PDE/PDE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START SCALAR COUNTS
   :end-before: #### SPHINX SNIPPET END SCALAR COUNTS

Initial condition
-----------------
Since the example problem is time-dependent, the initial condition must be provided. This must be done in a user-defined method named ``set_initial_conditions`` within the definition of the ``Problem``-derived class. This method must have ``self`` as its only parameter.

The initial condition should be a continuous function of position and must be consistent with the boundary conditions; the numerical initial condition defined in ``set_initial_conditions`` must be consistent with the boundary conditions as expressed in ``set_boundary_conditions``.

The ``set_initial_conditions`` methods should set the values for the present solution, with index ``0`` in the ``field`` deque. It should set the values of the dependent variables at all locations at which they are unknown; while there is no need to set values that are also set by ``set_boundary_conditions`` it causes no harm.

In the present example, 

.. include:: ../../pygdh/examples/PDE/PDE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START IC
   :end-before: #### SPHINX SNIPPET END IC

a local variable ``y`` is defined for clarity. The for loop in this method iterates over the ``Volume`` objects in ``unit_with_number``, setting the initial dependent variable value for each. The ``number`` members of the ``Volume`` objects are used to find the appropriate positions in the solution value arrays.

A known solution should be provided as the initial condition. An initial condition based on a single spatial eigenfunction gives the analytical solution the particularly simple form:

.. math:: 
   y(t,x) = \frac{x}{\alpha^2} + \frac{1}{2}\exp\left[-\left(\frac{\pi \alpha}{2}\right)^2 t\right] \sin \left(\frac{\pi}{2} x\right)

This is evaluated for :math:`t=0`, and the result is translated into the Python representation used above. This example makes use of the ``Domain`` object member ``alpha``, as well as entities from the ``math`` module, and the ``coordinate`` member of the ``Volume`` objects, which indicate their coordinate position in the spatial domain.

.. The location of each volume is accessed as the ``coordinate`` member of the corresponding ``Volume`` object. The ``sin`` function and the ``pi`` constant as defined in the ``math`` module (imported at the start of the second example) are also used.

.. Typically, the characteristic value of a variable on a given volume is the value of that variable at the ``coordinate`` position.

.. A continuous mathematical function is evaluated at discrete spatial points in order to obtain the numerical initial condition. The ``number`` member of a ``Volume`` object is automatically defined by PyGDH to be the integer label of that ``Volume``, and is the corresponding index into the ``volume_from_number`` list. In one-dimension, the ``coordinate`` member is a floating-point number that describes the unique coordinate value of the associated "volume", as taken from the ``coordinate_values`` parameter given for the ``Grid`` initialization.

.. The object member ``self.alpha`` is used in this routine. Since the user has substantial flexibility in defining the ``__init__`` method of this class, it is convenient to specify simulation parameters upon the creation of the ``Problem`` object, and to make these values available to the methods of the object. This is one reason that the ``set_initial_conditions`` method is defined in the ``Problem``-derived class, rather than the ``Grid`` class.

.. Declaring equations
.. ^^^^^^^^^^^^^^^^^^^
.. The methods representing governing equations will be discussed in the next chapter. This section will illustrate a technique for introducing with a "flux" boundary condition, as opposed to a boundary condition at which the solution value is prescribed.

.. Since the solution value is not given at the "upper" boundary, the value of the solution at this point must be computed. However, the equation that is used to describe the approximate behavior of the solution value in the volume adjacent to the boundary cannot be the same as that used to describe the behavior in volumes in the interior of the problem domain. In the interior of the domain, each volume is influence by the solution in the two neighboring volumes. The volume adjacent to the boundary has only one neighbor; the influence from the domain boundary is specified through the boundary condition.

.. As in the previous example, a single method (named ``pde``) can be used to describe the governing equation throughout the interior of the domain. However, a second method (named ``pde_boundary``) is now defined specifically to describe the behavior of the solution in the volume adjacent to the domain boundary.

.. The ``set_equations`` method is then defined as

.. include: : ../../pygdh/examples/PDE/PDE_annotated.py
..    :lines: 91-110

.. Each of the governing equation methods corresponds to a single scalar equation, and both are given entries in the ``equations_scalar_count`` dictionary that reflect this.

.. The assignment of equations to ``Volume`` objects is similar to that in the previous example, except that now the ``Volume`` corresponding to the "right" boundary is now associated with a method (the new ``pde_boundary`` method) representing a governing equation.

Solving a time-dependent problem
================================
As in the previous chapter, the time-dependent solver method is used:

.. include:: ../../pygdh/examples/PDE/PDE_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

The results are plotted against the analytical solution at a few timesteps:

.. image:: ../../pygdh/examples/PDE/PDE.png

This output was produced with the following GNUPLOT script:

.. include:: ../../pygdh/examples/PDE/PDE.gpl
   :literal:

Suggested exercises
===================
1. Change the number of timesteps and timestep size. Do the numerical results remain consistent with the analytical solution?

2. Suppose that ``self`` references a ``Problem`` object. What does ``self.grid[1]`` mean? How about ``self.grid[0].field[-1]``, ``self.grid[1].field[0][1]``, ``self.grid[1].field[0][1][20]``, and ``self.grid[1].field[0][1,20]``? Why is it wrong to write ``self.grid[2].field[0,1,20]``?

3. Change the flux boundary condition to

.. math::

   \alpha \frac{\partial y}{\partial x}(1) = 10.

Use the initial condition

.. math:: 
   y(x) = \frac{10 x}{\alpha} + \sin \left(\frac{\pi}{2} x\right).

Your results should match the analytical solution

.. math:: 
   y(t,x) = \frac{10 x}{\alpha} + \exp\left[-\left(\frac{\pi \alpha}{2}\right)^2 t\right] \sin \left(\frac{\pi}{2} x\right).
