****************************
Solving systems of equations
****************************

In the previous chapters, each ``Volume`` was associated with a single governing equation and a single solution variable. PyGDH may also be used to solve systems of coupled governing equations on a spatial domain. Typically, this requires only that additional discretized equation methods be supplied, and that the additional solution fields, equations, and boundary conditions be declared. The example code is included in the PyGDH archive as ``pygdh/examples/system/system.py``.

Diffusion with reaction
=======================

The one-dimensional form of Fick's second law will be used to describe the diffusion of two species in the dilute limit. These two species can react in a homogeneous reaction with a rate equation that is first-order in both concentrations:

.. math::
   \frac{\partial c_1}{\partial t} = D_1 \frac{\partial^2 c_1}{\partial x^2} - k c_1 c_2

   \frac{\partial c_2}{\partial t} = D_2 \frac{\partial^2 c_2}{\partial x^2} - k c_1 c_2

with zero fluxes at one end of the domain and equal nonzero fluxes at the other end:

.. math::
   -D_1 \frac{\partial c_1}{\partial x}(0) = -D_2 \frac{\partial c_2}{\partial x}(0) = 0

   D_1 \frac{\partial c_1}{\partial x}(1) = D_2 \frac{\partial c_2}{\partial x}(1) = N

and parabolic initial conditions that are consistent with the boundary conditions:

.. math::
   c_1(x) = 1 + \frac{N(D_1 - D_2)}{6 D_1 D_2} + \frac{N}{2 D_1} x^2
   
   c_2(x) = 1 + \frac{N}{2 D_2} x^2

The constant terms are chosen so that the domain initially contains equal amounts of species 1 and 2.

Problem domain
==============

Since there are now two unknown solution variables, both must be named in the ``'field_names'`` entry in the grid description. The variable named ``'c1'`` is the element with index 0 in this list, and the variable named ``'c2'`` has index 1. These same associations of solution variables and indices will be recognized by PyGDH throughout the rest of the program.

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START GRID INIT
   :end-before: #### SPHINX SNIPPET END GRID INIT
		
Discretized equations
=====================

Rearranging each equation into the form required by PyGDH (collecting all nonzero terms on one side), and integrating each equation over a volume with lower boundary :math:`x = a` and upper boundary :math:`x = b`, one obtains

.. math::
   \frac{\partial \left<c_1\right>}{\partial t} (b - a) - D_1 \left(\frac{\partial c_1}{\partial x}(b) - \frac{\partial c_1}{\partial x}(a)\right) + k \left<c_1 c_2\right> (b - a) = 0

   \frac{\partial \left<c_2\right>}{\partial t} (b - a) - D_2 \left(\frac{\partial c_2}{\partial x}(b) - \frac{\partial c_2}{\partial x}(a)\right) + k \left<c_1 c_2\right> (b - a) = 0

where the angle brackets indicate that the value used is an average over the volume. In the corresponding methods, a first-order implicit time-differencing scheme will be used.

One can represent these equations with two discretized equation methods, one for each governing equation, that compute and store scalar values. Alternatively, one may define a single method that computes and stores a vector with two components, each a scalar value. This is a more efficient approach when the two governing equations rely on some of the same intermediate calculations. The choice is unimportant in this example, but the latter case is used for illustration:

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PDES
   :end-before: #### SPHINX SNIPPET END PDES

Note that the indices of the ``self.grid[0].field[0]`` two-dimensional arrays identify the solution variable of interest, and that their numerical labels are consistent with their order in the ``'field_names'`` entry of ``initialize_grid``.

Similar methods are needed to incorporate the flux boundary conditions. For illustration, one method will represent both equations at one boundary, and two methods will be used to represent each equation at the other boundary.

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START BOUNDARY PDES
   :end-before: #### SPHINX SNIPPET END BOUNDARY PDES

Equations and unknowns
======================

As described in the ``declare_unknowns`` method, 

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DECLARE UNKNOWNS
   :end-before: #### SPHINX SNIPPET END DECLARE UNKNOWNS

the governing equations must be evaluated for all ``Volume`` objects, as the boundary conditions are all flux conditions, so there are no given boundary values and each ``Volume`` is associated with two unknown field values. The ``unknown_field`` index corresponds to the position of each solution variable name in the ``'field_names'`` parameter of ``initialize_grid``.

The choice of representations for the governing equations is reflected in ``assign_equations``:

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ASSIGN EQUATIONS
   :end-before: #### SPHINX SNIPPET END ASSIGN EQUATIONS

The number of scalars associated with each method is noted in ``equation_scalar_counts``, and the elements of ``equations[0]`` specify the methods that PyGDH must run in order to evaluate the required governing equations.

At the no-flux boundary and in the interior of the domain, the solution behavior is governed by discretized equation methods that each store two scalars, one for each of the unknown values associated with each ``Volume``.

At the "inlet" boundary, two discretized equation methods that each store one scalar are provided, again one for each of the unknown values associated with each ``Volume``.

Initial conditions
==================

The initial conditions for each solution variable are entered in a straightforward way. As before, the last index provided in each of the local variable definitions corresponds to the position of each solution variable name in the ``'field_names'`` entry of the grid description given at initialization.

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START IC
   :end-before: #### SPHINX SNIPPET END IC

Solution
========

The solution is then obtained with the parameter values ``N = 1.0``, ``D_1 = 1.0``, ``D_2 = 2.0``, and ``k = 1.0``:

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

The results are plotted for ten timesteps, with both curves decreasing over time toward a steady solution:

.. image:: ../../pygdh/examples/system/system.png

Validation
==========

The validation process is similar to that discussed in the nonlinear PDE example. As before, array variables (that can hold an error measurement for every ``Volume``) are defined once for the entire program, and marked for inclusion in the output files:

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DEFINE FIELD VARIABLES
   :end-before: #### SPHINX SNIPPET END DEFINE FIELD VARIABLES

These arrays are then filled with error estimates:

.. include:: ../../pygdh/examples/system/system_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PROCESS SOLUTION
   :end-before: #### SPHINX SNIPPET END PROCESS SOLUTION

The base-10 logarithm of the error magnitude (relative to a finite-difference form of the discretized equations) is plotted below for the solution at final timestep:

.. image:: ../../pygdh/examples/system/system_log_validation.png

Since the system is clearly approaching a steady state, the time derivative terms are expected to approach zero, so their magnitudes relative to the discrepancies are not of concern.

From the computed solution, it is apparent that the reaction terms are on the order of 1, and therefore much larger than the discrepancies. The magnitudes of the Laplacian terms are harder to estimate, so the numerical approximations have been included in the program output. These too are significantly larger than the discrepancies.

The discrepancies at the boundaries are much larger than those in the interior; the solution values here are strongly linked to the alternative methods used in the boundary ``Volume`` elements, and the influence of neighboring solution values comes only from one direction. Nevertheless, these discrepancies are not of concern, as they are still much smaller that the values of the terms in the discretized equation, and one can demonstrate that their magnitudes decrease as the number of ``Volumes`` is increased. In this case, the discretized equation error scales is known to scale linearly with the grid spacing, so doubling the number of ``Volumes`` should halve the discrepancies, and this is indeed observed for the relatively large errors at the boundaries.

Both plots were produced with the following GNUPLOT script:

.. include:: ../../pygdh/examples/system/system.gpl
   :literal:
      
Suggested exercises
===================
1. Confirm that the FVM/FDM discrepancy magnitudes at the boundaries decrease linearly with the number of domain elements.
