********************
Introductory remarks
********************

This tutorial attempts to be self-contained. It introduces many of the elements necessary to use PyGDH effectively, including object-oriented programming, the Python language, discretization techniques, and validation of solutions. While many problems can be solved by using the information provided in earlier chapters, many details are discussed only in the later chapters.

PyGDH offers a flexible but simple approach to solving equations by helping users to express discretized equations in the form of a Python program. Some familiarity with Python is required in order to use PyGDH. This tutorial attempts to provide enough description to give the reader a working understanding of Python programming. The next chapter provides a brief introduction to the Python language, and additional concepts will be introduced as necessary when example code is examined. Readers seeking a deeper understanding of the Python language are encouraged to read the `official Python documentation <https://docs.python.org/>`_ for their installed version of Python.
