*******************************
Two-dimensional spatial domains
*******************************

Programs using PyGDH to solve problems with two-dimensional spatial domains have a similar structure to those used to solve one-dimensional problems, but a few extensions are necessary.

Please note that in comparison to the one-dimensional case, even seemingly small problem domains in higher dimensions may require fairly large amounts of computing time.  This is one reason that support for three-dimensional spatial domains has not yet been implemented.

Initialization
==============

The number of domain dimensions is inferred from the parameters passed to ``initialize_grid``.  As in the one-dimensional case, the ``'coordinate_values'`` parameter is itself a dictionary with one sequence of values for each spatial coordinate.  These values give the locations of grid lines along the specified coordinate.  As this suggests, these coordinate value sequences define a grid of rectangles (potentially of varying sizes), formed by the intersection of grid lines, along which the value of only one coordinate changes.

The sequences of coordinate values must be specified in a strictly monotonically increasing order.  

The ``'coordinate_order'`` parameter is a list of the "keys" from the ``'coordinate_values'`` dictionary, indicating the order in which numerical coordinates are to be understood (this must be specified explicitly, because the ordering of entries in a dictionary is not guaranteed to be the order in which they are created).  For example, the parameters appearing in:

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START GRID INIT
   :end-before: #### SPHINX SNIPPET END GRID INIT
		
indicate that the first number in a pair of coordinates corresponds to a coordinate named ``'x'``, and that the second number corresponds to a coordinate named ``'y'``.  Note that the same names do not have to be used for corresponding Python variables, but inconsistency is likely to be confusing.

The ``'coordinate_order'`` list also serves to associate numerical labels with the different coordinate directions.  These numerical labels are used as indices in arrays for which a numerical index is used to indicate direction, such as when describing interpolants.

Grid layout overview
====================

PyGDH is only capable of solving problems on spatial domains that can be mapped to rectangular grids.  These grids may have irregularly-spaced grid lines, and regions may be excluded from the grid when defining the spatial domain if the remaining volumes have boundaries that coincide with grid lines.

While the ``Volume`` objects in two-dimensional domains are assigned integer labels, these labels are not necessarily related in a simple way to positions within the domain, as is the case in one dimension.  Many of the underlying data structures, such as the solution value arrays in ``fields``, use these integer labels, but it is never necessary for the user to know the actual label values.

In two dimensions, the sequences given in the ``'coordinate_values'`` entry in a grid description dictionary give the coordinates of the grid lines--on each grid line, the value of the respective coordinate is fixed.  Each intersection of grid lines is associated with the position of a ``Volume``.  In other words, taking the first coordinate from the ``'coordinate_values'`` entry associated with the first element in the ``'coordinate_order'`` list, and taking the second coordinate from the entry associated with the second element in the ``'coordinate_order'`` list, gives a coordinate pair that is associated with a ``Volume``.  This coordinate pair is stored as the ``coordinate`` member of the same ``Volume``.  

The integer indices into the ``'coordinate_values'`` sequences which correspond to the ``coordinate`` values of a ``Volume`` object are, when taken in order, the "grid indices" of the ``Volume``.  These are stored as the ``grid_indices`` member of the same ``Volume``, which is a useful label for referencing ``Volume`` objects.

Defining domain boundaries
==========================

In the one-dimensional case, there were only ``Volume`` elements associated with domain boundaries, one at each end.  In the two-dimensional case, a rectangular domain has four boundaries, and excluding portions of a grid creates additional boundaries.

Since assignments of values and equations are commonly performed on collections of ``Volume`` objects, PyGDH requires, for two-dimensional problems, that users define collections of these objects in order to simplify the rest of the problem description.  This is done by defining a method named ``define_unit_set_with_name`` in a class derived from ``pygdh.Grid``. This method takes no arguments besides ``self``, and must define a dictionary for each ``Grid`` in ``self.grid`` that contains more than one ``Volume``.  Each "key-value" entry in this dictionary should have a descriptive name (in the form of a string) as a "key" for the collection of ``Volume`` objects represented by the "value".

These collections of ``Volume`` objects must be defined as Python set objects, which correspond to mathematical sets.  PyGDH assists the user in constructing the required set objects by specifying locations of their ``Volume`` objects.

In the one-dimensional case, relative positions of ``Volume`` objects within the domain were directly related to the integer labels, so identifying the ``Volume`` objects by label was a simple matter.  In two dimensions, ``Volume`` objects are most easily referenced by their locations, expressed as grid indices.  The ``boundary_unit_set_grown_from_edge_index`` method of the ``Grid`` objects returns a set of ``Volume`` objects on the same boundary edge, given the grid indices of one of the ``Volume`` objects on that boundary.

.. Once all of the sets of ``Volume`` objects on boundaries have been created, one may choose create an additional set of all ``Volume`` objects in the ``Grid`` that are not present in the set union of all set objects currently in the ``volume_set`` dictionary.
..  When given grid indices corresponding to a ``Volume`` at the intersection of multiple boundaries, all ``Volume`` objects on all of these boundaries are included in the returned set.  While this may be a useful abbreviation, it is unclear and a potential source of confusion.

..   The ``volume_set_from_volumes`` method returns a set of ``Volume`` objects, given a ``list`` of their grid indices.  Finally, the ``complement_to_volume_set`` method returns a set of all ``Volumes`` in the domain that do not belong to the set of ``Volumes`` given as an argument.

The following method definition groups the boundaries of the rectangular domain into sets with descriptive, user-defined names:

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DEFINE UNIT SETS
   :end-before: #### SPHINX SNIPPET END DEFINE UNIT SETS

.. The ``volume_set`` members may be created in one-dimensional problems as well, but doing so is optional.

Declaring unknowns and assigning equations
==========================================

The process of notifying ``Volume`` objects about their associated unknown solution values is essentially the same as in the one-dimensional problems, but here, the user is encouraged to use the set definitions from the previous section:

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START DECLARE UNKNOWNS
   :end-before: #### SPHINX SNIPPET END DECLARE UNKNOWNS

Note that the ``for`` statements iterate directly over the ``Volume`` objects contained in the set objects. It is not necessary for the user to know how the set objects store the ``Volume`` objects, only that the ``for`` loops are guaranteed to visit all of the stored objects.
.. The ``volumes`` member of the ``Grid`` objects stored in ``self.grid`` is a ``list`` containing all ``Volume`` objects in the domain.

Similarly, the set definitions should be used when assigning equations to ``Volume`` objects.

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START ASSIGN EQUATIONS
   :end-before: #### SPHINX SNIPPET END ASSIGN EQUATIONS

Discretization in two dimensions
================================

In Cartesian coordinates, the Laplace equation in two dimensions is written as:

.. math::
   \frac{\partial^2 T}{\partial x^2} + \frac{\partial^2 T}{\partial y^2} = 0

Performing a double integral over the two-dimensional "volume" (an area) within the boundaries of a ``Volume`` gives, for this "well-behaved" equation:

.. math::
   \int_{x_0}^{x_1} \int_{y_0}^{y_1} \left(\frac{\partial^2 T}{\partial x^2} + \frac{\partial^2 T}{\partial y^2} \right) \ dy \ dx = 0

   \int_{y_0}^{y_1} \int_{x_0}^{x_1} \frac{\partial^2 T}{\partial x^2} \ dx \ dy + \int_{x_0}^{x_1} \int_{y_0}^{y_1} \frac{\partial^2 T}{\partial y^2} \ dy \ dx = 0

   \int_{y_0}^{y_1} \left[\frac{\partial T}{\partial x}\right]_{x_0}^{x_1} \ dy + \int_{x_0}^{x_1} \left[\frac{\partial T}{\partial y}\right]_{y_0}^{y_1} \ dx = 0

   \left[\frac{\partial T}{\partial x}\left(x,\frac{y_1 + y_0}{2}\right)\right]_{x_0}^{x_1} (y_1 - y_0) + \left[\frac{\partial T}{\partial y}\left(\frac{x_1 + x_0}{2},y\right)\right]_{y_0}^{y_1} (x_1 - x_0) \approx 0

In the notation of PyGDH, this can be written as:

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START PDE
   :end-before: #### SPHINX SNIPPET END PDE

The ``length`` members of the ``Volume`` elements are sequences with as many elements as there are spatial dimensions in the domain.  Each element gives the difference in the associated coordinate value from one side of the ``Volume`` to the other.  The ``'coordinate_order'`` in the grid definition determines which integer index value corresponds to which coordinate variable.

Boundary values
===============

For the present example, we will use the following boundary conditions:

.. math::
   T(0,y) = 0

   T(x,0) = 0

   T(1,y) = \sin(\pi y)

   T(x,1) = 0

The boundary values are defined by the same mechanism as before, but the user is again encouraged to make use of the set definitions:

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START BC
   :end-before: #### SPHINX SNIPPET END BC

Solution
========

Just as in the one-dimensional time-independent case, an instance of the user's class is created, and directed to obtain a solution: 

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions_annotated.py
   :code:
   :start-after: #### SPHINX SNIPPET START MAIN
   :end-before: #### SPHINX SNIPPET END MAIN

The results are plotted below against the analytical solution,

.. math:: 
   T(x,y) = \frac{\sinh(\pi x)}{\sinh \pi} \sin (\pi y)

.. image:: ../../pygdh/examples/two_dimensions/two_dimensions.png

This output was produced with the following GNUPLOT script:

.. include:: ../../pygdh/examples/two_dimensions/two_dimensions.gpl
   :literal:
      
Suggested exercises
===================
1. Solve Poisson's equation

.. math::
   \frac{\partial^2 \phi}{\partial x^2} + \frac{\partial^2 \phi}{\partial y^2} = -4 \pi \rho

for :math:`\rho = 1` and boundary conditions
   
.. math::
   \phi(0,y) = 0

   \phi(x,0) = - 2 \pi x^2 + 1

   \phi(1,y) = \sin(\pi y) - 2 \pi

   \phi(x,1) = - 2 \pi x^2 + 1

and compare the numerical result with the analytical solution.
