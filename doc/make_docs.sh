#!/bin/sh
# Run this program after running all examples with test.py
PYTHON_EXECUTABLE=`cat ../python_executable.txt`
VERSION=`cat ../version.txt`
cd ../pygdh/examples/compiling
${PYTHON_EXECUTABLE} system_output.py system_uncompiled_profile.dat > system_uncompiled_profile.log
${PYTHON_EXECUTABLE} system_output.py system_compiled_profile.dat > system_compiled_profile.log
cd ../../../doc
make html
make latexpdf
mv build/latex/PyGDH.pdf build/latex/PyGDH-${VERSION}.pdf
