import sys
import numpy
from distutils.core import setup
from Cython.Build import cythonize
setup(name='pygdh',
      version='VERSION',
      description='Grid Discretization Helper',
      author='Kenneth Higa',
      author_email='KHiga@lbl.gov',
      license='BSD-3-Clause-LBNL',
      platforms='All',
      requires=['numpy','scipy'],
      packages=['pygdh'],
      ext_modules = cythonize('pygdh/*.pyx', compiler_directives={'language_level':sys.version_info.major}),
      include_dirs=[numpy.get_include()],     
      package_data = {'pygdh': ['*pxd']})
