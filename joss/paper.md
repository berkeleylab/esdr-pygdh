---
title: 'PyGDH: Python Grid Discretization Helper'

tags:

- Python
- nonlinear
- differential equations
- finite volume method
- solver
- simulation

authors:

- name: Kenneth Higa
  affiliation: 1
  orcid: 0000-0003-4866-0748
- name: Vincent S. Battaglia
  affiliation: 1
  orcid: 0000-0002-5596-9148
- name: Venkat Srinivasan
  affiliation: 2
  orcid: 0000-0002-1248-5952

affiliations:

- name: Energy Technologies Area, Lawrence Berkeley National Laboratory
  index: 1
- name: Argonne Collaborative Center for Energy Storage Science, Argonne National Laboratory
  index: 2

date: 29 September 2020

bibliography: paper.bib

---

# Summary

Mathematical models expressed in the form of discretized equations play an important role in many scientific disciplines. In our experience, few domain scientists have sufficient background in numerical computing (or the time required to acquire such a background) to use many flexible and powerful but complex open source packages, such as FEniCS [@AlnaesBlechta2015a] and OpenFOAM [@openfoam]. Many user-friendly open source packages, such as FiPy [@fipy], and many commercial packages, such as COMSOL Multiphysics [@comsol] and Simcenter STAR-CCM+ [@starccmplus], provide limited flexibility in the equations that users can express. Additionally, the use of commercial packages, which by nature do not perform calculations transparently, can hinder reproducibility, which is vital to the scientific process. PyGDH ("pigged") is a Python 2 / Python 3 [@python] package that is meant to be accessible to scientists who might not be specialists in scientific computing, while approaching the level of flexibility associated with writing dedicated programs tailored to solving specific problems. The PyGDH User's Guide provides detailed instructions for creating numerical models, including a brief introduction to necessary command line and Python [@python] skills, and discussions of discretization and validation. Note that PyGDH emphasizes flexibility and simplicity over performance, and was not designed for high-performance applications or models describing complex spatial domains.

# Statement of need

PyGDH was created to address the following requirements:

- uses a widely understood, easily learned language, Python [@python], to allow users with limited scientific computing background to conveniently express mathematical problems to be solved using the finite volume method; this is a common and frequently simple but effective discretization approach, for which [@patankar] provides an excellent introduction
- to support flexibility, extensibility, and reuse, retains full access to an underlying general-purpose, object-oriented language with broad adoption, Python [@python], and its wealth of libraries
- allows users to directly describe arbitrary discretized equations (that might be nonlinear and/or time-dependent) to be solved using nonlinear solvers, with convenient representations that are similar to standard mathematical representations but which are in fact executable numerical code used directly by the solver
- requires minimal numerical "bookkeeping" work by users
- allows convenient reuse and postprocessing of calculation results
- allows specification of problems on zero-, one-, and simple two-dimensional spatial domains
- has few strict dependencies, specifically Python [@python], NumPy [@oliphant2006guide],[@van2011numpy], and SciPy [@2020SciPy-NMeth], to simplify installation for novice users
- open source to ensure that all calculations are transparent and to encourage dissemination, verification, and reuse of numerical models; PyGDH was released under the OSI-approved [BSD-3-Clause-LBNL](https://opensource.org/BSD-3-Clause-LBNL) license.

We anticipate that PyGDH will be useful to students learning about scientific computing and researchers who want to explore the behavior of potentially complicated equations on simple spatial domains. As an example of the capabilities of this package, please see the work for which PyGDH was originally created [@higa-srinivasan].

# Acknowledgments

This work was supported by the Assistant Secretary for Energy Efficiency and Renewable Energy, Advanced Manufacturing Office and Office of Vehicle Technologies of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

We thank the developers of the Python programming language [@python] and its standard libraries, the developers of the NumPy [@oliphant2006guide],[@van2011numpy], SciPy [@2020SciPy-NMeth], h5py [@h5py], and HDF5 [@hdf5] libraries (and the libraries on which these are based), the developers of the Sphinx Python Documentation Generator [@sphinx], Gnuplot [@gnuplot], matplotlib [@Hunter:2007], and the Cython compiler [@behnel2010cython], as well as developers throughout the Open Source community who have made this work possible.

We hope that PyGDH will encourage scientific openness and strengthen public confidence in the work of the scientific community by providing an open, simple, and transparent problem-solving environment in which researchers can describe mathematical models in compact forms that are easily disseminated. We thank Greg Wilson of [Software Carpentry](http://software-carpentry.org) for promoting openness in software development within the scientific community and providing inspiration to release this work as open-source software.

We also thank Ashlea Patterson, Sunil Mair, Anne Suzuki, Fabiola Lopez, Andrew Veenstra, Tiffany Ho and Fiona Stewart for improving the user experience by providing valuable feedback on early versions of the PyGDH documentation and software.

# References