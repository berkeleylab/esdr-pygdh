from __future__ import print_function
from __future__ import absolute_import

import numpy
from . import grid
import fvm
import h5py
import csv

class Results:
    def __init__(self,filename_root):
        
        self.hdf_file = h5py.File(filename_root+'.h5','r')

        self.grid_count = self.hdf_file.attrs['grid_count']

        # Store reference to groups for easy access
        self.grid_group = [ self.hdf_file['grid_'+str(grid_index)] for grid_index in range(self.grid_count) ]

        self.grid = [ None for grid_index in range(self.grid_count) ]

        # Read in the basic simulation information

        for grid_index in range(self.grid_count):
            
            ## Read in basic grid information

            indexed_grid = self.grid[grid_index]

            # This is a list of the sequences of coordinate values associated with each dimension
            # Rather than storing HDF5 datasets, I will store numpy arrays
            coordinates_list = [ numpy.array(self.grid_group[grid_index]['coordinates_'+str(dimension_index)]) for dimension_index in range(self.grid_group[grid_index].attrs['dimension_count']) ]

            # Create Grid object
            # we don't know how many past solutions are needed.  This is something that would have to be stored if we need it.
            grid_description = { 'coordinate_values': {'r':coordinates_list[0], 'z':coordinates_list[1]},
                                 'coordinate_order': ['r', 'z'],
                                 'field_names': [ str(i) for i in range(self.grid_group[grid_index].attrs['field_count']) ], 
                                 'stored_solution_count': 1}
                                 
            self.grid[grid_index] = grid.Grid(grid_description)
            self.grid[grid_index].field = [ None for i in range(3)]
            self.grid[grid_index].field[0] = numpy.array(self.grid_group[grid_index]['solution_0'])

            # This is really only needed when the original simulation is not used to process the stored data

            self.grid[grid_index].volume_index_to_grid_indices = self.grid_group[grid_index]['volume_index_to_grid_indices']
            self.grid[grid_index].reconstruct_from_data()

            ## These may not be so necessary for postprocessing, but it would be nice to pass reconstructed Grid objects to reinitialize simulations.
            self.grid[grid_index].assign_physical_coordinates()
            self.grid[grid_index].determine_neighbors()
            self.grid[grid_index].count_neighbors()
            self.grid[grid_index].determine_interior_neighbors()
            self.grid[grid_index].calculate_neighbor_relationships()
            self.grid[grid_index].calculate_relative_boundary_positions()
            self.grid[grid_index].calculate_lengths()
            
    def close(self):
        self.hdf_file.close()

    def read_fields_into_grid(self,grid_number,timestep,sequence_index):
        self.grid[grid_index].field[sequence_index] = numpy.array(self.grid_group[grid_number]['solution_'+str(timestep)])

    def stored_dataset(self,grid_index,dataset_name_root,timestep):
        return numpy.array(self.grid_group[grid_index][dataset_name_root+'_'+str(timestep)])

    # Assume that conversion of solution has already been done
    def write_fields_in_gnuplot_format(self,grid_number,fields,filename):
        """Writes field value data from simulation output files to a text file suitable for plotting by programs such as Gnuplot.

        Output file consists of coordinates of each volume, followed by the value of each stored field, in order.  Each column is separated by spaces, blank lines groups of volumes for which only one coordinate value varies, and two blank lines separate results for different timesteps.
        """
        grid = self.grid[grid_number]
        if self.grid_group[grid_number].attrs['dimension_count'] == 1:
            with open(filename,'w') as file_obj:
                for coord_index in range(len(self.coordinates_list[0])):
                    coord = self.coordinates_list[0][coord_index]
                    coord_string = str(coord)
                    vol_index = coord_index
                    if vol_index == -1:
                        file_obj.write(coord_string + ' inf\n')
                    else:
                        file_obj.write(coord_string)
                        # Write data from current solution
                        for field in fields:
                            file_obj.write(' ' + str(field[vol_index]))
                        file_obj.write('\n')                        

        elif grid.dimension_count == 2:

            with open(filename,'w') as file_obj:
                for coord_index0 in range(len(grid.coordinates_list[0])):
                    coord0 = grid.coordinates_list[0][coord_index0]
                    coord0_str = str(coord0)
                    for coord_index1 in range(len(grid.coordinates_list[1])):
                        coord1 = grid.coordinates_list[1][coord_index1]
                        coord_string = coord0_str + ' ' + str(coord1)
                        vol_index = grid.layout[coord_index0][coord_index1]
                        if vol_index == -1:
                            file_obj.write(coord_string + ' inf\n')
                        else:
                            file_obj.write(coord_string)
                            # Write data from current solution
                            for field in fields:
                                file_obj.write(' ' + str(field[vol_index]))
                            file_obj.write('\n')                        
                    file_obj.write('\n')

    def write_grid_in_gnuplot_format(self):
        grid = self.grid[0]
        for i in range(grid.index_count[0]):
            for j in range(grid.index_count[1]):
                vol = grid.volumes[grid.layout[i][j]]
                print(vol.coordinates[0] + solution_fields[0][vol.index], vol.coordinates[1] + solution_fields[1][vol.index])
            print()
        for j in range(grid.index_count[1]):    
            for i in range(grid.index_count[0]):
                vol = grid.volumes[grid.layout[i][j]]
                print(vol.coordinates[0] + solution_fields[0][vol.index], vol.coordinates[1] + solution_fields[1][vol.index])
            print()

## for i in range(problem.index_count[0]):
##     for j in range(problem.index_count[1]):
##         vol = problem.grid[0].volumes[problem.grid[0].layout[i][j]]
##         print vol.coordinates[0], vol.coordinates[1]
##     print
## for j in range(problem.index_count[1]):    
##     for i in range(problem.index_count[0]):
##         vol = problem.grid[0].volumes[problem.grid[0].layout[i][j]]
##         print vol.coordinates[0], vol.coordinates[1]
##     print

## print


