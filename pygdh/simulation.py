from __future__ import print_function
from pygdh import grid
import collections
from pygdh import pygdh_io
import time
from pygdh import model

class Simulation:

    def __init__(self,name,output_types,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,write_initial_fields=True):
        """output_types: a list of strings specifying the desired output file
          formats.  Currently available options are 'HDF5', 'CSV', and 
          'GNUPLOT'"""

        self.name = name
        self.output_types = output_types
        self.handle_exception_in_solver=handle_exception_in_solver
        self.adaptive_timestepping=adaptive_timestepping
        self.minimum_timestep_size=minimum_timestep_size
        self.use_predictor=use_predictor
        self.quiet=quiet
        self.write_initial_fields=write_initial_fields
    
    def initialize_simulation(self,grid_sequence,submodel,substep_counts=None,stored_solution_count=1):
        """Initialize Simulation object.

        **Parameter**

        ``grid_sequence``: a ``list`` or ``tuple`` of ``grid.Grid`` objects
        associated with spatial domains.  The index of an object in this
        sequence becomes the integer label associated with the object.

        ``stored_solution_count``: an integer giving the number of
        solutions that are used in the time discretization formulas
        appearing in equations associated with the present ``Grid``        
        """

        # A Simulation advances all submodels by the same timestep size during a timestep. Done asynchronously, the submodels are advanced in order specified by the active submodel sequence
        # PyGDH must be able to restart a simulation at the start of any timestep at the top level (at which there is a single Simulation that is not managed by another Simulation)
        # This suggests that at minimum, at the end of each top-level timestep, all Grids on which later calculations will depend must be written out
        # All Grids that need to accessed by the managed Models must be supplied to the Simulation, since the Simulation passes these to the managed Models. It's possible that the Models will not modify all of these Grids.
        # Simulations managed by other Simulations ("subsimulations") might involve Grids that are only used internally, so as long as all Simulations are responsible for writing out Grids used by their Models, restarts should be possible.
        
        # Check that parameters are tuples or lists
        assert (type(grid_sequence) == type(tuple())
                or type(grid_sequence) == type(list())), 'ERROR: Simulation.initialize_models: expected list or tuple of "Grid" objects as "grid_sequence" parameter.'

        for element in grid_sequence:
            # Check that this is an object of the grid.Grid type
            assert isinstance(element,grid.Grid), 'ERROR: Simulation.initialize_models: expected "Grid" objects as elements of "grid_sequence" parameter'

        # Store argument for later use
        self.grid = grid_sequence
        
        self.grid_count = len(grid_sequence)

        # Provide slow but convenient means of referencing Grid objects
        self.grid_with_name = {}
        for element in grid_sequence:
            self.grid_with_name[element.name] = element

        self.grid_number_with_name = {}
        for element_number in range(len(grid_sequence)):
            self.grid_number_with_name[element.name] = element_number        

        self.input = None
            
        self.stored_solution_count = stored_solution_count
        self.time = collections.deque(range(self.stored_solution_count))
        
        assert (type(submodel) == type(tuple())
                or type(submodel) == type(list())), 'ERROR: Simulation.initialize_models: expected list or tuple of "Grid" objects as "submodel" parameter.'

        for nested in submodel:
            if isinstance(nested,model.Model):
                # Notify all managed submodels of all of the managed grids
                nested.set_grid(self.grid,stored_solution_count=stored_solution_count)
                #### temporary
                nested.time = self.time
            elif isinstance(nested,Simulation):
                pass
            else:
                raise AssertionError('ERROR: Simulation.initialize_models: expected "Model" or "Simulation" objects as elements of "submodel" parameter')
            
        self.submodel = submodel

        # This is needed because it is saved by set_step_sequence()
        self.active_submodel_number_sequence = range(len(self.submodel))
        self.set_step_sequence(range(len(self.submodel)))
        
        if substep_counts == None:
            self.substep_counts = [1 for i in submodel]
        else:
            self.substep_counts = substep_counts
            
        self.time = collections.deque(range(self.stored_solution_count))

    # def return_active_submodel_number_sequence(self):
    #     r"""Return a sequence of submodel numbers indicating which submodels are to be solved on the next timestep.
    #     """

    #     return range(len(self.submodel))
        
    def solve_time_independent_system(self):

        for submodel in self.submodel:
            if isinstance(submodel,model.Model):
                #submodel.check_consistency_of_unknowns()
                submodel.check_consistency_of_initial_and_boundary_conditions()
        
        for submodel in self.submodel:
            # At the boundaries for which the solution variable values are given
            # rather than computed, set the appropriate values in the 'field' 
            # members of the 'Grid' objects
            submodel.set_boundary_values()

            # Copy initial solution guess from the values provided in the 'field'
            # members of the 'Grid' objects (stored for the "present" timestep with
            # numerical label 0)
            guess = submodel.full_DOF_vector_from_fields(0)

            ### Solve the nonlinear system, starting from the initial guess

            print('INFO: Initialization complete, starting time-independent solver.')

            [solution, ier, mesg] = submodel.solve(submodel.system_residuals,guess)

            # Copy the solution to the 'field' members of the 'Grid' objects, for
            # the "present" timestep with numerical label 0
            submodel.distribute_full_DOF_vector_to_Grid_objects(solution)

            # The user may override this method in order to perform certain
            # processing tasks on the numerical solution
            submodel.process_solution()

        # Create object to manage all output
        output = pygdh_io.Output(self.grid,self.name,self.output_types)

        # Save the mappings needed to reconstruct the solution over the domain 
        # from the arrays of solution values
        output.write_mappings()

        # Save the numerical solution
        output.write_fields(0,0.0)

        output.close()

        print('INFO: Time-independent solver method finished.')

    def initialize_subsimulations(self,timestep_size):

        self.timestep_size = timestep_size
        #### This needs to be updated in order to restart simulations
        self.timestep_index = 0
        # Create object to handle all output
        self.output = pygdh_io.Output(self.grid,self.name,self.output_types)

        # Now that the domain is fully specified, write out mappings needed to 
        # reconstruct the solution over the domain from the arrays of solution
        # values
        self.output.write_mappings()

        #self.active_submodel_number_sequence = self.return_active_submodel_number_sequence()

        for submodel_number in self.active_submodel_number_sequence:
            
            submodel = self.submodel[submodel_number]
            if isinstance(submodel,model.Model):
                # The 0th element of the 'field' member is assumed to contain the 
                # solution at the previous timestep on all 'Grid' objects.
                initial_timestep_index = 0
                # This is also performed at the start of Model.time_dependent_system_step
                submodel.inverse_timestep_size = 1./self.timestep_size
                submodel.initialize_time_dependent_solver(initial_timestep_index)
                submodel.set_initial_conditions()
                submodel.set_boundary_values()

        self.pre_step_hook()
                
        for submodel_number in self.active_submodel_number_sequence:
            
            submodel = self.submodel[submodel_number]
            if isinstance(submodel,model.Model):

                submodel.process_solution()                

            else:
                # Might want to move this to a later point
                submodel.initialize_subsimulations(self.timestep_size)
                
        # Store initial data and just take the time from the first 'Grid'
        # object
        if self.write_initial_fields:
            start_time = self.time[0]
            self.output.write_fields(0,start_time)
            
        # # Before rotating the initial conditions into the past solution
        # # position, use them to obtain the initial solution guess
        # if self.input == None or use_predictor == False:
        #     for submodel in self.submodel:
        #         submodel.initial_guess = submodel.full_DOF_vector_from_fields(0)

        #     #self.time[0] = 0.0
        # else:
        #     for submodel in self.submodel:            
        #         submodel.initial_guess = submodel.initial_guess(self.timestep_size,submodel.full_DOF_vector_from_DOF_vectors(-1))

    def full_step(self,timestep_size):
        '''
If at the top level, make a single timestep. If below the top level, make as many timesteps as needed to fill a single timestep at the parent level. All active submodels and subsimulations must be advanced by the timestep interval.
        '''
        # This is called recursively for Simulations, which all track time separately because nested Simulations might require multiple smaller timesteps to span the timestep made by the parent Simulation; once the parent timestep is complete, all nested Simulations and Models must be advanced by the same total time as the parent Simulation

        # Rotate the last solution (or initial condition) into the -1 position.
        # Rotate the past solutions
        for grid in self.grid:
            grid.field.rotate(-1)
            # Save the oldest field and add a new "present" (index 0) field
            grid.oldest_field = grid.field.popleft()
            grid.field.appendleft(grid.candidate_field)

        # Update the simulation time
        # self.time[-1] is initialized to zero by default            
        self.time.rotate(-1)
        self.oldest_time = self.time[0]
        self.timestep_index += 1

        self.time[0] = self.time[-1] + timestep_size
        
        # We can now proceed with the calculations for this timestep. If a model is encountered before all calculations are completed, we can return to this point without rotating data structures.
        while True:

            # Just take the time from the first ``grid.Grid`` object
            
            for grid in self.grid:            
                # When multiple Models make use of the the same Grids, broadcasting the full DOF vector for a given Model does not guarantee that the Grid data for the current timestep is current. Therefore, the Grid data for the current timestep is initially populated with a copy from the last timestep
                grid.field[0][0:grid.field_count,0:grid.unit_count] = grid.field[-1][0:grid.field_count,0:grid.unit_count]
                #print(grid.field)
            # In the timestep preparation routine, the solutions just computed
            # have been rotated into the -1 memory location, but have been
            # copied to the 0 memory location

            early_exit = False

            ### I'd like to change this mechanism
            for submodel_number in self.active_submodel_number_sequence:
                submodel = self.submodel[submodel_number]

                interrupted = (submodel_number not in self.last_active_submodel_number_sequence)

                ### Needed so that we can pass if there's nothing to solve
                success = True
                if isinstance(submodel,model.Model) and submodel.equation_scalar_counts != None:
                    #### later, do this once and don't allow Models to define their own self.time
                    submodel.time = self.time
                    # if isinstance(submodel,model.Model):
                    #     submodel.time.rotate(-1)
                    #     submodel.time[0] = self.time[0]

                    success = submodel.time_dependent_system_step(self.timestep_size,self.output,handle_exception_in_solver=self.handle_exception_in_solver,adaptive_timestepping=self.adaptive_timestepping,minimum_timestep_size=self.minimum_timestep_size,use_predictor=self.use_predictor,quiet=self.quiet,interrupted=interrupted) and not submodel.abort_solver_and_reject_solution()
                elif isinstance(submodel,Simulation):
                    status = submodel.full_step(timestep_size)
                    success = (status != -1)

                if not success:
                    # If any submodel is not solved, this timestep attempt is unsuccessful
                    break
            if success:
                for submodel_number in self.active_submodel_number_sequence:
                    if isinstance(submodel,model.Model):           
                        self.submodel[submodel_number].process_solution()
                # The solution is acceptable, so keep the new element and switch the candidate_field members with oldest_field members in preparation for a following timestep

                for grid_obj in self.grid:
                    grid_obj.candidate_field = grid_obj.oldest_field

                # This is the user's opportunity to perform validation, and store results besides the solution

                self.process_solutions()

                # Store solution data, just take the time from the first ``grid.Grid`` object
                for submodel_number in self.active_submodel_number_sequence:
                    if isinstance(submodel,model.Model) and not early_exit and self.submodel[submodel_number].abort_solver_and_accept_solution():
                        early_exit = True
                if early_exit:
                    return 1
                else:
                    return 0
            else:
                if self.adaptive_timestepping:
                    timestep_size *= 0.5
                    if timestep_size > minimum_timestep_size:
                        self.time[0] = self.time[-1] + timestep_size
                        continue
                return -1

    def cascading_write(self,timestep_index,timestamp):
        
        #self.output.write_fields(self.timestep_index,self.time[0])
        self.output.write_fields(timestep_index,timestamp)        

        for submodel_number in self.active_submodel_number_sequence:
            submodel = self.submodel[submodel_number]
            if isinstance(submodel,Simulation):
                submodel.cascading_write(timestep_index,timestamp)
        
    ### Need to add restart capability for coordinated time-dependent calculations
        
    def solve_time_dependent_system(self,maximum_time_interval=None,timestep_size=None,timestep_count=None):
        """Solve time-dependent system, called only at the top level.
        
        **Parameters**

       ``restart``: ``False`` if this is a new simulation, ``True`` if restarting from saved data

        **Returns** solution vector
        """

        assert ((maximum_time_interval != None and timestep_size != None) or (maximum_time_interval != None and timestep_count != None) or (timestep_count != None and timestep_size != None)), '! Model.solve_time_dependent_system: Not enough information provided to perform time-dependent simulation.'

        assert self.stored_solution_count > 0, 'ERROR: pygdh.Simulation.solve_time_dependent_system: self.stored_solution_count == 0'

        for submodel in self.submodel:
            if isinstance(submodel,model.Model):
                #submodel.check_consistency_of_unknowns()
                submodel.check_consistency_of_initial_and_boundary_conditions()
        
        # This top-level timestep index provides the most efficient way of retriving records, especially among different files. Subsimulations at the same top-level timestep should use the same index.
        self.timestep_index = 0
            
        # Make the timestep size available to the other methods
        if timestep_size == None:
            self.timestep_count = timestep_count
            self.timestep_size = maximum_time_interval / float(self.timestep_count)
        elif timestep_count == None:
            self.timestep_size = timestep_size
            self.timestep_count = int(round(maximum_time_interval / timestep_size))
        else:
            self.timestep_size = timestep_size
            self.timestep_count = timestep_count                
            maximum_time_interval = self.timestep_size * self.timestep_count

        self.initialize_subsimulations(timestep_size)
        
        start_time = self.time[0]

        print('INFO: Initialization complete, starting time-dependent solver.')

        while (self.time[0] - start_time < maximum_time_interval) and (not self.adaptive_timestepping and self.timestep_index < self.timestep_count):

            if not self.quiet:
                print("Timestep", self.timestep_index, "at time", self.time[0], '...')

            self.pre_step_hook()
            status = self.full_step(timestep_size)

            if status == -1:
                print('ERROR: Solution did not converge.')
            elif status == 1:
                # Successful timestep, with criterion for early exit triggered
                print('INFO: Early exit criterion fulfilled.')
                self.cascading_write(self.timestep_index,self.time[0])
            elif status == 0:
                # Successful timestep
                self.cascading_write(self.timestep_index,self.time[0])
                        
        self.output.close()

        print('INFO: Time-dependent solver method finished.')

    def set_step_sequence(self,step_sequence):

        self.last_active_submodel_number_sequence = self.active_submodel_number_sequence
        self.active_submodel_number_sequence = step_sequence
        
        
    def pre_step_hook(self):

        pass
        
    def timestep_preparation_returning_submodel_number_sequence(self):
        # Provides a simple, clean approach to changing which submodels are solved on-the-fly without having the complicate the internals of Model objects, leaving them to represent a single mathematical model
        # Since this determination is often made along with setting some kind of system input, those determinations should be made here too
        # By default, solve all submodels in original order
        # Override or include this for more sophisticated requirements
        return [i for i in range(len(self.submodel))]
    
    def __del__(self):
        if self.input != None:
            self.input.close()            

    def load_solutions(self,input_filename_root):
        self.input = pygdh_io.Input(self.grid,input_filename_root)
    
    def restore_from_timestep(self,timestep):

        self.input.restore_from_timestep(timestep,self.time)

        for submodel in self.submodel:
            submodel.compute_grid_DOF_vectors()

    def process_solution(self):

        for submodel in self.submodel:
            submodel.process_solution()
            
    def preprocess_solutions(self):

        pass

    def process_solutions(self):

        pass
    
