cdef class Unit:
    cdef public grid, index, coordinate, unknown_field, interior_neighbor_local_index, equation_count_array, equation_to_residual_mapping
    cdef public int number, neighbor_count, scalar_count, equation_count, DOF_count
    cpdef define_variables(self)
    cpdef define_interpolants(self)
