cimport numpy
from . cimport unit
from . cimport volume
cdef class Grid:
#    cdef public char* name
    cdef public name, unit_classes, field_names, coordinate_order, coordinates_list, index_bounds, unit_with_number, output_fields, finalized, field, time_dependent, oldest_field, unit_set_with_name, field_number_with_name
    cdef public numpy.ndarray unit_number_with_index, unit_with_index, candidate_field
    cdef public int field_count, dimension_count, unit_count, stored_solution_count
