# cython: profile=False
from __future__ import print_function
from __future__ import absolute_import

import numpy
cimport numpy
from . import unit
from . cimport unit
from . import volume
from . cimport volume
import collections

cdef class Grid:
    """Defines objects that describe the spatial problem domains.
    """
    
    def initialize_grid(self, name, field_names = [],
                        coordinate_values = [], coordinate_order = [], 
                        unit_classes = [],stored_solution_count=1):
        """Initialize Grid object.

        **Parameters**

        ``coordinate_values``: a dictionary with one key-value pair
        for each spatial coordinate.  Each key is a string containing
        a descriptive name for the coordinate, and the corresponding
        value is a strictly monotonically increasing sequence of
        numbers specifying the coordinate values of grid lines

        ``coordinate_order``: a ``list`` of all keys from the
        ``coordinate_values`` entry, in the order with which all
        coordinate ntuples are to be understood, and in which integer
        labels are assigned, starting from zero

        ``field_names``: a ``list`` of descriptive names for the
        solution field variables, in the order to which integer labels
        are assigned, starting from zero
        """

        ## Save some parameter values

        self.name = name

        self.unit_classes = unit_classes

        self.field_names = field_names

        self.field_count = len(field_names)

        self.field_number_with_name = {}
        for field_number in range(self.field_count):
            self.field_number_with_name[field_names[field_number]] = field_number
        
        self.oldest_field = None
        
        if len(coordinate_values) == 0:

            assert len(coordinate_order) == 0, 'ERROR: Grid.initialize_grid: "coordinate_order" parameter should be an empty sequence because "coordinate_values" is an empty dictionary"'

            print('INFO: Grid.initialize_grid: no coordinate information provided, so creating zero-dimensional Grid object', name)
            self.dimension_count = 0

        else:
            ## Determine the number of dimensions of this 'Grid' object and
            ## store the coordinates of grid line

            if len(coordinate_order) == 0:

                assert len(coordinate_values) == 1, 'ERROR: Grid.initialize_grid: "coordinate_order" parameter is an empty sequence, so at most one entry may be provided in the "coordinate_values" parameter'

                self.coordinate_order = coordinate_values.keys()

            else:
                assert len(coordinate_values) == len(coordinate_order), 'ERROR: Grid.initialize_grid: "coordinate_order" and "coordinate_values" parameters must have the same number of entries'
                self.coordinate_order = coordinate_order            

            self.dimension_count = len(self.coordinate_order)
            
            ## Check that every key-value pair in coordinate_values has a 
            ## string as a key
            
            for key in coordinate_values.iterkeys():
                assert (isinstance(key,str) or isinstance(key,unicode)), 'ERROR: Grid.initialize_grid: keys in "coordinate_values" argument should be strings'

            ## Check that every coordinate variable listed in coordinate_order
            ## is defined in coordinate_values
            
            for coordinate_name in self.coordinate_order:
                assert coordinate_name in coordinate_values, 'ERROR: Grid.initialize_grid: mismatch between entries of "coordinate_order" and "coordinate_values" parameters'

            ## Check that every key-value pair in coordinate_values has an 
            ## appropriate value type
            
            try:
                for value in coordinate_values.itervalues():
                    assert (isinstance(value,tuple) or isinstance(value,list) 
                            or isinstance(value,numpy.ndarray))
            except:
                print('ERROR: Coordinate values', value, 'in coordinate_values argument should be a tuple, list, or NumPy array.')
                raise AssertionError

            print('INFO: Creating', str(self.dimension_count) + '-dimensional Grid object named', '"' + self.name + '".')

            # Sequence of coordinate values, in user-specified coordinate order
            self.coordinates_list = [ 
                numpy.array(coordinate_values[coordinate_name]) 
                for coordinate_name in self.coordinate_order ]

            # This expects a sequence of sequences
            self.index_bounds = [ len(coordinates) 
                                  for coordinates in self.coordinates_list ]

            if self.dimension_count == 2:

                self.unit_number_with_index = numpy.empty(self.index_bounds,
                                                          dtype=numpy.int16)
                self.unit_with_index = numpy.empty(self.index_bounds,
                                                   dtype=numpy.object_)
        self.unit_count = 0
        self.unit_with_number = None
        self.output_fields = { }
        self.unit_set_with_name = None

        # Create 'Unit' objects and assign numbers and indices
        self.create_unit_objects()

        if self.dimension_count != 0:

            ## When spatial relationships matter, determine relationships among 
            ## 'Unit' objects

            self.count_neighbors()
            self.determine_interior_neighbors()
            for unit in self.unit_with_number:
                unit.calculate_boundary_distances()
                unit.calculate_volume_dimensions()

            if self.dimension_count > 1:

                ## For a spatial domain with more than one dimension, the user 
                ## is required to override this in order to group units into 
                ## sets. This promotes program clarity.
                self.define_unit_set_with_name()

                try:
                    assert len(self.unit_set_with_name) != 0
                except:
                    print('ERROR: unit_set_with_name not defined for self "' + self.name +'"')
                    raise AssertionError

                try:
                    for key,value in self.unit_set_with_name.iteritems():
                        assert len(value) != 0
                except:
                    print('ERROR: unit_set_with_name "' + key + '" of Self "' + self.name + '" contains no "Unit" objects')
                    raise AssertionError

        for unit in self.unit_with_number:
            unit.define_variables()
            unit.define_interpolants()            

        # The user may override this method to define additional variables for
        # calculations
        self.define_field_variables()

        # The user may override this method to specify that the values of
        # additional variables should be included in output files
        self.set_output_fields()

        # In typical cases, this will have the same value for each Grid used in a coordinated simulation, which seems redundant. However, by providing the opportunity to decouple the Grid objects, one could use different timestepping schemes or compute different intermediate timepoints in different subproblems
        self.stored_solution_count = stored_solution_count

        # Human-friendly results will be stored in here
        # The zero initial guess comes from here.
        self.field = collections.deque( [ numpy.zeros((self.field_count,self.unit_count)) for i in range(self.stored_solution_count) ] )

    # These routines are called in order ######################################

    def create_unit_objects(self):

        if self.dimension_count == 0:

            ## There is only one 'Unit'

            assert issubclass(self.unit_classes[0],unit.Unit), 'ERROR: create_unit_objects: class specified in "unit_classes" parameter to "initialize_grid" must be derived from "unit.Unit"'

            self.unit_count = 1
            unit_number = 0

            self.unit_with_number = [ 
                self.unit_classes[unit_number](self,unit_number,
                                               ( unit_number, ), None) ]

        elif self.dimension_count == 1:

            ## In the 1D case, the 'Unit' objects cover a continuous region

            for class_name in self.unit_classes:
                assert issubclass(class_name,unit.Unit), 'ERROR: create_unit_objects: classes specified in "unit_classes" parameter to "initialize_grid" must be derived from "unit.Unit"'

            ## Also in 1D, the 'index' and 'number' are the same

            self.unit_count = self.index_bounds[0]
            self.unit_with_number = [ 
                self.unit_classes[unit_number](
                    self,unit_number,(unit_number,),
                    self.coordinates_list[0][unit_number]) 
                for unit_number in range(self.unit_count) ]

        elif self.dimension_count == 2:

            ## In the 2D case, some sections of the rectangular grid may be
            ## excluded from the domain, so it is necessary to loop over all
            ## possible 'Unit' locations

            ## Use two indexing operators to 'self.unit_classes' so that the
            ## user can define this in a variety of ways

            self.unit_count = 0
            self.unit_with_number = []
            for i in range(self.index_bounds[0]):
                for j in range(self.index_bounds[1]):
                    assert (self.unit_classes[i][j] == None 
                            or issubclass(self.unit_classes[i][j],unit.Unit)), 'ERROR: create_unit_objects: elements specified in "unit_classes" parameter to "initialize_grid" must be "None" or derived from "unit.Unit"'

                    if self.unit_classes[i][j] == None:

                        # Signal that there is no 'Unit' associated with this
                        # location
                        self.unit_with_index[i,j] = None
                        self.unit_number_with_index[i,j] = -1

                    else:

                        self.unit_number_with_index[i,j] = self.unit_count
                        coordinate = [ self.coordinates_list[0][i], 
                                       self.coordinates_list[1][j] ]
                        new_unit = self.unit_classes[i][j](
                            self,self.unit_count,(i,j),numpy.array(coordinate))
                        self.unit_with_number.append(new_unit)
                        self.unit_with_index[i,j] = new_unit
                        self.unit_count += 1

        # Human-friendly results will initially be stored here
        self.candidate_field = numpy.empty((self.field_count,self.unit_count))

    def count_neighbors(self):

        if self.dimension_count == 1:

            ## In 1D, the 'Unit' objects together represent a continous
            ## domain. The objects on the ends of the domain have only one
            ## neighbor, toward the interior of the domain, and all others have
            ## neighbors to both sides
            self.unit_with_number[0].neighbor_count = 1
            for unit_number in range(1,self.unit_count-1):
                self.unit_with_number[unit_number].neighbor_count = 2
            self.unit_with_number[self.unit_count - 1].neighbor_count = 1

        elif self.dimension_count == 2:
            for unit in self.unit_with_number:
                # Total the number of neighboring units
                unit.neighbor_count = 0
                for index0 in (unit.index[0]-1,unit.index[0]+1):
                    if (index0 >= 0 and index0 < self.index_bounds[0]
                        and self.unit_with_index[index0,unit.index[1]] != None):
                        unit.neighbor_count += 1

                for index1 in (unit.index[1]-1,unit.index[1]+1):
                    if (index1 >= 0
                        and index1 < self.index_bounds[1] and
                        self.unit_with_index[unit.index[0],index1] != None):
                        unit.neighbor_count += 1

                for index0 in (unit.index[0]-1,unit.index[0]+1):
                    for index1 in (unit.index[1]-1,unit.index[1]+1):
                        if (index0 >= 0 and index1 >= 0
                            and index0 < self.index_bounds[0]
                            and index1 < self.index_bounds[1] and
                            self.unit_with_index[index0,index1] != None):
                            unit.neighbor_count += 1

    def determine_interior_neighbors(self):
        if self.dimension_count == 1:
            self.unit_with_number[0].interior_neighbor = self.unit_with_number[1]
            self.unit_with_number[0].interior_direction_relative_index = 1

            self.unit_with_number[self.unit_count-1].interior_neighbor = self.unit_with_number[self.unit_count-2]
            self.unit_with_number[self.unit_count-1].interior_direction_relative_index = -1
            for unit_number in range(1,self.unit_count-1):
                unit = self.unit_with_number[unit_number]
                unit.interior_neighbor = unit
                unit.interior_direction_relative_index = 0

        elif self.dimension_count == 2:
            for unit in self.unit_with_number:

                ## Interior

                if unit.neighbor_count == 8:
                    unit.interior_direction_relative_index = (0,0)

                ## On an edge
                    
                elif (unit.neighbor_count == 5 or unit.neighbor_count == 6):
                    for i in (unit.index[0]-1,unit.index[0]+1):
                        if (i == -1 or i == self.index_bounds[0]
                            or self.unit_with_index[i,unit.index[1]] == None):
                            unit.interior_direction_relative_index = (
                                unit.index[0]-i,0)
                            break
                    for j in (unit.index[1]-1,unit.index[1]+1):
                        if (j == -1 or j == self.index_bounds[1]
                            or self.unit_with_index[unit.index[0],j] == None):
                            unit.interior_direction_relative_index = (
                                0,unit.index[1]-j)
                            break

                ## On a corner
                            
                elif unit.neighbor_count == 3:
                    for i in (unit.index[0]-1,unit.index[0]+1):
                        for j in (unit.index[1]-1,unit.index[1]+1):
                            if (i != -1 and i != self.index_bounds[0]
                                and j != -1 and j != self.index_bounds[1]
                                and self.unit_with_index[i,j] != None):
                                unit.interior_direction_relative_index = (
                                    i-unit.index[0],j-unit.index[1])
                                break
                            
                ## At an internal corner
                            
                elif unit.neighbor_count == 7:
                    for i in (unit.index[0]-1,unit.index[0]+1):
                        for j in (unit.index[1]-1,unit.index[1]+1):
                            if ((i == -1 or i == self.index_bounds[0])
                                and (j == -1 or j == self.index_bounds[1])
                                or self.unit_with_index[i,j] == None):
                                unit.interior_direction_relative_index = (
                                    unit.index[0]-i,unit.index[1]-j)
                                break

                [index0, index1] = unit.index

                unit.interior_neighbor = self.unit_with_index[
                    index0 + unit.interior_direction_relative_index[0],
                    index1 + unit.interior_direction_relative_index[1]]

    ### These may be overridden by the user ###################################

    def define_unit_set_with_name(self):
        """Placeholder for user-defined method that groups 'Unit' objects into
        sets

        This routine should define a dictionary named ``unit_set_with_name`` in each
        ``Grid`` object.  The keys should be strings that describe the
        significance of the values, which are ``set`` objects containing
        ``volume.Volume`` objects.

        At a minimum, all ``volume.Volume`` objects on the domain boundary must be
        assigned to one of these ``set`` objects
        """
        print('ERROR: Required method ``define_unit_set_with_name`` not defined.')
        raise AssertionError

    def define_field_variables(self):
        """Placeholder for optional user-defined method that defines additional
        variables, typically members of ``self`` that are used by multiple
        methods.
        """
        print('WARNING: Optional method ``define_field_variables`` not defined for', '"' + self.name + '".  Has validation been performed?')
        pass

    def set_output_fields(self):
        """Placeholder for optional user-defined method that defines which
        variables, in addition to the solution variables, are written to output
        files.
        """
        print('WARNING: Optional method ``set_output_fields`` not defined for', '"' + self.name + '".  Has validation been performed?')
        pass

    ##########################################################################

    def boundary_unit_set_grown_from_edge_index(self,index):
        """Parameter:
        index: index of grid point identifying a 'Unit' in the middle of a 
          domain boundary

        Returns a set object containing all of the 'Unit' objects aligned on
          the same boundary edge"""

        assert len(index) == 2, 'Grid.boundary_unit_set_grown_from_edge_index: "index" should be a sequence of two integers'

        unit = self.unit_with_index[index[0],index[1]]

        assert (unit.neighbor_count == 5 or unit.neighbor_count == 6), 'ERROR: boundary_unit_set_with_name_grown_from_edge_index: starting index must refer to a boundary element that is not a corner'

        unit_set_with_name = set((unit,))

        if unit.interior_direction_relative_index[0] == 0:

            if unit.index[0] != 0:

                relative_position = 1

                while unit.index[0]-relative_position != -1:
                    candidate = self.unit_with_index[
                        unit.index[0]-relative_position, unit.index[1]]
                    if candidate == None or candidate.neighbor_count == 8:
                        break;

                    unit_set_with_name.add(candidate)
                    relative_position += 1

            if unit.index[0] != self.index_bounds[0]-1:

                relative_position = 1

                while unit.index[0]+relative_position != self.index_bounds[0]:
                    candidate = self.unit_with_index[
                        unit.index[0]+relative_position, unit.index[1]]
                    if candidate == None or candidate.neighbor_count == 8:
                        break;

                    unit_set_with_name.add(candidate)
                    relative_position += 1

        else:

            if unit.index[1] != 0:

                relative_position = 1

                while unit.index[1]-relative_position != -1:
                    candidate = self.unit_with_index[
                        unit.index[0], unit.index[1]-relative_position]
                    if candidate == None or candidate.neighbor_count == 8:
                        break;

                    unit_set_with_name.add(candidate)
                    relative_position += 1

            if unit.index[1] != self.index_bounds[1] - 1:

                relative_position = 1

                while unit.index[1]+relative_position != self.index_bounds[1]:
                    candidate = self.unit_with_index[
                        unit.index[0], unit.index[1]+relative_position]
                    if candidate == None or candidate.neighbor_count == 8:
                        break;

                    unit_set_with_name.add(candidate)
                    relative_position += 1

        return unit_set_with_name

