# cython: profile=False
from __future__ import absolute_import

import numpy
cimport numpy
DTYPE = numpy.float_
ctypedef numpy.float_t DTYPE_t
import math
from . import unit

cdef class Interpolant:

    def __init__(self,absolute_position,output_derivative_orders,
                 exponents_sequence,data_unit_sequence):
        """interpolant.Interpolant.__init__

        Parameters:
        
        absolute_position: position at which result is calculated,
          in coordinate system of 'Grid' object
          May be a scalar in the 1D case
          
        output_derivative_orders: sequence indicating orders of derivatives
          to be taken in each direction
          May be a scalar in the 1D case
          
        exponents_sequence: contains sequences of exponents for all terms
          to be included in calculation
          In the 1D case, the highest exponent can be provided alone as an
            integer
          
        unit_sequence: contains all Unit objects from which data is to be
          extracted"""

        if (isinstance(absolute_position,float) 
            or isinstance(absolute_position,int)):
            dimension_count = 1
        else:
            dimension_count = len(absolute_position)

        data_unit_count = len(data_unit_sequence)

        assert data_unit_count != 0, 'ERROR: Interpolant.__init__: "data_unit_sequence" must contain at least one "Unit" object'

        # Check that 'data_unit_sequence' contains only Unit objects
        for unit_candidate in data_unit_sequence:
            assert isinstance(unit_candidate,unit.Unit), 'ERROR: Interpolant.__init__: "data_unit_sequence" may only contain objects belonging to class "Unit"'
            assert unit_candidate.grid.dimension_count == dimension_count, 'ERROR: Interpolant.__init__: all elements of "data_unit_sequence" must be associated with a "Grid" object with a "dimension_count" that is consistent with the dimension count implied by "absolute_position" argument'
            
        # This is used for fast identification of Unit objects later
        data_unit_numbers = [ unit_obj.number
                              for unit_obj in data_unit_sequence ]

        # Rows of term_matrix correspond to different TSE terms
        # Columns of term_matrix correspond to different data locations
        term_matrix = numpy.empty((data_unit_count,data_unit_count))
        
        if dimension_count == 1:
            ## In the 1D case, work with scalars rather than with sequences of
            ## a single element
            
            if not isinstance(output_derivative_orders,int):
                
                assert len(output_derivative_orders) == 1, 'ERROR: Interpolant.__init__: "output_derivative_orders" in 1D case must be either an integer or a sequence containing a single integer'
                output_derivative_orders = output_derivative_orders[0]
                assert isinstance(output_derivative_orders,int), 'ERROR: Interpolant.__init__: "output_derivative_orders" in 1D case must be either an integer or a sequence containing a single integer'
                
            if isinstance(exponents_sequence,int):
                exponents_sequence = range(exponents_sequence+1)

            for exponent in exponents_sequence:
                assert isinstance(exponent,int), 'ERROR: Interpolant.__init__: "exponents_sequence" in 1D case must be either an integer or a sequence containing only integers'

            assert len(exponents_sequence) == len(data_unit_sequence), 'ERROR: Interpolant.__init__: number of exponent values to consider (given in "exponents_sequence") is not equal to number of "Unit" objects provided in "data_unit_sequence"; system is overspecified or underspecified'

            ## Loop over the different TSE terms
            
            for exponent_set_number in range(data_unit_count):

                exponents = exponents_sequence[exponent_set_number]
                inv_denominator = 1.0 / math.factorial(exponents)

                ## Loop over the 'Unit' objects from which data will be taken
                
                for unit_number in range(data_unit_count):

                    # Fill 'term_matrix' with TSE terms
                    term_matrix[exponent_set_number,unit_number] = (
                        inv_denominator
                        * (data_unit_sequence[unit_number].coordinate
                           - absolute_position)**exponents)
        else:
            ## Higher dimensions
            
            assert len(exponents_sequence) == len(data_unit_sequence), 'ERROR: Interpolant.__init__: number of exponent values to consider (given in "exponents_sequence") is not equal to number of "Unit" objects provided in "data_unit_sequence"; system is overspecified or underspecified'

            if not isinstance(output_derivative_orders,tuple):
                output_derivative_orders = tuple(output_derivative_orders)

            ## Loop over the different TSE terms
                
            for exponent_set_number in range(data_unit_count):

                exponents = exponents_sequence[exponent_set_number]            

                denominator = 1.0
                for exponent in exponents:
                    denominator *= math.factorial(exponent)

                inv_denominator = 1.0 / float(denominator)

                ## Loop over the 'Unit' objects from which data will be taken
                
                for unit_number in range(data_unit_count):

                    unit_obj = data_unit_sequence[unit_number]

                    product = 1.0
                    for i in range(dimension_count):
                        product *= (unit_obj.coordinate[i]
                                    - absolute_position[i])**exponents[i]
                        
                    # Fill 'term_matrix' with TSE terms
                    term_matrix[exponent_set_number,unit_number] = inv_denominator*product

        # Determine the derivative orders for which weights are to be computed
        output_vector = numpy.zeros(data_unit_count)
        for exponent_set_number in range(data_unit_count):
            if (exponents_sequence[exponent_set_number]
                == output_derivative_orders):
                output_vector[exponent_set_number] = 1.0
                break

        # Solve to determine the weight for each data point
        coefficient_array = numpy.linalg.solve(term_matrix, output_vector)

        ## For efficiency, keep only the terms that are significant

        # Coefficients smaller than this will be ignored
        tol = 1e-14
        
        ignored_units = [ math.fabs(coefficient) < tol
                          for coefficient in coefficient_array ]
        ignored_count = ignored_units.count(True)
        
        if ignored_count != 0:

            ## Build new sequences consisting of only significant terms

            coefficient_sequence = []
            significant_data_unit_numbers = []            
            for index in range(data_unit_count):
                if not ignored_units[index]:
                    coefficient_sequence.append(coefficient_array[index])
                    significant_data_unit_numbers.append(
                        data_unit_numbers[index])
                    
            self.coefficient_array = numpy.array(coefficient_sequence)
            self.data_unit_numbers = significant_data_unit_numbers
            self.data_unit_count = data_unit_count - ignored_count
        else:
            self.coefficient_array = coefficient_array
            self.data_unit_numbers = data_unit_numbers
            self.data_unit_count = data_unit_count

        ## These definitions allow an interpolant described by an Interpolant
        ## object 'obj' to be evaluated as either 'obj(data)', or 'obj[data]'

        #self.__getitem__ = self.value

    cpdef double value(self,data):
        """Interpolant.value

        Parameter:

        data: sequence of values ordered by numerical label of corresponding
        'Unit' object, with one element for each 'Unit' object in containing
        'Grid' object

        Returns value of interpolating polynomial at the location specified
        at creation of the 'Interpolant' object"""

        cdef double accumulator

        ## Sum the values in 'data' with the weights computed earlier

        accumulator = 0
        
        for data_unit_number in range(self.data_unit_count):
            accumulator += (self.coefficient_array[data_unit_number]
                    * data[self.data_unit_numbers[data_unit_number]])
            
        return accumulator
    
    def __call__(self,data):
        """Interpolant.value

        Parameter:

        data: sequence of values ordered by numerical label of corresponding
        'Unit' object, with one element for each 'Unit' object in containing
        'Grid' object

        Returns value of interpolating polynomial at the location specified
        at creation of the 'Interpolant' object"""
        
        ## Sum the values in 'data' with the weights computed earlier

        accumulator = 0
        
        for data_unit_number in range(self.data_unit_count):
            accumulator += (self.coefficient_array[data_unit_number]
                    * data[self.data_unit_numbers[data_unit_number]])
            
        return accumulator

