import pygdh
import numpy
import math

class LeftBoundaryVolume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        #self.dx_left = self.default_interpolant(-1.0, 1, 1)
        self.dx_left = self.default_interpolant(-1.0, 1, 2)        
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

class RightBoundaryVolume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        #self.dx_right = self.default_interpolant(1.0, 1, 1)
        self.dx_right = self.default_interpolant(1.0, 1, 2)        

# Objects of this class describe the spatial domains on which models will be
# solved.
class Domain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, unit_count, x_coordinates):

        unit_classes = [ LeftBoundaryVolume ]
        
        for i in range(1,unit_count-1):
            unit_classes.append(Volume)
            
        unit_classes.append(RightBoundaryVolume)
        
        self.initialize_grid(name,
                             # The independent variable will be named 'y'
                             field_names=['y'],
                             # The dependent variable will be named 'x',
                             # and 'y' will be computed at the positions
                             # specified by the x_coordinates parameter
                             coordinate_values={'x': x_coordinates},
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object                             
                             unit_classes=unit_classes, 
                             stored_solution_count=2)

# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent models, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class BaseSubmodel(pygdh.Model):

    def __init__(self, alpha):

        # Save the argument value as an object member
        self.alpha = alpha

    def add_boundary_source(self,boundary_source):

        self.boundary_source = boundary_source 

# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent models, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class LeftSubmodel(BaseSubmodel):

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        self.unknown[1][:,:] = False
        # left boundary
        vol = self.grid[0].unit_with_number[0]
        self.unknown[0][0,vol.number] = False

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            vol = self.grid[0].unit_with_number[vol_number]
            self.unknown[0][0,vol.number] = True

        # right boundary
        vol = self.grid[0].unit_with_number[-1]
        self.unknown[0][0,vol.number] = True

    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.bulk_pde: 1,
                                       self.right_boundary_pde: 1}

    def right_boundary_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[0].field[0][0]

        # Past solution values
        y_1 = self.grid[0].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))
                        + self.timestep_size * self.boundary_source())

    def right_boundary_term(self):
        vol = self.grid[0].unit_with_number[-1]
        y = self.grid[0].field[0][0]
        return -self.alpha/vol.volume*(vol.dx_right(y) - vol.dx_left(y))
        
    def assign_equations(self):

        grid_equations = self.equations[0]

        # Left boundary
        grid_equations[0] = []

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            grid_equations[vol_number] = [self.bulk_pde]

        # Right boundary
        grid_equations[-1] = [self.right_boundary_pde]

    def set_boundary_values(self):

        ## Boundary with the prescribed value is on the left side of the left
        ## domain
        y_left = self.grid[0].field[0][0]

        # Left boundary
        y_left[0] = 0.

    # This sets the initial values of the independent variables on the region
    # represented by an object of this class.
    def set_initial_conditions(self):

        # Defined for clarity
        y = self.grid[0].field[0][0]

        # Set the value of the independent variable on each volume
        for vol in self.grid[0].unit_with_number:
            y[vol.number] = (
                vol.coordinate / self.alpha
                - 0.5*math.sin(0.5*math.pi*vol.coordinate))

    def bulk_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[0].field[0][0]

        # Past solution values
        y_1 = self.grid[0].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))*vol.volume
                        - self.timestep_size * self.alpha
                        * (vol.dx_right(y0) - vol.dx_left(y0)))


    def process_solution(self):

        tol_squared = 1e-2

        for vol_number in range(self.grid[0].unit_count):

            vol = self.grid[0].unit_with_number[vol_number]
            assert (vol.coordinate/self.alpha**2 - 0.5*math.exp(-(0.5*math.pi*self.alpha)**2*self.time[0])*math.sin(0.5*math.pi*vol.coordinate) - self.grid[0].field[0][0,vol_number])**2 < tol_squared

# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent models, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class RightSubmodel(BaseSubmodel):

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):
        self.unknown[0][:,:] = False
        # left boundary
        vol = self.grid[1].unit_with_number[0]
        self.unknown[1][0,vol.number] = True

        # Interior volumes
        for vol_number in range(1, self.grid[1].unit_count-1):
            vol = self.grid[1].unit_with_number[vol_number]
            self.unknown[1][0,vol.number] = True

        # right boundary
        vol = self.grid[1].unit_with_number[-1]
        self.unknown[1][0,vol.number] = True

    def left_boundary_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[1].field[0][0]

        # Past solution values
        y_1 = self.grid[1].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))
                        + self.timestep_size * self.boundary_source())

    def left_boundary_term(self):
        vol = self.grid[1].unit_with_number[0]
        y = self.grid[1].field[0][0]

        return -self.alpha/vol.volume*(vol.dx_right(y) - vol.dx_left(y))
        
    def right_boundary_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[1].field[0][0]

        # Past solution values
        y_1 = self.grid[1].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))*vol.volume
                        + self.timestep_size
                        * (-1.0 + self.alpha * vol.dx_left(y0)))

    def assign_equations(self):

        grid_equations = self.equations[1]

        # Left boundary
        grid_equations[0] = [self.left_boundary_pde]

        # Interior volumes
        for vol_number in range(1, self.grid[1].unit_count-1):
            grid_equations[vol_number] = [self.bulk_pde]

        # Right boundary
        grid_equations[-1] = [self.right_boundary_pde]

    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.left_boundary_pde: 1,
                                       self.bulk_pde: 1,
                                       self.right_boundary_pde: 1}

    # This sets the initial values of the independent variables on the region
    # represented by an object of this class.
    def set_initial_conditions(self):

        # Defined for clarity
        y = self.grid[1].field[0][0]

        # Set the value of the independent variable on each volume
        for vol in self.grid[1].unit_with_number:
            y[vol.number] = (
                vol.coordinate / self.alpha
                - 0.5*math.sin(0.5*math.pi*vol.coordinate))

    def bulk_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[1].field[0][0]

        # Past solution values
        y_1 = self.grid[1].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))*vol.volume
                        - self.timestep_size * self.alpha
                        * (vol.dx_right(y0) - vol.dx_left(y0)))


    def process_solution(self):

        tol_squared = 1e-2

        for vol_number in range(self.grid[1].unit_count):

            vol = self.grid[1].unit_with_number[vol_number]
            assert (vol.coordinate/self.alpha**2 - 0.5*math.exp(-(0.5*math.pi*self.alpha)**2*self.time[0])*math.sin(0.5*math.pi*vol.coordinate) - self.grid[1].field[0][0,vol_number])**2 < tol_squared        
    
unit_count = 6
left_domain = Domain('left_domain',unit_count,numpy.linspace(0.0,0.5,unit_count))
right_domain = Domain('right_domain',unit_count,numpy.linspace(0.5,1.0,unit_count))
        
# Create an object instance, setting alpha in both regions to 1.0
alpha = 1.0
left_submodel = LeftSubmodel(alpha)
right_submodel = RightSubmodel(alpha)

left_submodel.add_boundary_source(right_submodel.left_boundary_term)
right_submodel.add_boundary_source(left_submodel.right_boundary_term)

simulation = pygdh.Simulation()
# Output will be automatically generated for Grids declared to Simulation here
simulation.initialize_simulation([left_domain,right_domain],[right_submodel,left_submodel],stored_solution_count=2)

number_of_timesteps = 3
timestep_size = 0.01

# The names of all output files will begin with this string
filename_root = 'coordinated'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

# Calculate solution and write output to file
simulation.solve_time_dependent_system(filename_root, output_types,
                                        timestep_count=number_of_timesteps,
                                        timestep_size=timestep_size)
