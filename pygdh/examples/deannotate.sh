#!/bin/sh
# It shouldn't be necessary to run this unless one is updating example code for the documentation.
for i in multiply_connected PDE compiling nonlinear postprocessing two_dimensions coordination submodel ODE ODE_in_time multiple_grids output system
do
    cd $i
    for j in *_annotated.py
    do
	sed '/#### SPHINX SNIPPET/d' $j > ${j%_annotated.py}.py
    done
    cd ..
done
