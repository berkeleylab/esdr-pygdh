import pygdh
import numpy
import math

# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define 'Volume'-
    # associated quantities that are not automatically created by PyGDH.
    def define_variables(self):

        # Store position of left boundary, relative to 'Volume' grid point
        self.left = self.relative_position_with_local_position(-1)
        # Store position of center, relative to 'Volume' grid point
        self.center = self.relative_position_with_local_position(0)
        # Store position of right boundary, relative to 'Volume' grid point
        self.right = self.relative_position_with_local_position(1)

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        # Operator for estimating first derivative at the volume center
        self.dx_center = self.default_interpolant(0.0, 1, 3)
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)
        # Operator for estimating function value at left boundary of volume
        self.interpolate_left = self.default_interpolant(-1.0, 0, 2)
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        # Operator for estimating function value at right boundary of volume
        self.interpolate_right = self.default_interpolant(1.0, 0, 2)

# Objects of this class describe the spatial domains on which models will be
# solved.
class Sphere(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, unit_count):
        self.initialize_grid(name,
                             coordinate_values = {
                                 'r' :  numpy.linspace(0.0, 1.0, unit_count)},
                             coordinate_order=['r'],
                             field_names=['u'],
                             unit_classes=[ Volume for i in range(unit_count) ])

    # Variables that are used by multiple methods and/or additional output
    # variables should be defined here. This routine is called once before
    # any numerical solutions are generated.
    def define_field_variables(self):

        # 'numpy.empty' creates an NumPy array of the specified size, with
        # uninitialized element values. The specified sizes here are consistent
        # with the solution arrays.
        self.tr_r0 = numpy.empty(self.unit_count)
        self.tr_center = numpy.empty(self.unit_count)        
        self.tr_r1 = numpy.empty(self.unit_count)
        
        self.tt_r0 = numpy.empty(self.unit_count)
        self.tt_center = numpy.empty(self.unit_count)        
        self.tt_r1 = numpy.empty(self.unit_count)

        self.tr_output = numpy.empty(self.unit_count)
        self.tt_output = numpy.empty(self.unit_count)        

    # This is an optional method for specifying additional output variables
    def set_output_fields(self):

        # This is a dictionary, with pairs of descriptive strings and arrays
        # with the same shape as the field variable arrays
        self.output_fields = {'radial_stress':self.tr_output,
                              'tangential_tress':self.tt_output}

# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent models, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class StaticSphere(pygdh.Model):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_model' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, Lame_lambda, mu, p):

        self.Lame_lambda = Lame_lambda
        self.mu = mu
        self.tr_surface = -p

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # "Left" boundary
        vol = self.grid[0].unit_with_number[0]
        self.unknown[0][0,vol.number] = False

        # Interior unit_with_number
        for vol_number in range(1, self.grid[0].unit_count-1):
            vol = self.grid[0].unit_with_number[vol_number]
            self.unknown[0][0,vol.number] = True

        # "Right" boundary, value to be copied from "Left" boundary of Right
        # grid
        vol = self.grid[0].unit_with_number[-1]
        self.unknown[0][0,vol.number] = True

    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.interior_momentum_balance: 1,
                                       self.surface_momentum_balance: 1}
        
    def assign_equations(self):

        domain_equations = self.equations[0]
        # "Left" boundary
        domain_equations[0] = []

        # Interior unit_with_number
        for vol_number in range(1, self.grid[0].unit_count-1):
            domain_equations[vol_number] = [self.interior_momentum_balance]

        # "Right" boundary, value to be copied from "Left" boundary of Right
        # grid
        domain_equations[-1] = [self.surface_momentum_balance]
    
    def set_boundary_values(self):

        # The boundary with the prescribed value is in the left field
        u = self.grid[0].field[0][0]

        # "Left" boundary
        u[0] = 0.

    # This optional method provides an opportunity to calculate and store
    # values in the global variables, before the equation methods are
    # evaluated (on each timestep, for a time-dependent model).
    def calculate_global_values(self):

        # Defined for efficiency and clarity
        u = self.grid[0].field[0][0]
        
        for vol in self.grid[0].unit_with_number:

            ## Calculate stress components at left edge of volume
            
            if vol.number == 0:
                # Since the value of u is given at the origin, the stress
                # components at the origin are not needed in the calculations.
                # However, it may be desirable to include them in the output.
                #
                # Limiting forms of the stress components are needed at the
                # origin because they depend on u / r.  Unfortunately, 
                # obtaining this ahead of time usually requires some
                # analytical result.
                u__r = vol.dx_left(u)
            else:
                u__r = (vol.interpolate_left(u)
                       / (vol.coordinate + vol.left))

            trE = vol.dx_left(u) + 2*u__r

            self.grid[0].tr_r0[vol.number] = (self.Lame_lambda*trE
                                             + 2*self.mu*vol.dx_left(u))
            self.grid[0].tt_r0[vol.number] = (self.Lame_lambda*trE
                                             + 2*self.mu*u__r)

            ## Calculate stress components at center of volume

            self.grid[0].tr_center[vol.number] = (
                self.Lame_lambda*trE
                + 2*self.mu*vol.dx_center(u))
            if vol.number == 0:
                self.grid[0].tt_center[vol.number] = (
                    self.grid[0].tr_center[vol.number])
            else:
                self.grid[0].tt_center[vol.number] = (
                    self.Lame_lambda*trE
                    + 2*self.mu*vol.interpolate(u)
                    / (vol.coordinate + vol.center))
    
            ## Calculate stress components at right edge of volume
            
            trE = (vol.dx_right(u)
                   + 2*vol.interpolate_right(u)
                   / (vol.coordinate + vol.right))
            
            self.grid[0].tr_r1[vol.number] = (
                self.Lame_lambda*trE
                + 2*self.mu*vol.dx_right(u))
            self.grid[0].tt_r1[vol.number] = (
                self.Lame_lambda*trE
                + 2*self.mu*vol.interpolate_right(u)
                / (vol.coordinate + vol.right))
                
    def interior_momentum_balance(self, vol, residual):

        r = self.grid[0].field[0][0]

        residual[0] = (self.grid[0].tr_r1[vol.number]
                       - self.grid[0].tr_r0[vol.number]
                       + 2*(self.grid[0].tr_center[vol.number]
                            - self.grid[0].tt_center[vol.number])
                       /(vol.coordinate + vol.center)
                       *(vol.right - vol.left))

    def surface_momentum_balance(self, vol, residual):

        r = self.grid[0].field[0][0]

        residual[0] = (self.tr_surface - self.grid[0].tr_r0[vol.number]
                       + 2*(self.grid[0].tr_center[vol.number]
                            - self.grid[0].tt_center[vol.number])
                       *(vol.right - vol.left))

    # This routine is called once after a solution is obtained
    # (on each timestep in a time-dependent model)
    def process_solution(self):

        grid0 = self.grid[0]
        
        grid0.tr_output[0] = grid0.tr_r0[0]
        grid0.tt_output[0] = grid0.tt_r0[0]        

        for vol_number in range(1,grid0.unit_count-1):
            grid0.tr_output[vol_number] = grid0.tr_center[vol_number]
            grid0.tt_output[vol_number] = grid0.tt_center[vol_number]

        grid0.tr_output[-1] = grid0.tr_r1[-1]
        grid0.tt_output[-1] = grid0.tt_r1[-1]

        tol_squared = 1e-16
        
        for vol_number in range(self.grid[0].unit_count):

            vol = self.grid[0].unit_with_number[vol_number]
            assert (self.tr_surface/(3.*self.Lame_lambda + 2.*self.mu)*vol.coordinate - self.grid[0].field[0][0,vol_number])**2 < tol_squared

## Create an object instance of the user's class, call with optional arguments
unit_count = 41
grid_obj = Sphere('sphere', unit_count)

model = StaticSphere(1.0,1.0,1.0)

# The names of all output files will begin with this string
filename_root = 'output'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model])

# Calculate solution and write output to file
simulation.solve_time_independent_system(filename_root, output_types)
