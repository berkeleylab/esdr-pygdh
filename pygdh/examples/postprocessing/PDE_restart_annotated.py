import pygdh
import PDE_body

# Create the Grid object representing the model domain with 11 volumes
volume_count = 11
alpha = 1.0
grid_obj = PDE_body.Domain('domain', alpha, volume_count)

# Create an object instance
model = PDE_body.PDE(alpha)

number_of_timesteps = 2
timestep_size = 0.01

saved_solution_filename_root = 'PDE_postprocess'

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model],stored_solution_count=2)

# Load the saved solution file
simulation.load_solutions(saved_solution_filename_root)

simulation.restore_from_timestep(2)

restart_solution_filename_root = 'PDE_restart'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

# Calculate solution and write output to file
simulation.solve_time_dependent_system(restart_solution_filename_root,
                                    output_types,
                                    timestep_count=number_of_timesteps,
                                    timestep_size=timestep_size)

simulation.input.close()
