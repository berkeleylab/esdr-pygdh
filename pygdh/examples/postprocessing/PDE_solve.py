import pygdh
import PDE_body

# Create the Grid object representing the model domain with 11 volumes
volume_count = 11
alpha = 1.0
grid_obj = PDE_body.Domain('domain', alpha, volume_count)

# Create an object instance
model = PDE_body.PDE(alpha)

number_of_timesteps = 3
timestep_size = 0.01

# The names of all output files will begin with this string
filename_root = 'PDE_postprocess'

# One output file will be created for each format given here
output_types = ['HDF5']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model],stored_solution_count=2)

# Calculate solution and write output to file
simulation.solve_time_dependent_system(filename_root, output_types,
                                    timestep_count=number_of_timesteps,
                                    timestep_size=timestep_size)
