import pygdh
import PDE_body

#### SPHINX SNIPPET START LOAD
# Create the Grid object representing the model domain with 11 volumes
volume_count = 11
alpha = 1.0
grid_obj = PDE_body.Domain('domain', alpha, volume_count)

# Create an object instance
model = PDE_body.PDE(alpha)

# Load the saved solution file
filename_root = 'PDE_postprocess'

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model],stored_solution_count=2)

simulation.load_solutions(filename_root)

simulation.restore_from_timestep(2)
#### SPHINX SNIPPET END LOAD

#### SPHINX SNIPPET START PLOT
# Plot with matplotlib
import matplotlib.pyplot as plt

plt.plot(grid_obj.coordinates_list[0],model.grid[0].field[0][0])
plt.xlabel('x')
plt.ylabel('y')
plt.savefig(filename_root + '.png')
plt.show()

simulation.input.close()
#### SPHINX SNIPPET END PLOT
