#### SPHINX SNIPPET START IMPORT
import pygdh
import numpy
import math
#### SPHINX SNIPPET END IMPORT

# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

# Objects of this class describe the spatial domains on which models will be
# solved.
class Domain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, volume_count):

        self.initialize_grid(name,
                             # The independent variable will be named 'y'
                             field_names=['y'],
                             # The dependent variable will be named 'x',
                             # and 'y' will be computed at the specified number
                             # of equally-spaced values of 'x'
                             coordinate_values={
                                 'x': numpy.linspace(0.0, 1.0, volume_count)},
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object                             
                             unit_classes=[Volume for i in range(volume_count)],
                             stored_solution_count=2)

# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent models, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class PDE(pygdh.Model):

#### SPHINX SNIPPET START MODEL INIT
    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_model' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, alpha):

        # Save the argument value as an object member
        self.alpha = alpha
#### SPHINX SNIPPET END MODEL INIT

#### SPHINX SNIPPET START IC
    # This sets the initial values of the independent variables on the region
    # represented by an object of this class.
    def set_initial_conditions(self):

        # Defined for clarity
        y = self.grid[0].field[0][0]

        # Set the value of the independent variable on each volume
        for vol in self.grid[0].unit_with_number:
            y[vol.number] = (
                vol.coordinate / self.alpha
                - 0.5*math.sin(0.5*math.pi*vol.coordinate))
#### SPHINX SNIPPET END IC

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # "Left" boundary
        vol = self.grid[0].unit_with_number[0]
        self.unknown[0][0,vol.number] = False

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            vol = self.grid[0].unit_with_number[vol_number]
            self.unknown[0][0,vol.number] = True

        # "Right" boundary
        vol = self.grid[0].unit_with_number[-1]
        self.unknown[0][0,vol.number] = True

    # These methods represent the governing equation and are named and defined
    # by the user. These must accept three particular parameters and must store
    # results in the data structure associated with the last parameter.
#### SPHINX SNIPPET START PDE
    def pde(self, vol, residual):

        # Present solution values
        y0 = self.grid[0].field[0][0]

        # Past solution values
        y_1 = self.grid[0].field[-1][0]

        # FVM representation of governing equation
        residual[0] = ((vol.interpolate(y0)
                        - vol.interpolate(y_1))*vol.volume
                       - self.timestep_size * self.alpha
                       * (vol.dx_right(y0)
                          - vol.dx_left(y0)))
#### SPHINX SNIPPET END PDE

#### SPHINX SNIPPET START PDE BOUNDARY
    def pde_boundary(self, vol, residual):

        # Present solution values
        y0 = self.grid[0].field[0][0]

        # Past solution values
        y_1 = self.grid[0].field[-1][0]

        # FVM representation of governing equation, incorporating boundary
        # condition
        residual[0] = ((vol.interpolate(y0)
                     - vol.interpolate(y_1))*vol.volume
                    + self.timestep_size
                    * (-1.0 + self.alpha * vol.dx_left(y0)))
#### SPHINX SNIPPET END PDE BOUNDARY

    # This method is used to define boundary conditions in which the solution
    # value is specified. This must be consistent with the contents of the
    # 'declare_unknowns' methods defined in the corresponding 'Grid' objects.
    def set_boundary_values(self):

        # Defined for clarity; there is only one 'Grid' object, with index 0,
        # and one solution field 'y', with index 0
        y = self.grid[0].field[0][0]

        # "Left" boundary
        y[0] = 0.0

#### SPHINX SNIPPET START SCALAR COUNTS        
    # This is a required method that notifies PyGDH about the number of scalar
    # values returned by each governing equation method.
    def set_equation_scalar_counts(self):
        
        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.pde: 1,
                                       self.pde_boundary: 1}
#### SPHINX SNIPPET END SCALAR COUNTS
        
    # This is a required method that notifies PyGDH about the methods that
    # describe governing equations, and indicates where in the domain the
    # equations apply. This must be consistent with the 'declare_unknowns'
    # methods of the corresponding 'Grid' objects.
    def assign_equations(self):

        # Defined for clarity, equations to be defined on the only Grid,
        # with index 0
        domain_equations = self.equations[0]

        # "Left" boundary
        domain_equations[0] = []

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            domain_equations[vol_number] = [self.pde]

        # "Right" boundary
        domain_equations[-1] = [self.pde_boundary]

    def process_solution(self):

        tol_squared = 1e-6
        
        for vol_number in range(self.grid[0].unit_count):

            vol = self.grid[0].unit_with_number[vol_number]
            assert (vol.coordinate/self.alpha**2 - 0.5*math.exp(-(0.5*math.pi*self.alpha)**2*self.time[0])*math.sin(0.5*math.pi*vol.coordinate) - self.grid[0].field[0][0,vol_number])**2 < tol_squared
        
#### SPHINX SNIPPET START MAIN
# Create the Grid object representing the model domain, with 11 volumes
volume_count = 11
grid_obj = Domain('domain', volume_count)

# Create an object instance, setting alpha = 1.0
model = PDE(1.0)

number_of_timesteps = 3
timestep_size = 0.01

# The names of all output files will begin with this string
filename_root = 'PDE'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model],stored_solution_count=2)

# Calculate solution and write output to file
simulation.solve_time_dependent_system(filename_root, output_types,
                                    timestep_count=number_of_timesteps,
                                    timestep_size=timestep_size)
#### SPHINX SNIPPET END MAIN
