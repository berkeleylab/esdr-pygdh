import pygdh
import numpy
import math

# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

#### SPHINX SNIPPET START GRID        
# Objects of this class describe the spatial domains on which models will be
# solved.
class Domain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, unit_count, x_coordinates):

        self.initialize_grid(name,
                             # The independent variable will be named 'y'
                             field_names=['y'],
                             # The dependent variable will be named 'x',
                             # and 'y' will be computed at the positions
                             # specified by the x_coordinates parameter
                             coordinate_values={'x': x_coordinates},
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object                             
                             unit_classes=[Volume for i in range(unit_count)],
                             stored_solution_count=2)
#### SPHINX SNIPPET END GRID        

# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent models, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class MultipleGrids(pygdh.Model):

#### SPHINX SNIPPET START MODEL INIT     
    def __init__(self, alpha_l, alpha_r):

        # Save the argument value as an object member
        self.alpha_l = alpha_l
        self.alpha_r = alpha_r
#### SPHINX SNIPPET END MODEL INIT     
        
    # This sets the initial values of the independent variables on the region
    # represented by an object of this class.
    def set_initial_conditions(self):

        # Defined for clarity
        y = self.grid[0].field[0][0]

        # Set the value of the independent variable on each volume
        for vol in self.grid[0].unit_with_number:
            y[vol.number] = (
                vol.coordinate / self.alpha_l
                - 0.5*math.sin(0.5*math.pi*vol.coordinate))

        # Defined for clarity
        y = self.grid[1].field[0][0]

        # Set the value of the independent variable on each volume
        for vol in self.grid[1].unit_with_number:
            y[vol.number] = (
                vol.coordinate / self.alpha_r
                - 0.5*math.sin(0.5*math.pi*vol.coordinate))

#### SPHINX SNIPPET START DECLARE UNKNOWNS
    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        for grid_number in range(2):
            # left boundary
            vol = self.grid[grid_number].unit_with_number[0]
            self.unknown[grid_number][0,vol.number] = False

            # Interior volumes
            for vol_number in range(1, self.grid[grid_number].unit_count-1):
                vol = self.grid[grid_number].unit_with_number[vol_number]
                self.unknown[grid_number][0,vol.number] = True

            # right boundary
            vol = self.grid[grid_number].unit_with_number[-1]
            self.unknown[grid_number][0,vol.number] = True
#### SPHINX SNIPPET END DECLARE UNKNOWNS

#### SPHINX SNIPPET START LEFT PDE            
    def left_grid_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[0].field[0][0]

        # Past solution values
        y_1 = self.grid[0].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))*vol.volume
                        - self.timestep_size * self.alpha_l
                        * (vol.dx_right(y0) - vol.dx_left(y0)))
#### SPHINX SNIPPET END LEFT PDE

#### SPHINX SNIPPET START INTERFACE PDE
    def interface_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[0].field[0][0]

        # Past solution values
        y_1 = self.grid[0].field[-1][0]

        right_y0 = self.grid[1].field[0][0]
        right_vol = self.grid[1].unit_with_number[0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))*(vol.volume + right_vol.volume)
                        + self.timestep_size
                        * (-self.alpha_r*right_vol.dx_right(right_y0)
                           +self.alpha_l*vol.dx_left(y0)))
#### SPHINX SNIPPET END INTERFACE PDE

#### SPHINX SNIPPET START RIGHT PDE            
    def right_grid_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[1].field[0][0]

        # Past solution values
        y_1 = self.grid[1].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))*vol.volume
                        - self.timestep_size * self.alpha_r
                        * (vol.dx_right(y0) - vol.dx_left(y0)))
#### SPHINX SNIPPET END RIGHT PDE

#### SPHINX SNIPPET START RIGHT BOUNDARY PDE            
    def right_grid_right_boundary_pde(self, vol, residuals):

        # Present solution values
        y0 = self.grid[1].field[0][0]

        # Past solution values
        y_1 = self.grid[1].field[-1][0]

        residuals[0] = ((vol.interpolate(y0) - vol.interpolate(y_1))*vol.volume
                        + self.timestep_size
                        * (-1.0 + self.alpha_r * vol.dx_left(y0)))
#### SPHINX SNIPPET END RIGHT BOUNDARY PDE

#### SPHINX SNIPPET START BC
    def set_boundary_values(self):

        ## Boundary with the prescribed value is on the left side of the left
        ## domain
        y_left = self.grid[0].field[0][0]

        # Left boundary
        y_left[0] = 0.

        ## In the right domain, the value at left boundary is taken from the
        ## adjacent left domain
        y_right = self.grid[1].field[0][0]

        # Left boundary
        y_right[0] = y_left[-1]
#### SPHINX SNIPPET END BC

    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.left_grid_pde: 1,
                                       self.interface_pde: 1,
                                       self.right_grid_pde: 1,
                                       self.right_grid_right_boundary_pde: 1}

#### SPHINX SNIPPET START ASSIGN EQUATIONS
    def assign_equations(self):

        ## Left grid

        left_grid_equations = self.equations[0]

        # Left boundary
        left_grid_equations[0] = []

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            left_grid_equations[vol_number] = [self.left_grid_pde]

        # Right boundary
        left_grid_equations[-1] = [self.interface_pde]
        
        ## Right grid

        right_grid_equations = self.equations[1]

        # Left boundary, value to be copied from left grid
        right_grid_equations[0] = []

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            right_grid_equations[vol_number] = [self.right_grid_pde]

        # Right boundary
        right_grid_equations[-1] = [self.right_grid_right_boundary_pde]
#### SPHINX SNIPPET END ASSIGN EQUATIONS

    def process_solution(self):

        tol_squared = 1e-6

        assert self.alpha_l == self.alpha_r
        
        for vol_number in range(self.grid[0].unit_count):

            vol = self.grid[0].unit_with_number[vol_number]
            assert (vol.coordinate/self.alpha_l**2 - 0.5*math.exp(-(0.5*math.pi*self.alpha_l)**2*self.time[0])*math.sin(0.5*math.pi*vol.coordinate) - self.grid[0].field[0][0,vol_number])**2 < tol_squared

#### SPHINX SNIPPET START MAIN
# The left and right domains are both objects of the 'Domain' class,
# but they must be initialized to represent different regions
unit_count1 = 6
unit_count2 = 6
grid_obj1 = Domain('left_domain',unit_count1,
                   numpy.linspace(0., 0.5, unit_count1))
grid_obj2 = Domain('right_domain',unit_count2,
                   numpy.linspace(0.5, 1., unit_count2))

# Create an object instance, setting alpha_l = 1.0, alpha_r = 1.0
model = MultipleGrids(1.0, 1.0)

number_of_timesteps = 3
timestep_size = 0.01

# The names of all output files will begin with this string
filename_root = 'multiple_grids'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj1,grid_obj2],[model],stored_solution_count=2)

# Calculate solution and write output to file
simulation.solve_time_dependent_system(filename_root, output_types,
                                    timestep_count=number_of_timesteps,
                                    timestep_size=timestep_size)
#### SPHINX SNIPPET END MAIN
