#### SPHINX SNIPPET START IMPORT
import pygdh
import numpy
import math
#### SPHINX SNIPPET END IMPORT

#### SPHINX SNIPPET START VOLUME
# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):
#### SPHINX SNIPPET END VOLUME

#### SPHINX SNIPPET START DEFINE INTERPOLANTS    
    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)
#### SPHINX SNIPPET END DEFINE INTERPOLANT        

#### SPHINX SNIPPET START GRID        
# Objects of this class describe the spatial domains on which models will be
# solved.
class Domain(pygdh.Grid):
#### SPHINX SNIPPET END GRID

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, volume_count):

#### SPHINX SNIPPET START INITALIZE GRID                
        self.initialize_grid(name,
                             # The independent variable will be named 'y'
                             field_names=['y'],
                             # The dependent variable will be named 'x',
                             # and 'y' will be computed at the specified number
                             # of equally-spaced values of 'x'
                             coordinate_values={
                                 'x': numpy.linspace(0.0, 1.0, volume_count)},
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object

                             unit_classes=[
                                 Volume for i in range(volume_count) ]

                             )
#### SPHINX SNIPPET END INITALIZE GRID                        

#### SPHINX SNIPPET START MODEL
# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent models, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class ODE(pygdh.Model):
#### SPHINX SNIPPET END MODEL

#### SPHINX SNIPPET START MODEL INIT    
    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_model' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, alpha):

        # Save the argument value as an object member
        self.alpha = alpha
#### SPHINX SNIPPET END MODEL INIT

#### SPHINX SNIPPET START DECLARE UNKNOWNS
    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # "Left" boundary
        vol = self.grid[0].unit_with_number[0]
        self.unknown[0][0,vol.number] = False

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            vol = self.grid[0].unit_with_number[vol_number]
            self.unknown[0][0,vol.number] = True

        # "Right" boundary
        vol = self.grid[0].unit_with_number[-1]
        self.unknown[0][0,vol.number] = False
#### SPHINX SNIPPET END DECLARE UNKNOWNS

#### SPHINX SNIPPET START ODE
    # This method represents a governing equation and is named and defined by
    # the user. It must accept three particular parameters and must store
    # results in the data structure associated with the last parameter.
    def ode(self, vol, residual):

        # Defined for clarity; there is only one 'Grid' object, with index 0,
        # and one solution field 'y', with index 0
        y = self.grid[0].field[0][0]

        # FVM representation of governing equation
        residual[0] = (vol.dx_right(y) - vol.dx_left(y)
                       + vol.interpolate(y) * self.alpha**2 * vol.volume)
#### SPHINX SNIPPET END ODE

#### SPHINX SNIPPET START BOUNDARY VALUES
    # This method is used to define boundary conditions in which the solution
    # value is specified. This must be consistent with the contents of the
    # 'declare_unknowns' methods defined in the corresponding 'Grid' objects.
    def set_boundary_values(self):

        # Defined for clarity; there is only one 'Grid' object, with index 0,
        # and one solution field 'y', with index 0
        y = self.grid[0].field[0][0]

        # "Left" domain boundary
        y[0] = 0.0

        # "Right" domain boundary
        y[-1] = 1.0
        
#### SPHINX SNIPPET END BOUNDARY VALUES

#### SPHINX SNIPPET START SCALAR COUNTS
    # This is a required method that notifies PyGDH about the number of scalar
    # values returned by each governing equation method.
    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.ode: 1}
#### SPHINX SNIPPET END SCALAR COUNTS

#### SPHINX SNIPPET START_ASSIGN EQUATIONS
    # This is a required method that notifies PyGDH about the methods that
    # describe governing equations, and indicates where in the domain the
    # equations apply. This must be consistent with the 'declare_unknowns'
    # methods of the corresponding 'Grid' objects.
    def assign_equations(self):

        # Defined for clarity, equations to be defined on the only Grid,
        # with index 0
        domain_equations = self.equations[0]

        # "Left" domain boundary
        domain_equations[0] = []

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            domain_equations[vol_number] = [self.ode]

        # "Right" domain boundary
        domain_equations[-1] = []
#### SPHINX SNIPPET END ASSIGN EQUATIONS        

    def process_solution(self):

        tol_squared = 1e-8
        
        for vol_number in range(self.grid[0].unit_count):

            vol = self.grid[0].unit_with_number[vol_number]
            assert (self.grid[0].field[0][0,vol_number] - math.sin(self.alpha*vol.coordinate)/math.sin(self.alpha))**2 < tol_squared
            
#### SPHINX SNIPPET START MAIN
# Create the 'Grid' object representing the model domain
unit_count = 11

grid_obj = Domain('domain',unit_count)

# Create an object instance, using 11 volumes and setting alpha = 1.0
model = ODE(1.0)

# The names of all output files will begin with this string
filename_root = 'ODE'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model])

# Calculate solution and write output to file
simulation.solve_time_independent_system(filename_root, output_types)
#### SPHINX SNIPPET END MAIN
