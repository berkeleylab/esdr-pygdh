import numpy
import pygdh
import math

# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):
        
        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant((-1.0,0),(1,0),(1,1))
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant((1.0,0),(1,0),(1,1))
        # Operator for estimating first derivative at bottom boundary of volume
        self.dy_down = self.default_interpolant((0,-1.0),(0,1),(1,1))
        # Operator for estimating first derivative at top boundary of volume
        self.dy_up = self.default_interpolant((0,1.0),(0,1),(1,1))

# Objects of this class describe the spatial domains on which models will be
# solved.
class Domain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, unit_count):

        self.initialize_grid(name,
                             coordinate_values = {
                                 'x': numpy.linspace(0., 1., unit_count),
                                 'y': numpy.linspace(0., 1., unit_count)},
                             coordinate_order=['x','y'],
                             field_names=['T'],
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object                             
                             unit_classes=[ [ Volume
                                              for j in range(unit_count) ]
                                            for i in range(unit_count)])

    def define_unit_set_with_name(self):

        # Descriptive names for boundaries of rectangular domain
        self.unit_set_with_name = {
            'left': self.boundary_unit_set_grown_from_edge_index([0, 1]),
            'bottom': self.boundary_unit_set_grown_from_edge_index([1, 0]),
            'top': self.boundary_unit_set_grown_from_edge_index([1, -1]),
            'right': self.boundary_unit_set_grown_from_edge_index([-1, 1])}

class Laplace_Equation(pygdh.Model):

    def __init__(self):

        pass

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # Indicate that equation is to be solved on all volumes, then overwrite
        # for volumes on boundaries.  This is inefficient, but only done once,
        # so the cost is negligible.
        for vol in self.grid[0].unit_with_number:
            self.unknown[0][0,vol.number] = True

        ## Solution values are prescribed on all boundaries

        for vol in self.grid[0].unit_set_with_name['left']:
            self.unknown[0][0,vol.number] = False

        for vol in self.grid[0].unit_set_with_name['bottom']:
            self.unknown[0][0,vol.number] = False

        for vol in self.grid[0].unit_set_with_name['top']:
            self.unknown[0][0,vol.number] = False

        for vol in self.grid[0].unit_set_with_name['right']:
            self.unknown[0][0,vol.number] = False

    def pde(self, vol, residual):

        # Defined for clarity
        T = self.grid[0].field[0][0]

        residual[0] = ((vol.dx_right(T) - vol.dx_left(T))*vol.length[1]
                       + (vol.dy_up(T) - vol.dy_down(T))*vol.length[0])

    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.pde: 1}

    def assign_equations(self):

        domain_equations = self.equations[0]

        # Indicate that equation is to be solved on all volumes, then overwrite
        # for volumes on boundaries.  This is inefficient, but only done once,
        # so the cost is negligible.
        for vol in self.grid[0].unit_with_number:
            domain_equations[vol.number] = [self.pde]

        ## Solution values are prescribed on all boundaries

        for vol in self.grid[0].unit_set_with_name['left']:
            domain_equations[vol.number] = []

        for vol in self.grid[0].unit_set_with_name['bottom']:
            domain_equations[vol.number] = []

        for vol in self.grid[0].unit_set_with_name['top']:
            domain_equations[vol.number] = []

        for vol in self.grid[0].unit_set_with_name['right']:
            domain_equations[vol.number] = []

    def set_boundary_values(self):
        
        # Defined for clarity
        T = self.grid[0].field[0][0]

        ## Prescribed value of zero on all but the 'right' boundary

        for vol in self.grid[0].unit_set_with_name['left']:
            T[vol.number] = 0

        for vol in self.grid[0].unit_set_with_name['bottom']:
            T[vol.number] = 0

        for vol in self.grid[0].unit_set_with_name['top']:
            T[vol.number] = 0

        # This boundary condition is consistent with the other conditions at the
        # corners, as it should be.
        for vol in self.grid[0].unit_set_with_name['right']:
            T[vol.number] = math.sin(math.pi*vol.coordinate[1])

    def process_solution(self):

        tol_squared = 1e-4
        
        for vol_number in range(self.grid[0].unit_count):

            vol = self.grid[0].unit_with_number[vol_number]
            assert (math.sinh(math.pi*vol.coordinate[0])*math.sin(math.pi*vol.coordinate[1]) / math.sinh(math.pi) - self.grid[0].field[0][0,vol_number])**2 < tol_squared

unit_count = 10
grid_obj = Domain('domain',unit_count)

model = Laplace_Equation()

# The names of all output files will begin with this string
filename_root = 'two_dimensions'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model])

# Calculate solution and write output to file
simulation.solve_time_independent_system(filename_root, output_types)
