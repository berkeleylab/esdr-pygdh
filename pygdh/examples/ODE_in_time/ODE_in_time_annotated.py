import pygdh
import numpy
import math

# PyGDH needs 'Grid' objects for storing solution results, even though this
# model is not position-dependent
class FalseDomain(pygdh.Grid):

#### SPHINX SNIPPET START GRID INIT
    def __init__(self,name):

        # This 'Grid' object only contains a single 'Volume' object
        self.initialize_grid(name, field_names=['y'],
                             # As there is no spatial dependence, it is not
                             # necessary to declare independent spatial
                             # variables
                             unit_classes=[pygdh.Volume],
                             stored_solution_count=2)
#### SPHINX SNIPPET END GRID INIT

class ODE_in_time(pygdh.Model):

#### SPHINX SNIPPET START MODEL INIT   
    def __init__(self, alpha):

        # Save the argument value as an object member
        self.alpha = alpha
#### SPHINX SNIPPET END MODEL INIT   

#### SPHINX SNIPPET START IC
    def set_initial_conditions(self):

        # Set the value of the single dependent variable directly
        self.grid[0].field[0][0,0] = 1.0
#### SPHINX SNIPPET END IC

#### SPHINX SNIPPET START ODE
    def ode_in_time(self, vol, residual):

        # There is only one solution field, with index 0, and with one
        # ``Volume``, with label 0        
        y = self.grid[0].field[0][0,0]
        y_1 = self.grid[0].field[-1][0,0]

        residual[0] = (y - y_1)*self.inverse_timestep_size + self.alpha*y
#### SPHINX SNIPPET END ODE

    def set_equation_scalar_counts(self):

        self.equation_scalar_counts = {self.ode_in_time: 1}
        
    def assign_equations(self):

        self.equations[0][0] = [self.ode_in_time]

    def process_solution(self):

        tol_squared = 1e-5
        
        for vol_number in range(self.grid[0].unit_count):

            vol = self.grid[0].unit_with_number[vol_number]
            assert (math.exp(-self.alpha*self.time[0]) - self.grid[0].field[0][0,vol_number])**2 < tol_squared
        
#### SPHINX SNIPPET START MAIN

grid_obj = FalseDomain('false_domain')

# Create an object instance, setting alpha = 1.0
model = ODE_in_time(1.0)

number_of_timesteps = 100
timestep_size = 0.01

# The names of all output files will begin with this string
filename_root = 'ODE_in_time'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model],stored_solution_count=2)

# Calculate solution and write output to file
simulation.solve_time_dependent_system(filename_root, output_types,
                                    timestep_count=number_of_timesteps,
                                    timestep_size=timestep_size)
#### SPHINX SNIPPET END MAIN
