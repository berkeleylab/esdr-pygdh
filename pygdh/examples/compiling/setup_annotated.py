import numpy
from distutils.core import setup
from Cython.Build import cythonize
import sys
setup(
    include_dirs=[numpy.get_include()],
    ext_modules = cythonize("system_body.pyx",include_path=sys.path)
)
