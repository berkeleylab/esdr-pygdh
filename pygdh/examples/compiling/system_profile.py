import cProfile
import system_body
import sys
import pygdh

assert len(sys.argv) == 2

# Create an object instance
volume_count = 81
grid_obj = system_body.Domain('domain',volume_count)

N = 1.
D_1 = 2.
D_2 = 1.
k = 1.

model = system_body.System(N, D_1, D_2, k)

# The names of all output files will begin with this string
filename_root = 'system'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

timestep_size = 2.5e-1
timesteps = 20

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model],stored_solution_count=2)

# Calculate solution and write output to file
cProfile.run('simulation.solve_time_dependent_system(filename_root, output_types, timestep_count=timesteps, timestep_size=timestep_size)',sys.argv[1])
