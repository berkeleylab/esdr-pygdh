import pygdh
import numpy

# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        # Operator for estimating function value at the volume center
        self.d2x_center = self.default_interpolant(0.0, 2, 2)
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

# Objects of this class describe the spatial domains on which models will be
# solved.
class Domain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, unit_count):

        self.initialize_grid(name,
                             coordinate_values = {
                                 'x' :  numpy.linspace(0.0, 1.0, unit_count)},
                             coordinate_order=['x'],
                             field_names=['c1','c2'],
                             # Each unit in 'Domain' will be described by a
                             # 'Volume' object                             
                             unit_classes=[Volume for i in range(unit_count)],
                             stored_solution_count=2)

    # Variables that are used by multiple methods and/or additional output
    # variables should be defined here. This routine is called once before
    # any numerical solutions are generated.
    def define_field_variables(self):

        # These are initialized to zero because ``process_solution`` is not
        # called before the first solution is computed, but values will be sent
        # to the output file.
        self.c1_fdm_residual = numpy.zeros(self.unit_count)
        self.c2_fdm_residual = numpy.zeros(self.unit_count)
        self.d2c1__dx2 = numpy.empty(self.unit_count)
        self.d2c2__dx2 = numpy.empty(self.unit_count)

    # This is an optional method for specifying additional output variables
    def set_output_fields(self):

        self.output_fields = {'c1_fdm_residual': self.c1_fdm_residual,
                              'c2_fdm_residual': self.c2_fdm_residual,
                              'd2c1__dx2': self.d2c1__dx2,
                              'd2c2__dx2': self.d2c2__dx2}

class System(pygdh.Model):

    def __init__(self, N, D_1, D_2, k):

        self.N = N
        self.D_1 = D_1
        self.D_2 = D_2
        self.k = k

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # "Left" boundary
        vol = self.grid[0].unit_with_number[0]
        self.unknown[0][0,vol.number] = True
        self.unknown[0][1,vol.number] = True

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            vol = self.grid[0].unit_with_number[vol_number]
            self.unknown[0][0,vol.number] = True
            self.unknown[0][1,vol.number] = True

        # "Right" boundary, value to be copied from "Left" boundary of Right
        # grid
        vol = self.grid[0].unit_with_number[-1]
        self.unknown[0][0,vol.number] = True
        self.unknown[0][1,vol.number] = True

    # This specifies the initial conditions for this time-dependent
    # model
    def set_initial_conditions(self):
        c1 = self.grid[0].field[0][0]
        c2 = self.grid[0].field[0][1]
        for vol in self.grid[0].unit_with_number:
            x = vol.coordinate
            c1[vol.number] = (
                1.0 + self.N*(self.D_1 - self.D_2)/(6.*self.D_1*self.D_2)
                + 0.5*self.N*x**2/self.D_1)
            c2[vol.number] = 1.0 + 0.5*self.N*x**2/self.D_2

    # This describes a "vector-valued" equation, storing two scalar values
    def pdes(self, vol, residuals):

        c1 = self.grid[0].field[0][0]
        c2 = self.grid[0].field[0][1]

        c1_1 = self.grid[0].field[-1][0]
        c2_1 = self.grid[0].field[-1][1]

        residuals[0] = ((c1[vol.number] - c1_1[vol.number]) * vol.volume
                        * self.inverse_timestep_size
                        - self.D_1*(vol.dx_right(c1) - vol.dx_left(c1))
                        + self.k*vol.interpolate(c1)
                        * vol.interpolate(c2)*vol.volume)
        residuals[1] = ((c2[vol.number] - c2_1[vol.number]) * vol.volume
                        * self.inverse_timestep_size
                        - self.D_2*(vol.dx_right(c2) - vol.dx_left(c2))
                        + self.k*vol.interpolate(c1)
                        * vol.interpolate(c2)*vol.volume)

    ## These represent scalar-valued equations and incorporate flux
    ## conditions at one of the domain boundaries
        
    def c1_inlet_pde(self, vol, residuals):

        c1 = self.grid[0].field[0][0]
        c2 = self.grid[0].field[0][1]

        c1_1 = self.grid[0].field[-1][0]

        residuals[0] = ((c1[vol.number] - c1_1[vol.number]) * vol.volume
                        * self.inverse_timestep_size
                        - self.N + self.D_1*vol.dx_left(c1)
                        + self.k*vol.interpolate(c1)
                        * vol.interpolate(c2)*vol.volume)

    def c2_inlet_pde(self, vol, residuals):

        c1 = self.grid[0].field[0][0]
        c2 = self.grid[0].field[0][1]

        c2_1 = self.grid[0].field[-1][1]

        residuals[0] = ((c2[vol.number] - c2_1[vol.number]) * vol.volume
                        * self.inverse_timestep_size
                        - self.N + self.D_2*vol.dx_left(c2)
                        + self.k*vol.interpolate(c1)
                        * vol.interpolate(c2)*vol.volume)

    # This represents another "vector-valued" equation, storing two scalars
    def no_flux_pdes(self, vol, residuals):

        c1 = self.grid[0].field[0][0]
        c2 = self.grid[0].field[0][1]

        c1_1 = self.grid[0].field[-1][0]
        c2_1 = self.grid[0].field[-1][1]

        residuals[0] = ((c1[vol.number] - c1_1[vol.number]) * vol.volume
                        * self.inverse_timestep_size
                        - self.D_1*vol.dx_right(c1)
                        + self.k*vol.interpolate(c1)
                        * vol.interpolate(c2)*vol.volume)
        residuals[1] = ((c2[vol.number] - c2_1[vol.number]) * vol.volume
                        * self.inverse_timestep_size
                        - self.D_2*vol.dx_right(c2)
                        + self.k*vol.interpolate(c1)
                        * vol.interpolate(c2)*vol.volume)

    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.no_flux_pdes: 2,
                                       self.pdes: 2,
                                       self.c1_inlet_pde: 1,
                                       self.c2_inlet_pde: 1}

    def assign_equations(self):

        domain_equations = self.equations[0]
        # "Left" boundary
        domain_equations[0] = [self.no_flux_pdes]

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            domain_equations[vol_number] = [self.pdes]

        # "Right" boundary
        domain_equations[-1] = [self.c1_inlet_pde, self.c2_inlet_pde]

    # This routine is called once after a solution is obtained
    # (on each timestep in a time-dependent model)    
    def process_solution(self):

        grid0 = self.grid[0]

        c1 = grid0.field[0][0]
        c2 = grid0.field[0][1]
        c1_1 = grid0.field[-1][0]
        c2_1 = grid0.field[-1][1]

        for vol_number in range(grid0.unit_count):
            vol = grid0.unit_with_number[vol_number]

            grid0.d2c1__dx2[vol_number] = vol.d2x_center(c1)
            grid0.d2c2__dx2[vol_number] = vol.d2x_center(c2)

            grid0.c1_fdm_residual[vol_number] = (
                (c1[vol_number] - c1_1[vol_number])
                * self.inverse_timestep_size
                - self.D_1*grid0.d2c1__dx2[vol_number]
                + self.k*c1[vol_number] * c2[vol_number])

            grid0.c2_fdm_residual[vol_number] = (
                (c2[vol_number] - c2_1[vol_number])
                * self.inverse_timestep_size
                - self.D_2*grid0.d2c2__dx2[vol_number]
                + self.k*c1[vol_number] * c2[vol_number])
