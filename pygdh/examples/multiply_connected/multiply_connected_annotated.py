import numpy
import pygdh
import math

# Objects of this class are the units from which the spatial domain will be
# constructed.
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):
        
        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant((-1.0,0),(1,0),(1,1))
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant((1.0,0),(1,0),(1,1))
        # Operator for estimating first derivative at bottom boundary of volume
        self.dy_down = self.default_interpolant((0,-1.0),(0,1),(1,1))
        # Operator for estimating first derivative at top boundary of volume
        self.dy_up = self.default_interpolant((0,1.0),(0,1),(1,1))
        # Operators for estimating second derivatives at center of volume
        self.d2x = self.default_interpolant((0,0),(2,0),(2,2))
        self.d2y = self.default_interpolant((0,0),(0,2),(2,2))

# Objects of this class describe the spatial domains on which models will be
# solved.
class Domain(pygdh.Grid):

#### SPHINX SNIPPET START GRID INIT
    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self, name, unit_count):

        # Each unit in 'Domain' will be described by a 'Volume' object
        unit_layout = [ [ Volume for j in range(unit_count) ]
                        for i in range(unit_count) ]

        # Put a hole into the domain
        for i in range(3,6):
            for j in range(3,6):
                unit_layout[i][j] = None

        self.initialize_grid(name,
                             coordinate_values = {
                                 'x': numpy.linspace(0., 1., unit_count),
                                 'y': numpy.linspace(0., 1., unit_count)},
                             coordinate_order=['x','y'],
                             field_names=['T'],
                             unit_classes=unit_layout)
#### SPHINX SNIPPET END GRID INIT

#### SPHINX SNIPPET START DEFINE UNIT SETS
    def define_unit_set_with_name(self):

        # Descriptive names for boundaries of rectangular domain
        self.unit_set_with_name = {
            'left': self.boundary_unit_set_grown_from_edge_index((0, 1)),
            'bottom': self.boundary_unit_set_grown_from_edge_index((1, 0)),
            'top': self.boundary_unit_set_grown_from_edge_index((1, -1)),
            'right': self.boundary_unit_set_grown_from_edge_index((-1, 1)),
            'inner_left': self.boundary_unit_set_grown_from_edge_index((2,3)),
            'inner_bottom': self.boundary_unit_set_grown_from_edge_index((3,2)),
            'inner_top': self.boundary_unit_set_grown_from_edge_index((3,6)),
            'inner_right': self.boundary_unit_set_grown_from_edge_index((6,3))}

        boundaries = set()
        for unit_set in self.unit_set_with_name.values():
            boundaries = boundaries.union(unit_set)
        self.unit_set_with_name['boundaries'] = boundaries
        self.unit_set_with_name['all'] = set(self.unit_with_number)
        self.unit_set_with_name['interior'] = self.unit_set_with_name['all'].difference(self.unit_set_with_name['boundaries'])
#### SPHINX SNIPPET END DEFINE UNIT SETS

    def define_field_variables(self):
        self.validation = numpy.empty(self.unit_count)

    def set_output_fields(self):
        self.output_fields = { 'validation': self.validation }

class Laplace_Equation(pygdh.Model):

    def __init__(self):

        pass

#### SPHINX SNIPPET START DEFINE PDE            
    def pde(self, vol, residual):

        # Defined for clarity
        T = self.grid[0].field[0][0]

        # Discretized Laplace equation in two dimensions
        residual[0] = ((vol.dx_right(T)
                        - vol.dx_left(T))*vol.length[1]
                       + (vol.dy_up(T)
                          - vol.dy_down(T))*vol.length[0])
#### SPHINX SNIPPET END DEFINE PDE            

    def set_equation_scalar_counts(self):

        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.pde: 1}
        
#### SPHINX SNIPPET START ASSIGN UNKNOWNS AND EQUATIONS
    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # Indicate that equation is to be solved on all volumes, then overwrite
        # for volumes on boundaries.  This is inefficient, but only done once,
        # so the cost is negligible.
        for vol in self.grid[0].unit_with_number:
            self.unknown[0][0,vol.number] = True

        ## Solution values are prescribed on all boundaries

        for vol in self.grid[0].unit_set_with_name['boundaries']:
            self.unknown[0][0,vol.number] = False
        for vol in self.grid[0].unit_set_with_name['interior']:
            self.unknown[0][0,vol.number] = True

    def assign_equations(self):

        domain_equations = self.equations[0]
        
        # Solve the PDE for all interior ``Volume`` objects
        for vol in self.grid[0].unit_set_with_name['interior']:
            domain_equations[vol.number] = [self.pde]

        # Solution values are prescribed on all boundaries
        for vol in self.grid[0].unit_set_with_name['boundaries']:
            domain_equations[vol.number] = []
#### SPHINX SNIPPET END ASSIGN UNKNOWNS AND EQUATIONS

#### SPHINX SNIPPET START BC            
    def set_boundary_values(self):

        # Defined for clarity
        T = self.grid[0].field[0][0]

        ## Prescribed value of zero on all outside boundaries except for the
        ## 'right' boundary

        for vol in self.grid[0].unit_set_with_name['left']:
            T[vol.number] = 0

        for vol in self.grid[0].unit_set_with_name['bottom']:
            T[vol.number] = 0

        for vol in self.grid[0].unit_set_with_name['top']:
            T[vol.number] = 0

        # This boundary condition is consistent with the other conditions at the
        # corners, as it should be.
        for vol in self.grid[0].unit_set_with_name['right']:
            T[vol.number] = math.sin(math.pi*vol.coordinate[1])

        ## Prescribed value of zero on all inner boundaries

        for vol in self.grid[0].unit_set_with_name['inner_left']:
            T[vol.number] = 0.5

        for vol in self.grid[0].unit_set_with_name['inner_bottom']:
            T[vol.number] = 0.5

        for vol in self.grid[0].unit_set_with_name['inner_top']:
            T[vol.number] = 0.5

        for vol in self.grid[0].unit_set_with_name['inner_right']:
            T[vol.number] = 0.5
#### SPHINX SNIPPET END BC            

    def process_solution(self):
        T = self.grid[0].field[0][0]
        for vol in self.grid[0].unit_with_number:
            if vol in self.grid[0].unit_set_with_name['interior']:
                self.grid[0].validation[vol.number] = vol.d2x(T) + vol.d2y(T)
            else:
                self.grid[0].validation[vol.number] = 0

        tol = 1e-26
        for vol in self.grid[0].unit_with_number:
            assert self.grid[0].validation[vol.number]**2 < tol
                
#### SPHINX SNIPPET START MAIN
unit_count = 10
grid_obj = Domain('domain',unit_count)

model = Laplace_Equation()

# The names of all output files will begin with this string
filename_root = 'multiply_connected'

# One output file will be created for each format given here
output_types = ['GNUPLOT']

simulation = pygdh.Simulation()
simulation.initialize_simulation([grid_obj],[model])

# Calculate solution and write output to file
simulation.solve_time_independent_system(filename_root, output_types)
#### SPHINX SNIPPET END MAIN
