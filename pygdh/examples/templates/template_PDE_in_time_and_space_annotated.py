import pygdh

# User-defined (and named) class defining objects that are the elements from
# which the spatial domain will be constructed
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

# User-defined (and named) class defining the spatial domain on which the
# mathematical problem is to be solved
class Domain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self):

        self.initialize_grid(name,
                             field_names=[<DESCRIPTIVE NAMES OF DEPENDENT
                                          VARIABLES AS STRINGS>],
                             coordinate_values={
                                 <DESCRIPTIVE NAME OF INDEPENDENT VARIABLE AS
                                 STRING>: <VALUES OF INDEPENDENT VARIABLE AT
                                 GRID LINES AS SEQUENCE OF FLOATING-POINT
                                 NUMBERS>},
                             unit_classes=[<LIST OF CLASS NAMES DESCRIBING
                                           ELEMENTS OF SPATIAL DOMAIN, ONE
                                           PER ELEMENT>],
                             stored_solution_count=<NUMBER OF STORED SOLUTIONS
                             FOR TIMESTEPPING> )

# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent problems, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class MathematicalProblem(pygdh.Problem):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_problem' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self):

        # Create the pygdh.Grid-derived object representing the problem domain
        grid_obj = Domain()

        # This pygdh.Problem method must be called at the end of the present
        # method, and specifies the spatial domains as well as the number of
        # past solutions to retain for the timestepping scheme.
        self.initialize_problem([grid_obj],stored_solution_count=<NUMBER OF
                                STORED SOLUTIONS FOR TIMESTEPPING>)

    # This sets the initial values of the independent variables on the region
    # represented by an object of this class.
    def set_initial_conditions(self):

        # The vector of present values of the single dependent variable is
        # stored in self.grid[0].field[0][0]

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # "Left" boundary
        vol = self.grid[0].unit_with_number[0]
        self.unknown[0][0,vol.number] = <"True" IF UNKNOWN, "False" OTHERWISE>

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            vol = self.grid[0].unit_with_number[vol_number]
            self.unknown[0][0,vol.number] = <"True" IF UNKNOWN, "False" OTHERWISE>

        # "Right" boundary
        vol = self.grid[0].unit_with_number[-1]
        self.unknown[0][0,vol.number] = <"True" IF UNKNOWN, "False" OTHERWISE>

    # These methods represent the governing equation and are named and defined
    # by the user. These must accept three particular parameters and must store
    # results in the data structure associated with the last parameter.

    def governing_equation_method(self, vol, residual):

        # Solution value at previous timestep on present element is
        # self.grid[0].field[-1][0,vol.number]
        # Solution value at present timestep on present element is
        # self.grid[0].field[0][0,vol.number]
        
        residual[0] = <DISCRETIZED EQUATION RESIDUAL>

    # This method is used to define boundary conditions in which the solution
    # value is specified. This must be consistent with the contents of the
    # 'declare_unknowns' methods defined in the corresponding 'Grid' objects.
    def set_boundary_values(self):

        # Defined for clarity; there is only one 'Grid' object, with index 0,
        # and one solution field 'y', with index 0
        y = self.grid[0].field[0][0]

        # "Left" boundary
        y[0] = 0.0

    # This is a required method that notifies PyGDH about the number of scalar
    # values returned by each governing equation method.
    def set_equation_scalar_counts(self):
        
        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.governing_equation_method: 1}
        
    # This is a required method that notifies PyGDH about the methods that
    # describe governing equations, and indicates where in the domain the
    # equations apply. This must be consistent with the 'declare_unknowns'
    # methods of the corresponding 'Grid' objects.
    def assign_equations(self):

        # Defined for clarity, equations to be defined on the only Grid,
        # with index 0
        domain_equations = self.equations[0]

        # "Left" boundary
        domain_equations[0] = []

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            domain_equations[vol_number] = [self.governing_equation_method]

        # "Right" boundary
        domain_equations[-1] = []

# Create an object instance
problem = MathematicalProblem()

number_of_timesteps = <NUMBER OF TIMESTEPS AS INTEGER>
timestep_size = <TIMESTEP SIZE AS FLOATING-POINT NUMBER>

total_time = number_of_timesteps*timestep_size

# The names of all output files will begin with this string
filename_root = <ROOT OF OUTPUT FILE NAMES AS STRING>

# One output file will be created for each format given here
# Options are 'GNUPLOT', 'CSV', 'HDF5', and 'PICKLE'
output_types = <LIST OF OUTPUT FORMATS AS STRINGS>

# Calculate solution and write output to file
problem.solve_time_dependent_system(filename_root, output_types,
                                    maximum_time_interval=total_time,
                                    timestep_size=timestep_size)

