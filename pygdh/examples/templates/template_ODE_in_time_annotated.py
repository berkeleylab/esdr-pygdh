import pygdh

# User-defined (and named) pygdh.Grid-derived class describing spatial domain.
# In the present problem, the solution is not position-dependent, but this
# is needed for storing solution results.
class FalseDomain(pygdh.Grid):

    # User-defined method for initializing the spatial domain
    def __init__(self):

        # The pygdh.Grid.initialize_grid method MUST be called at the end of
        # the present method.
        
        self.initialize_grid(<DESCRIPTIVE NAME OF GRID OBJECT AS STRING>,
                             field_names=[
                                 <NAME OF INDEPENDENT VARIABLE AS A STRING>],
                             # This 'Grid' object only contains a single
                             # 'Volume' object
                             #
                             # As there is no spatial dependence, it is not
                             # necessary to declare independent spatial
                             # variables
                             unit_classes=[pygdh.Volume],
                             stored_solution_count=<NUMBER OF STORED SOLUTIONS
                             FOR TIMESTEPPING SCHEME>)

# User-defined pygdh.Problem-derived class defining most of the mathematical
# details
class MathematicalProblem(pygdh.Problem):

    # User-defined method for initializing objects of this user-defined
    # pygdh.Problem-derived class
    def __init__(self):

        # Create an instance of the user-defined pygdh.Grid-derived class
        # describing the spatial domain
        grid_obj = FalseDomain()

        # This pygdh.Problem method must be called at the end of the present
        # method.
        self.initialize_problem([grid_obj],stored_solution_count=
                                <NUMBER OF STORED SOLUTIONS FOR
                                TIMESTEPPING SCHEME>)

    # Method for setting the initial value of the independent variable
    def set_initial_conditions(self):

        # The present value of the single dependent variable is stored in
        # self.grid[0].field[0][0,0]
        
        self.grid[0].field[0][0,0] = <INITIAL VALUE>

    # User-defined method that computes residuals of the equations to be
    # solved. The name may be changed to any legal Python name.
    def governing_equation_method(self, vol, residual):

        # Solution value at previous timestep is self.grid[0].field[-1][0,0]
        # Solution value at present timestep is self.grid[0].field[0][0,0]

        residual[0] = <DISCRETIZED EQUATION RESIDUAL>

    # Required method in which the user must define the equation_scalar_counts
    # member of the present (self) object as a Python dictionary associating
    # governing equation methods with the number of scalar equations
    # represented by the methods
    def set_equation_scalar_counts(self):

        self.equation_scalar_counts = {self.governing_equation_method: 1}

    # Required method in which the user must provide a list of methods which
    # govern solution behavior on each pygdh.Volume-derived element in the
    # pygdh.Grid-derived spatial domain
    def assign_equations(self):

        # Element 'n' on grid 'm' is associated with self.equations[m][n]
        self.equations[0][0] = [self.governing_equation_method]

# Create an pygdh.Problem-derived object instance
problem = MathematicalProblem()

number_of_timesteps = <NUMBER OF TIMESTEPS AS INTEGER>
timestep_size = <TIMESTEP SIZE AS FLOATING-POINT NUMBER>

total_time = number_of_timesteps*timestep_size

# The names of all output files will begin with this string
filename_root = <ROOT OF OUTPUT FILE NAMES AS STRING>

# One output file will be created for each format given here
# Options are 'GNUPLOT', 'CSV', 'HDF5', and 'PICKLE'
output_types = <LIST OF OUTPUT FORMATS AS STRINGS>

# Calculate solution and write output to file
problem.solve_time_dependent_system(filename_root, output_types,
                                    maximum_time_interval=total_time,
                                    timestep_size=timestep_size)
