import pygdh

# User-defined (and named) class defining objects that are the elements from
# which the spatial domain will be constructed
class Volume(pygdh.Volume):

    # Defining this method gives users an opportunity to define their own
    # mathematical operators, for later use in describing governing equations.
    def define_interpolants(self):

        # Operator for estimating first derivative at left boundary of volume
        self.dx_left = self.default_interpolant(-1.0, 1, 1)
        
        # Operator for estimating function value at the volume center
        self.interpolate = self.default_interpolant(0.0, 0, 2)
        
        # Operator for estimating first derivative at right boundary of volume
        self.dx_right = self.default_interpolant(1.0, 1, 1)

# User-defined (and named) class defining the spatial domain on which the
# mathematical problem is to be solved
class Domain(pygdh.Grid):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_grid' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self):
        
        self.initialize_grid(name,
                             field_names=[<DESCRIPTIVE NAMES OF DEPENDENT
                                          VARIABLES AS STRINGS>],
                             coordinate_values={
                                 <DESCRIPTIVE NAME OF INDEPENDENT VARIABLE AS
                                 STRING>: <VALUES OF INDEPENDENT VARIABLE AT
                                 GRID LINES AS SEQUENCE OF FLOATING-POINT
                                 NUMBERS>},
                             unit_classes=[<LIST OF CLASS NAMES DESCRIBING
                                           ELEMENTS OF SPATIAL DOMAIN, ONE
                                           PER ELEMENT>] )
        
# Objects of this class bring the spatial domain definitions together with
# equations, boundary conditions, and for time-dependent problems, initial
# conditions. These objects can then be directed to compute numerical
# solutions.
class MathematicalProblem(pygdh.Problem):

    # This method is automatically called immediately after an object of this
    # class is created. Users are free to modify this method definition as
    # desired, as long as the 'self.initialize_problem' method is called with
    # appropriate parameter values at some point in this method.
    def __init__(self):

        # Create an instance of the user-defined pygdh.Grid-derived class
        # describing the spatial domain
        grid_obj = Domain()

        # This pygdh.Problem method must be called at the end of the present
        # method.        
        self.initialize_problem([grid_obj])

    # This method is required. It informs PyGDH of the unknown quantities for
    # for which it must solve.
    def declare_unknowns(self):

        # "Left" boundary
        vol = self.grid[0].unit_with_number[0]
        self.unknown[0][0,vol.number] = <"True" IF UNKNOWN, "False" OTHERWISE>

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            vol = self.grid[0].unit_with_number[vol_number]
            self.unknown[0][0,vol.number] = <"True" IF UNKNOWN, "False" OTHERWISE>

        # "Right" boundary
        vol = self.grid[0].unit_with_number[-1]
        self.unknown[0][0,vol.number] = <"True" IF UNKNOWN, "False" OTHERWISE>

    # This method represents a governing equation and is named and defined by
    # the user. It must accept three particular parameters and must store
    # results in the data structure associated with the last parameter.
    def governing_equation_method(self, vol, residual):

        # Solution value at previous timestep on present element is
        # self.grid[0].field[-1][0,vol.number]
        # Solution value at present timestep on present element is
        # self.grid[0].field[0][0,vol.number]

        residual[0] = <DISCRETIZED EQUATION RESIDUAL>
        
    # This method is used to define boundary conditions in which the solution
    # value is specified. This must be consistent with the contents of the
    # 'declare_unknowns' methods defined in the corresponding 'Grid' objects.
    def set_boundary_values(self):

        # The vector of solution values is represented by self.field[0][0]

        self.field[0][0,0] = <LEFT BOUNDARY CONDITION>

        # "Right" domain boundary
        self.field[0][0,-1] = <RIGHT BOUNDARY CONDITION>

    # This is a required method that notifies PyGDH about the number of scalar
    # values returned by each governing equation method.
    def set_equation_scalar_counts(self):
        
        # This is a dictionary associating governing equation methods with
        # the number of scalar values that they return
        self.equation_scalar_counts = {self.governing_equation_method: 1}
        
    # This is a required method that notifies PyGDH about the methods that
    # describe governing equations, and indicates where in the domain the
    # equations apply. This must be consistent with the 'declare_unknowns'
    # methods of the corresponding user-defined pygdh.Grid-derived objects.
    def assign_equations(self):

        # Defined for clarity, equations to be defined on the only Grid,
        # with index 0
        domain_equations = self.equations[0]

        # "Left" domain boundary
        domain_equations[0] = []

        # Interior volumes
        for vol_number in range(1, self.grid[0].unit_count-1):
            domain_equations[vol_number] = [self.governing_equation_method]

        # "Right" domain boundary
        domain_equations[-1] = []

# Create an object instance
problem = MathematicalProblem()

# The names of all output files will begin with this string
filename_root = <ROOT OF OUTPUT FILE NAMES AS STRING>

# One output file will be created for each format given here
# Options are 'GNUPLOT', 'CSV', 'HDF5', and 'PICKLE'
output_types = <LIST OF OUTPUT FORMATS AS STRINGS>

# Calculate solution and write output to file
problem.solve_time_independent_system(filename_root, output_types)

