cimport numpy
cdef class Interpolant:
    cdef numpy.ndarray coefficient_array
    cdef list data_unit_numbers
    cdef int data_unit_count
    cpdef double value(self, data)
