cdef class Unit:
    """Defines objects that describe the generic individual units making up the
    problem domain"""

    def __init__(self,grid,number,index,coordinate):
        """Parameters:
        grid: 'Grid' object to which the new 'Unit' object will belong

        number: unique integer label identifying the new 'Unit' object within
          the containing 'Grid' object

        index: integer coordinate(s) of the grid point indicating the position
          of the new 'Unit' object in the rectangular grid defined in the
          containing 'Grid' object

        coordinate: coordinate(s) of the grid point indicating the position of
          the new 'Unit' object in the coordinate space defined by the
          containing 'Grid' object"""

        self.grid = grid
        self.number = number
        self.index = index
        self.coordinate = coordinate

        # The user must modify these later for volumes subject to boundary conditions, etc.
        self.unknown_field = [ True for field in range(self.grid.field_count) ]

        ## These are placeholders for quantities that are determined elsewhere

        # This is determined in Grid.count_neighbors
        self.neighbor_count = 0

        # These are determined in Grid.determine_interior_neighbors
        self.interior_neighbor_local_index = None

    ## These are placeholders for routines that the user may supply
            
    cpdef define_variables(self):
        """This method may be overridden in order to define additional
        quantities associated with 'Unit' objects"""
        pass

    cpdef define_interpolants(self):
        """This method may be overridden in order to define operators that
        act on sequences of variable values, with one element for each 'Unit'
        object in the containing 'Grid' object"""
        pass
