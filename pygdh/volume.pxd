from . cimport unit
cimport numpy
from . cimport interpolant
cdef class Volume(unit.Unit):
    cdef public double volume
    cdef public length, default_interpolant, default_1D_interpolant, default_2D_interpolant, grid_coordinates_list, interior_direction_relative_index, interior_neighbor
    cdef public numpy.ndarray distance_to_boundary
