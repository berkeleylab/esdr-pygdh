import unittest
import sys
import subprocess
import os
import glob

def custom_execfile(filename,custom_globals=None):

    if custom_globals == None:
        custom_globals = globals()
    
    if sys.version_info.major == 2:

        execfile(filename,custom_globals)

    else:

        with open(filename,'r') as example_file:
            source = compile(example_file.read(),filename,'exec')
            exec(source,custom_globals)
    
class TestExamples(unittest.TestCase):

    def tearDown(self):

        sys.path.pop()
        os.chdir('../..')
        
    def test_ODE(self):

        os.chdir('examples/ODE')
        sys.path.append(os.getcwd())
        custom_execfile('ODE.py')
        try:
            subprocess.call(['gnuplot','ODE.gpl'])
        except:
            print('Error running GNUPLOT')

    def test_PDE(self):

        os.chdir('examples/PDE')
        sys.path.append(os.getcwd())
        custom_execfile('PDE.py')
        try:
            subprocess.call(['gnuplot','PDE.gpl'])        
        except:
            print('Error running GNUPLOT')

    def test_ODE_in_time(self):

        os.chdir('examples/ODE_in_time')
        sys.path.append(os.getcwd())
        custom_execfile('ODE_in_time.py')
        try:
            subprocess.call(['gnuplot','ODE_in_time.gpl'])
        except:
            print('Error running GNUPLOT')
        
    def test_output(self):

        os.chdir('examples/output')
        sys.path.append(os.getcwd())
        custom_execfile('output.py')
        try:
            subprocess.call(['gnuplot','output.gpl'])
        except:
            print('Error running GNUPLOT')
        
    def test_nonlinear(self):

        os.chdir('examples/nonlinear')
        sys.path.append(os.getcwd())
        custom_execfile('nonlinear.py')
        try:
            subprocess.call(['gnuplot','nonlinear.gpl'])
        except:
            print('Error running GNUPLOT')
        
    def test_PDE_solve_postprocess_restart(self):

        os.chdir('examples/postprocessing')
        sys.path.append(os.getcwd())
        if os.access('PDE_postprocess.h5',os.F_OK):
            os.remove('PDE_postprocess.h5')
        
        custom_execfile('PDE_solve.py')
        custom_execfile('PDE_postprocess.py')
        custom_execfile('PDE_restart.py')
        try:
            subprocess.call(['gnuplot','PDE_restart.gpl'])
        except:
            print('Error running GNUPLOT')

    def test_PDE_solve_pickle_restart(self):

        os.chdir('examples/postprocessing')
        sys.path.append(os.getcwd())
        if os.access('PDE_pickle_postprocess.pkl',os.F_OK):
            os.remove('PDE_pickle_postprocess.pkl')
            
        custom_execfile('PDE_pickle_solve.py')
        custom_execfile('PDE_pickle_restart.py')
        try:
            subprocess.call(['gnuplot','PDE_pickle_restart.gpl'])        
        except:
            print('Error running GNUPLOT')

    def test_system(self):

        os.chdir('examples/system')
        sys.path.append(os.getcwd())
        custom_execfile('system.py')
        try:
            subprocess.call(['gnuplot','system.gpl'])
        except:
            print('Error running GNUPLOT')
        
    def test_multiple_grids(self):

        os.chdir('examples/multiple_grids')
        sys.path.append(os.getcwd())
        custom_execfile('multiple_grids.py')
        try:
            subprocess.call(['gnuplot','multiple_grids.gpl'])
        except:
            print('Error running GNUPLOT')
        
    def test_coordinated(self):

        os.chdir('examples/coordination')
        sys.path.append(os.getcwd())
        custom_execfile('coordinated.py')
        try:
            subprocess.call(['gnuplot','coordinated.gpl'])        
        except:
            print('Error running GNUPLOT')

    def test_submodel(self):

        os.chdir('examples/submodel')
        sys.path.append(os.getcwd())
        custom_execfile('submodel.py')
        try:
            subprocess.call(['gnuplot','submodel.gpl'])        
        except:
            print('Error running GNUPLOT')
            
    def test_two_dimensions(self):

        os.chdir('examples/two_dimensions')
        sys.path.append(os.getcwd())
        custom_execfile('two_dimensions.py')
        try:
            subprocess.call(['gnuplot','two_dimensions.gpl'])
        except:
            print('Error running GNUPLOT')
        
    def test_multiply_connected(self):

        os.chdir('examples/multiply_connected')
        sys.path.append(os.getcwd())
        custom_execfile('multiply_connected.py')
        try:
            subprocess.call(['gnuplot','multiply_connected.gpl'])        
        except:
            print('Error running GNUPLOT')

    def test_compile(self):

        os.chdir('examples/compiling')
        sys.path.append(os.getcwd())
        sos = glob.glob('system_body*.so')
        for so in sos:
            os.remove(so)
        
        custom_globals = globals()     
        custom_globals['sys'].argv = ['system_profile.py','system_uncompiled_profile.dat']
        custom_execfile('system_profile.py')
                                          
        custom_globals = globals()
        custom_globals['sys'].argv = ['setup.py','build_ext','--inplace']
        custom_execfile('setup.py',custom_globals=custom_globals)
                                          
        custom_globals = globals()
        custom_globals['sys'].argv = ['system_profile.py','system_compiled_profile.dat']        
        custom_execfile('system_profile.py')
        
        # Handled during documentation generation
        # custom_globals = globals()
        # custom_globals['sys'].argv = ['system_output.py','system_uncompiled_profile.dat']        
        # custom_execfile('system_output.py')
        
        # custom_globals = globals()
        # custom_globals['sys'].argv = ['system_output.py','system_compiled_profile.dat']        
        # custom_execfile('system_output.py')
        
if __name__ == '__main__':
    unittest.main()
