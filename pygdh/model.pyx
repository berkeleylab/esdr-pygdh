from __future__ import print_function
#from __future__ import absolute_import

import numpy
cimport numpy
DTYPE = numpy.float_
ctypedef numpy.float_t DTYPE_t
from . import grid
from . cimport grid
#from grid cimport *
import scipy
import collections
import scipy.optimize
from . import pygdh_io
import time
import random

cdef class Model:
    """Defines basic structure of a discretized mathematical model statement.
    The user must create a derived class from this class.
    """
    
    def set_grid(self,grid_sequence,stored_solution_count=1):
        """Initialize Model object.

        **Parameter**

        ``grid_sequence``: a ``list`` or ``tuple`` of ``grid.Grid`` objects
        associated with spatial domains.  The index of an object in this
        sequence becomes the integer label associated with the object.


        ``stored_solution_count``: an integer giving the number of
        solutions that are used in the time discretization formulas
        appearing in equations associated with the present ``Grid``        
        """

        self.stored_solution_count = stored_solution_count
        #self.time = collections.deque(range(self.stored_solution_count+1))

        # Check that the parameter is a tuple or list
        assert (type(grid_sequence) == type(tuple())
                or type(grid_sequence) == type(list())), 'ERROR: Model.initialize_model: expected list or tuple of "Grid" objects as "grid_sequence" parameter.'

        for element in grid_sequence:
            # Check that this is an object of the grid.Grid type
            assert isinstance(element,grid.Grid), 'ERROR: Model.initialize_model: expected "Grid" objects as elements of "grid_sequence" parameter'

        # Store argument for later use
        self.grid = grid_sequence
        
        self.grid_count = len(grid_sequence)

        # Provide slow but convenient means of referencing Grid objects
        self.grid_with_name = {}
        for element in grid_sequence:
            self.grid_with_name[element.name] = element

        self.grid_number_with_name = {}
        for element_number in range(len(grid_sequence)):
            element = grid_sequence[element_number]
            self.grid_number_with_name[element.name] = element_number

        #### might add a hook here so that people can define aliases
            
        # By default, everything is unknown
        self.unknown = [ numpy.ones((self.grid[i].field_count,self.grid[i].unit_count),dtype=numpy.bool_) for i in range(self.grid_count)]

        # The user should override this method to indicate where the solution 
        # values are unknown
        self.declare_unknowns()

        ### it might be useful to find out which grids are being modified and to perform rotations within this object so that one Model can run somewhat independently of the others, otherwise we need to allocate more grid space and perform copies for the Models that run less often. if we want to solve separate fields in the same physical space at different intervals, we would have to do copying or would have to keep separate grids for the fields solved at different rates so that they could be rotated independently. looking back in time at grids for which one does not have complete control could lead to confusion though, we would probably have to do copying to a global array.
        
        # This will be used to associate governing equation methods with 
        # 'Units'
        self.equations = [ [ [] for unit_number in 
                             range(self.grid[grid_number].unit_count) ] 
                           for grid_number in range(self.grid_count) ]

        # Get declarations of numbers of scalar values returned by governing 
        # equation method
        self.equation_scalar_counts = None

        self.set_equation_scalar_counts()

        if self.equation_scalar_counts != None:

            # Associate equations with 'Unit' objects
            self.assign_equations()
        
            self.check_equations_and_unknowns_consistency()

            self.associate_residual_indices_with_equations()

            # These are tasks that must be performed after 'Unit' objects have been
            # associated with equations
            self.DOF_count = 0

            self.active_units = [ [] for i in range(self.grid_count) ]
            self.grid_DOF_count = [ 0 for i in range(self.grid_count) ]
            self.grid_DOF_count_array = [ None for i in range(self.grid_count) ]
            self.grid_DOF_vector = [i for i in range(self.grid_count)]
            self.grid_candidate_DOF_vector = [i for i in range(self.grid_count)]
            self.field_number_with_grid_and_DOF_numbers = [i for i in range(self.grid_count)]
            self.unit_number_with_grid_and_DOF_numbers = [i for i in range(self.grid_count)]
            self.grid_and_field_to_solution_index = [i for i in range(self.grid_count)]
            self.grid_oldest_DOF_vector = [i for i in range(self.grid_count)]

            for grid_number in range(self.grid_count):

                grid_obj = self.grid[grid_number]
                
                ## Calculate the total number of degrees of freedom

                self.grid_DOF_count[grid_number] = 0
                for unit in grid_obj.unit_with_number:

                    unit_DOF_count = grid_obj.field_count - (grid_obj.field_count - numpy.count_nonzero(self.unknown[grid_number][:,unit.number]))

                    # Sum the number of degrees of freedom in this model
                    self.grid_DOF_count[grid_number] += unit_DOF_count

                    # Keep track of the units that should be visited
                    if unit_DOF_count != 0:
                        self.active_units[grid_number].append(unit)

                self.DOF_count += self.grid_DOF_count[grid_number]

                # This is intended to avoid creating lists with range
                self.grid_DOF_count_array[grid_number] = numpy.array(range(self.grid_DOF_count[grid_number]),dtype=numpy.int16)

                self.grid_candidate_DOF_vector[grid_number] = numpy.empty(self.grid_DOF_count[grid_number])

                # Generate mappings needed for the residual calculations
                self.generate_DOF_and_field_mappings(grid_number)        

                # This Grid is recognized as time-dependent if it stores past solutions
                grid_obj.time_dependent = (grid_obj.stored_solution_count > 1)

            if self.stored_solution_count != 1:
                self.set_initial_conditions()                

            for grid_number in range(self.grid_count):

                grid_obj = self.grid[grid_number]
                
                if grid_obj.time_dependent:
                    self.grid_DOF_vector[grid_number] = collections.deque( [ numpy.zeros(self.grid_DOF_count[grid_number]) for i in range(grid_obj.stored_solution_count-1) ] )
                    self.grid_DOF_vector[grid_number].appendleft(self.DOF_vector_from_fields(grid_number,grid_obj.field[0]))
                else:
                    # Only used if doing a nonlinear solve
                    #grid_obj.set_initial_guess()
                    self.grid_DOF_vector[grid_number] = collections.deque( [ self.DOF_vector_from_fields(grid_number,grid_obj.field[0]) ] )

            # This is defined to avoid creation of a new array on each solver
            # iteration
            #self.residual_vector = numpy.empty(self.DOF_count)

        self.input = None

        self.oldest_time = None
    def __del__(self):
        if self.input != None:
            self.input.close()            

    def load_solutions(self,input_filename_root):
        self.input = pygdh_io.Input(self.grid,input_filename_root)
        #self.initial_stored_timestep_number = self.input.hdf_file.attrs['initial_timestep_index']
        #self.final_stored_timestep_number = self.input.hdf_file.attrs['final_timestep_index']

    def check_consistency_of_unknowns(self):

        random.seed()
        value = random.random()

        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            for i in range(grid_obj.field_count):
                for j in range(grid_obj.unit_count):
                    grid_obj.field[0][i,j] = grid_number + i + j + value

        self.set_boundary_values()

        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            # Skip this check if the grid_obj.Grid object is being used to 
            # solve a time-dependent ODE
            if grid_obj.unit_count != 1:
                for vol in grid_obj.unit_with_number:
                    for field_number in range(grid_obj.field_count):
                        # Inspect the current solution values, which are
                        # potentially modified by 'set_boundary_values'
                        field = grid_obj.field[0]

                        # Assert that the field value associated with 'unit'
                        # has been set if and only if it corresponds to an 
                        # field marked as having an unknown value
                        assert(
                            abs(field[field_number,vol.number] 
                                - grid_number+field_number+vol.number+value) > 1e-8
                             and not self.unknown[grid_number][field_number,vol.number]), "ERROR: Model.check_consistency_of_unknowns: mismatch between prescribed value boundary condition " + str(field[field_number,vol.number]) + " and unknown field status " + str(self.unknown[grid_number][field_number,vol.number]) + ' ' + str(grid_number + field_number + vol.number + value) + " for field named " + grid_obj.field_names[field_number] + " with number " + str(field_number) + " on 'Unit' with index " + str(vol.index) + " on 'Grid' named " + grid_obj.name + " with number " + str(grid_number)

        ## Restore initial settings

        for grid in self.grid:
            for i in range(grid.field_count):
                for j in range(grid.unit_count):
                    grid.field[0][i,j] = 0.0

    def check_consistency_of_initial_and_boundary_conditions(self):

        for grid_obj in self.grid:
            if grid_obj.stored_solution_count > 1:
                grid_obj.candidate_field[:,:] = grid_obj.field[0][:,:]
        self.set_boundary_values()
        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            if grid_obj.stored_solution_count > 1:
                for field_number in range(grid_obj.field_count):
                    for unit_number in range(grid_obj.unit_count):
                        if grid_obj.candidate_field[field_number,unit_number] != grid_obj.field[0][field_number,unit_number]:
                            print('WARNING: Model.check_consistency_of_initial_and_boundary_conditions: mismatch between initial condition and boundary condition on "Grid" named', grid_obj.name, 'with number', grid_number, 'on "Unit" number', unit_number, 'for field named', grid_obj.field_names[field_number],'with number' + str(field_number))
            
    def distribute_full_DOF_vector_to_Grid_objects(self,DOF_vector):
        """DOF_vector: NumPy array storing one scalar for each DOF in entire
        model.

        Stores subsequences in similar vectors belonging to each 'Grid',
        for the associated degrees of freedom and the present timestep, as
        well as in the 'field' members"""

        split_start = 0

        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            split_end = split_start + self.grid_DOF_count[grid_number]
            # Save to the present timestep entry
            self.grid_DOF_vector[grid_number][0][:] = DOF_vector[split_start:split_end]
            split_start = split_end
            self.copy_DOF_vector_to_fields(self.grid_DOF_vector[grid_number][0],grid_number,grid_obj.field[0])

    def full_DOF_vector_from_fields(self,time_index):
        """Returns NumPy ``ndarray`` containing all degrees of freedom in the
        model.
        """
        full_DOF_vector_list = [ 
            self.DOF_vector_from_fields(grid_number,self.grid[grid_number].field[time_index]) 
            for grid_number in range(self.grid_count) ]
        
        return numpy.concatenate(full_DOF_vector_list)        

    def full_DOF_vector_from_DOF_vectors(self,time_index):
        """Returns NumPy ``ndarray`` containing all degrees of freedom in the
        model.
        """
        full_DOF_vector_list = [ self.grid_DOF_vector[grid_number][time_index] 
                                 for grid_number in range(self.grid_count) ]
        
        return numpy.concatenate(full_DOF_vector_list)            

    # def solve_time_independent_system(self,output_filename_root,output_types):
    #     """output_filename_root: a string from which all output filenames will
    #       be generated

    #     output_types: a list of strings specifying the desired output file
    #       formats.  Currently available options are 'HDF5', 'CSV', and 
    #       'GNUPLOT'"""

    #     # At the boundaries for which the solution variable values are given
    #     # rather than computed, set the appropriate values in the 'field' 
    #     # members of the 'Grid' objects
    #     self.set_boundary_values()
        
    #     # Copy initial solution guess from the values provided in the 'field'
    #     # members of the 'Grid' objects (stored for the "present" timestep with
    #     # numerical label 0)
    #     guess = self.full_DOF_vector_from_fields(0)
        
    #     ### Solve the nonlinear system, starting from the initial guess

    #     print('INFO: Initialization complete, starting time-independent solver.')
        
    #     [solution, ier, mesg] = self.solve(self.system_residuals,guess)

    #     # Copy the solution to the 'field' members of the 'Grid' objects, for
    #     # the "present" timestep with numerical label 0
    #     self.distribute_full_DOF_vector_to_Grid_objects(solution)

    #     # The user may override this method in order to perform certain
    #     # processing tasks on the numerical solution
    #     self.process_solution()

    #     # Create object to manage all output
    #     output = pygdh_io.Output(self.grid,output_filename_root,output_types)

    #     # Save the mappings needed to reconstruct the solution over the domain 
    #     # from the arrays of solution values
    #     output.write_mappings()

    #     # Save the numerical solution
    #     output.write_fields(0,0.0)
        
    #     output.close()

    #     print('INFO: Time-independent solver method finished.')

    def initial_guess(self,timestep_size,last):

        current = self.full_DOF_vector_from_DOF_vectors(-1)
        if len(self.time) == 1:
            # This allows for the time-dependent solver to be used to improve
            # convergence of time-independent models
            return current
        else:
            last_timestep_size = self.time[-1] - self.time[-2]
            timestep_ratio = timestep_size / last_timestep_size
            return current*(1.0 + timestep_ratio) - last*timestep_ratio

    def close_output_and_revert_fields(self,output):
        output.close()
        self.time[0] = self.oldest_time
        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            # Remove the bad solution, this is the same as candidate_field
            self.grid_DOF_vector[grid_number].popleft()
            grid_obj.field.popleft()                    
            # Restore the oldest solution
            self.grid_DOF_vector[grid_number].appendleft(self.grid_oldest_DOF_vector[grid_number])
            grid_obj.field.appendleft(grid_obj.oldest_field)                    
            # Rotate the previous solution into the current position
            grid_obj.field.rotate(1)
            self.grid_DOF_vector[grid_number].rotate(1)

    def time_dependent_system_step(self,timestep_size,output,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,interrupted=False):

        self.timestep_size = timestep_size
        self.inverse_timestep_size = 1./self.timestep_size

        # This is first set in self.initialize_time_dependent_solver()
        # PyGDH doesn't actually use it for much
        self.timestep_index += 1
        
        # This is used to extrapolate the initial guess after the first timestep
        last_full_DOF_vector = self.full_DOF_vector_from_DOF_vectors(-1)

        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            self.grid_DOF_vector[grid_number].rotate(-1)
        #     self.grid_oldest_DOF_vector[grid_number] = self.grid_DOF_vector[grid_number].popleft()
        #     self.grid_DOF_vector[grid_number].appendleft(self.grid_candidate_DOF_vector[grid_number])

#        while True:
            if interrupted or self.timestep_index == 1 or use_predictor == False:
                #initial_guess = self.full_DOF_vector_from_DOF_vectors(-1)
                initial_guess = self.full_DOF_vector_from_fields(-1)                
            else:
                initial_guess = self.initial_guess(self.timestep_size,last_full_DOF_vector)
            #print('initial',initial_guess)
            # Solve the nonlinear system, starting from an initial guess
            if handle_exception_in_solver:
                try:
                    [solution, ier, mesg] = self.solve(self.system_residuals,initial_guess)
                except:
                    print('ERROR: Quitting due to exception raised in nonlinear solver, set handle_exception_in_solver=False to see details.')
                    return False
            else:
                [solution, ier, mesg] = self.solve(self.system_residuals,initial_guess)                
            # else:
            #     try:
            #         [solution, ier, mesg] = self.solve(self.system_residuals,initial_guess)
            #     except:
            #         if adaptive_timestepping:
            #             print('ERROR: Exception raised, attempting to reduce timestep size')
            #             if 0.5*self.timestep_size < minimum_timestep_size:
            #                 print('ERROR: Timestep size has reached lower limit, quitting.')
            #                 output.close()
            #                 return False
            #             else:
            #                 self.timestep_size *= 0.5
            #                 self.inverse_timestep_size *= 2.0
            #                 print('ERROR: Exception raised, reducing timestep size to', self.timestep_size, '...')
            #                 continue
            #         else:
            #             print('ERROR: Quitting due to exception raised in nonlinear solver.')
            #             output.close()
            #             raise
            #             #return False

            # if ier != 1:
            #     if adaptive_timestepping:
            #         if 0.5*self.timestep_size < minimum_timestep_size:
            #             print('ERROR: Solver error, timestep size has reached lower limit, quitting.')
            #             return False
            #         else:
            #             self.timestep_size *= 0.5
            #             self.inverse_timestep_size *= 2.0
            #             print('ERROR: Solver error, reducing timestep size to', self.timestep_size)
            #             continue
            #     else:
            #         print('ERROR: Quitting due to failure of nonlinear solver.')
            #         return False
            # else:
            #     if not quiet:
            #         print(mesg)
            #     break

        # # Since this might have been successful, make the changes permanent
        # for grid_number in range(self.grid_count)
        #grid_obj = self.grid[grid_number]
        #     self.grid_oldest_DOF_vector[grid_number] = self.grid_DOF_vector[grid_number].popleft()
        #     self.grid_DOF_vector[grid_number].appendleft(self.grid_candidate_DOF_vector[grid_number])

        # Put solution into human-friendly terms
        if self.DOF_count == 1:
            # If ``system_residuals`` returns a NumPy ``ndarray`` containing only a single value (that is, the model to be solved is a single scalar equation), ``fsolve`` will return a scalar solution, not a vector containing a single element, as expected by ``distribute_full_DOF_vector_to_Grid_objects``
            self.distribute_full_DOF_vector_to_Grid_objects([solution])
        else:
            self.distribute_full_DOF_vector_to_Grid_objects(solution)

            #self.grid_candidate_DOF_vector[grid_number] = self.grid_oldest_DOF_vector[grid_number]

        return True   
        
    # def solve_time_dependent_system(self,output_filename_root,output_types,maximum_time_interval=None,timestep_size=None,timestep_count=None,handle_exception_in_solver=True,adaptive_timestepping=False,minimum_timestep_size=1e-6,use_predictor=True,quiet=False,write_initial_fields=True):
    #     """Solve time-dependent system.
        
    #     **Parameters**

    #     ``restart``: ``False`` if this is a new simulation, ``True`` if restarting from saved data
        
    #     ``output_filename_root``: a string that will be used at the start of all
    #     names of files created for output

    #     ``output_types``: a ``list`` of strings specifying the desired data
    #     output formats.  Currently available options are ``'HDF5'``, ``'CSV'``,
    #     and ``'GNUPLOT'``

    #     **Returns** solution vector
    #     """

    #     assert ((maximum_time_interval != None and timestep_size != None) or (maximum_time_interval != None and timestep_count != None) or (timestep_count != None and timestep_size != None)), 'ERROR: Model.solve_time_dependent_system: Not enough information provided to perform time-dependent simulation.'
        
    #     assert self.stored_solution_count != 1, 'ERROR: pygdh.model.solve_time_dependent_system: self.stored_solution_count == 1'
        
    #     # The 0th element of the 'field' member is assumed to contain the 
    #     # solution at the previous timestep on all 'Grid' objects.
    #     # This also sets self.timestep_index
    #     self.initialize_time_dependent_solver(0)

    #     # Make the timestep size available to the other methods
    #     if timestep_size == None:
    #         timestep_size = maximum_time_interval / float(timestep_count)
    #     elif timestep_count == None:
    #         timestep_count = int(round(maximum_time_interval / timestep_size))
    #     else:
    #         maximum_time_interval = timestep_size * timestep_count
            
    #     self.timestep_size = timestep_size
    #     self.inverse_timestep_size = 1. / timestep_size          

    #     #self.timestep_index = 0

    #     # Before rotating the initial conditions into the past solution
    #     # position, use them to obtain the initial solution guess
    #     if self.input == None or use_predictor == False: 
    #         self.time[0] = 0.0
    #         self.set_boundary_values()
    #         self.process_solution()
    #         initial_guess = self.full_DOF_vector_from_fields(0)
    #     else:
    #         self.set_boundary_values()
    #         self.process_solution()
    #         initial_guess = self.initial_guess(self.timestep_size,self.full_DOF_vector_from_DOF_vectors(-1))

    #     # Create object to handle all output
    #     output = pygdh_io.Output(self.grid,output_filename_root,output_types)

    #     # Now that the domain is fully specified, write out mappings needed to 
    #     # reconstruct the solution over the domain from the arrays of solution
    #     # values
    #     output.write_mappings()
        
    #     start_time = self.time[0]
        
    #     # Store initial data and just take the time from the first 'Grid'
    #     # object
    #     if write_initial_fields:
    #         output.write_fields(0,start_time)

    #     print('INFO: Initialization complete, starting time-dependent solver.')

    #     wall_time_interval = []
    #     while (self.time[0] - start_time < maximum_time_interval) and (not adaptive_timestepping and self.timestep_index < timestep_count):

    #         # Rotate the initial solution into the -1 position.
    #         self.time.rotate(-1)

    #         # This is used to extrapolate the initial guess after the first timestep
    #         last_full_DOF_vector = self.full_DOF_vector_from_DOF_vectors(-1)
            
    #         for grid_number in range(self.grid_count):
    #             grid_obj = self.grid[grid_number]
    #             # self.time[-1] is initialized to zero by default            
    #             self.grid_DOF_vector[grid_number].rotate(-1)
    #             grid_obj.field.rotate(-1)
            
    #         self.timestep_index += 1

    #         # When multiple Models make use of the the same Grids, broadcasting the full DOF vector for a given Model does not guarantee that the Grid data for the current timestep is current. Therefore, the Grid data for the current timestep is initially populated with a copy from the last timestep
    #         for grid in self.grid:
    #             grid.field[0][0:grid.field_count,0:grid.unit_count] = grid.field[-1][0:grid.field_count,0:grid.unit_count]

    #         while True:
    #             if self.timestep_index == 1:
    #                 initial_guess = self.full_DOF_vector_from_DOF_vectors(-1)
    #             else:
    #                 initial_guess = self.initial_guess(self.timestep_size,last_full_DOF_vector)

    #             current_time = self.time[-1] + self.timestep_size
    #             # Just take the time from the first ``grid.Grid`` object
    #             if not quiet:
    #                 print("Timestep", self.timestep_index, "at time", current_time, '...')

    #             # Solve the nonlinear system, starting from an initial guess
    #             reference_time = time.time()
    #             if handle_exception_in_solver:
    #                 try:
    #                     [solution, ier, mesg] = self.solve(self.system_residuals,initial_guess)
    #                 except:
    #                     print('ERROR: Quitting due to exception raised in nonlinear solver, set handle_exception_in_solver=False to see details.')
    #                     self.close_output_and_revert_fields(output)
    #                     return wall_time_interval
    #             else:
    #                 try:
    #                     [solution, ier, mesg] = self.solve(self.system_residuals,initial_guess)
    #                 except:
    #                     wall_time_interval.append(time.time() - reference_time)
    #                     if adaptive_timestepping:
    #                         print('ERROR: Exception raised, attempting to reduce timestep size')
    #                         if 0.5*self.timestep_size < minimum_timestep_size:
    #                             print('ERROR: Timestep size has reached lower limit, quitting.')
    #                             output.close()
    #                             return wall_time_interval
    #                         else:
    #                             self.timestep_size *= 0.5
    #                             self.inverse_timestep_size *= 2.0
    #                             print('ERROR: Exception raised, reducing timestep size to', self.timestep_size, '...')
    #                             continue
    #                     else:
    #                         print('ERROR: Quitting due to exception raised in nonlinear solver.')
    #                         self.close_output_and_revert_fields(output)
    #                         raise
    #                         #return wall_time_interval

    #             if ier != 1:
    #                 if adaptive_timestepping:
    #                     if 0.5*self.timestep_size < minimum_timestep_size:
    #                         print('ERROR: Solver error, timestep size has reached lower limit, quitting.')
    #                         output.close()
    #                         wall_time_interval.append(time.time() - reference_time)
    #                         return wall_time_interval
    #                     else:
    #                         self.timestep_size *= 0.5
    #                         self.inverse_timestep_size *= 2.0
    #                         print('ERROR: Solver error, reducing timestep size to', self.timestep_size)
    #                 else:
    #                     print('ERROR: Quitting due to failure of nonlinear solver.')
    #                     output.close()
    #                     return wall_time_interval
    #             else:
    #                 if not quiet:
    #                     print(mesg)
    #                 break

    #         # Save the oldest field and add a new "present" field
    #         self.oldest_time = self.time[0]
    #         for grid_number in range(self.grid_count):
    #             grid_obj = self.grid[grid_number]
    #             self.grid_oldest_DOF_vector[grid_number] = self.grid_DOF_vector[grid_number].popleft()
    #             self.grid_DOF_vector[grid_number].appendleft(self.grid_candidate_DOF_vector[grid_number])
    #             grid_obj.oldest_field = grid_obj.field.popleft()
    #             grid_obj.field.appendleft(grid_obj.candidate_field)

    #         # Put solution into human-friendly terms
    #         if self.DOF_count == 1:
    #             # If ``system_residuals`` returns a NumPy ``ndarray`` containing only a single value (that is, the model to be solved is a single scalar equation), ``fsolve`` will return a scalar solution, not a vector containing a single element, as expected by ``distribute_full_DOF_vector_to_Grid_objects``
    #             self.distribute_full_DOF_vector_to_Grid_objects([solution])
    #         else:
    #             self.distribute_full_DOF_vector_to_Grid_objects(solution)

    #         # This acts on the values for the present timestep
    #         ### Consider moving this outward in a coordinated simulation
    #         self.set_boundary_values()

    #         # Check for the termination condition, before setting the initial guess on the next timestep
    #         if self.abort_solver_and_reject_solution():
    #             print('ERROR: Stopping time-dependent solver and rejecting most recent solution.')
    #             self.close_output_and_revert_fields(output)
    #             return wall_time_interval

    #         # The solution is acceptable, so keep the new element and switch the candidate_field members with oldest_field members
    #         self.time[0] = self.time[-1] + self.timestep_size
    #         for grid_number in range(self.grid_count):
    #             grid_obj = self.grid[grid_number]
    #             self.grid_candidate_DOF_vector[grid_number] = self.grid_oldest_DOF_vector[grid_number]
    #             grid_obj.candidate_field = grid_obj.oldest_field                

    #         # This is the user's opportunity to perform validation, and store results besides the solution
    #         self.process_solution()

    #         # Store solution data, just take the time from the first ``grid.Grid`` object
    #         output.write_fields(self.timestep_index,self.time[0])

    #         if self.abort_solver_and_accept_solution():
    #             print('ERROR: Stopping time-dependent solver and accepting most recent solution.')
    #             output.close()
    #             return wall_time_interval

    #     output.close()

    #     print('INFO: Time-dependent solver method finished.')
    #     return wall_time_interval
    
    def check_equations_and_unknowns_consistency(self):

        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            for unit in grid_obj.unit_with_number:
                unknown_count = numpy.count_nonzero(self.unknown[grid_number][:,unit.number])
                scalar_count = 0
                for equation in self.equations[grid_number][unit.number]:
                    scalar_count += self.equation_scalar_counts[equation]
                assert unknown_count == scalar_count, 'ERROR: Inconsistent number of scalar return values ' + str(scalar_count) + ' and unknowns ' + str(unknown_count) + ' on "Unit" with indices ' + str(unit.index) + ' of grid_obj.Grid ' + str(grid_number)

    def associate_residual_indices_with_equations(self):

        self.residual_start_index_for_unit_equation = [ 
            [ [i for i in range(len(self.equations[grid_number][unit_number]))] 
              for unit_number in range(self.grid[grid_number].unit_count) ] 
            for grid_number in range(self.grid_count) ]

        self.residual_end_index_for_unit_equation = [ 
            [ [i for i in range(len(self.equations[grid_number][unit_number]))]
              for unit_number in range(self.grid[grid_number].unit_count) ] 
            for grid_number in range(self.grid_count) ]

        residual_number = 0
        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            grid_equations = self.equations[grid_number]
            for unit in grid_obj.unit_with_number:
                grid_unit_equations = grid_equations[unit.number]
                for equation_number in range(len(grid_unit_equations)):
                    # This is a list associating each DOF with an index of the residual
                    self.residual_start_index_for_unit_equation[grid_number][unit.number][equation_number] = residual_number
                    residual_number += self.equation_scalar_counts[grid_unit_equations[equation_number]]
                    self.residual_end_index_for_unit_equation[grid_number][unit.number][equation_number] = residual_number

        self.DOF_count = residual_number                        

    def system_residuals(self,candidate):
        """candidate: a sequence containing values for all degrees of freedom
          in the system of equations

        Returns a vector containing the results from evaluating all of the
          methods describing governing equations"""
        
        cdef Py_ssize_t equation_index

        # The methods describing governing equations are written in terms of
        # the solution values arranged on 'Grid' objects, so the values in the
        # vector representing all degrees of freedom must be copied into the
        # 'Grid' structures
        self.distribute_full_DOF_vector_to_Grid_objects(candidate)

        # The values of the degrees of freedom do not include the given
        # boundary values, so these must be written into the 'Grid' structures
        # for use in the methods representing governing equations
        self.set_boundary_values()

        # For efficiency, the user may override this method so that 
        # calculations are performed once per iteration for quantities needed
        # by multiple governing equation methods 
        self.calculate_global_values()

        cdef numpy.ndarray[DTYPE_t, ndim=1] residual_vector = numpy.empty(self.DOF_count, dtype=DTYPE)

        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            
            ## Local variables are defined to improve efficiency

            grid_equations = self.equations[grid_number]

            for unit in self.active_units[grid_number]:
                
                grid_unit_equations = grid_equations[unit.number]

                residual_start_index_for_unit_equation = self.residual_start_index_for_unit_equation[grid_number][unit.number]

                residual_end_index_for_unit_equation = self.residual_end_index_for_unit_equation[grid_number][unit.number]
                
                # The user may specify intermediate calculations used by 
                # multiple equations but for the same 'Unit' here. This can
                # be used instead of 'Model.calculate_global_values' in
                # in order to reduce memory requirements when possible
                self.calculate_local_values(unit)

                for equation_index in range(len(grid_unit_equations)):

                    grid_unit_equations[equation_index](unit,residual_vector[
                            residual_start_index_for_unit_equation[equation_index]:residual_end_index_for_unit_equation[equation_index]])

        return residual_vector

    def generate_DOF_and_field_mappings(self,grid_number):
        # Could also be done with an array of 2-vectors, but make sure that this behavior is as documented.
        grid_obj = self.grid[grid_number]
        self.field_number_with_grid_and_DOF_numbers[grid_number] = numpy.empty(self.grid_DOF_count[grid_number], dtype=numpy.int16)

        self.unit_number_with_grid_and_DOF_numbers[grid_number] = numpy.empty(self.grid_DOF_count[grid_number], dtype=numpy.int16)

        self.grid_and_field_to_solution_index[grid_number] = [ numpy.empty(grid_obj.field_count, dtype=numpy.int16) for i in range(grid_obj.unit_count) ]

        next_index = 0
        for vol in grid_obj.unit_with_number:
            for field in range(grid_obj.field_count):
                if self.unknown[grid_number][field,vol.number]:
                    
                    self.field_number_with_grid_and_DOF_numbers[grid_number][next_index] = field
                    self.unit_number_with_grid_and_DOF_numbers[grid_number][next_index] = vol.number
                    self.grid_and_field_to_solution_index[grid_number][vol.number][field] = next_index
                    next_index += 1
                
    def copy_DOF_vector_to_fields(self,DOF_vector,grid_number,present_field):
        """DOF_vector: a sequence of values, one for each degree of freedom in
          the domain described by the present 'Grid' object
        
        present_field: a two-dimensional array indexed by solution variable
          number and unit number, into which the values corresponding to each
          'Unit' in the present 'Grid' are written"""

        # Mapping information is contained in 'field_number_with_DOF_number' 
        # and 'unit_number_with_DOF_number'

        for DOF_number in self.grid_DOF_count_array[grid_number]:
            present_field[self.field_number_with_grid_and_DOF_numbers[grid_number][DOF_number], self.unit_number_with_grid_and_DOF_numbers[grid_number][DOF_number]] = DOF_vector[DOF_number]

    def DOF_vector_from_fields(self,grid_number,present_field):
        """present_field: a sequence with one value for each 'Unit' contained
          in the present 'Grid'

        Returns a vector containing only those values representing degrees of
          freedom in the system of equations.
        """
        # depending on how the return values are used, it might be better to not create new vectors
        grid_obj = self.grid[grid_number]
        DOF_vector = numpy.zeros(self.grid_DOF_count[grid_number])

        for unit in self.active_units[grid_number]:
            for field_number in range(grid_obj.field_count):
                if self.unknown[grid_number][field_number,unit.number]:
                    DOF_vector[self.grid_and_field_to_solution_index[grid_number][unit.number][field_number]] = present_field[field_number,unit.number]

        return DOF_vector

    def initialize_time_dependent_solver(self,initial_timestep_index):
        self.timestep_index = initial_timestep_index
        self.custom_initialize_time_dependent_solver()
    
    ### These are placeholders for user-specified methods #####################

    def custom_initialize_time_dependent_solver(self):
        pass    
    
    def declare_unknowns(self):
        pass

    def set_equation_scalar_counts(self):
        print('ERROR: Required method "set_equation_scalar_counts" has not been defined.')
        #raise AssertionError

    def assign_equations(self):
        """Placeholder for user-defined method that determines which solution
        field values are to be calculated and which equations are to be solved
        for each ``Volume`` object in the model.

        For every ``Volume``, this routine should set the ``unknown_field``
        elements (each corresponding, in order, to the elements of the
        ``'field_names'`` ``list`` given at the associated ``grid.Grid``
        initialization), and the ``equations`` sequence, which contains all
        function objects associated with the discretized equations that govern
        the solution behavior over the ``Volume``
        """
        print('ERROR: Required method "assign_equations" has not been defined.')
        raise AssertionError

    def set_boundary_values(self):
        """Placeholder for user-defined method that sets boundary values on all
        ``grid.Grid`` objects at the present time.

        This routine must set the known solution values in the present solution
        value NumPy ``ndarray`` (index 0 in the ``self.field`` ``deque``) on all
        ``grid.Grid`` objects.  This must be consistent with the ``unknown_field``
        assignments on each ``Volume``

        The solution value ``ndarray`` objects are two-dimensional arrays, with
        the first index identifying the solution field variable (in the order
        defined by the ``'field_names'`` entry in the grid description provided
        at initialization), and the second index identifying the integer index
        label identifying the ``Volume`` object (the same index used to access
        the object through the ``unit_with_number`` member of the ``grid.Grid`` object)
        """
        pass

    def calculate_global_values(self):
        """Placeholder for optional user-defined method that calculates values
        used in multiple methods (or multiple calls to the same method),
        typically for multiple ``Volume`` objects.  This is executed before the
        discretized equation methods are evaluated for a given timestep.
        """
        pass
    
    def calculate_local_values(self,vol):
        """Placeholder for optional user-defined method that calculates values
        used in multiple methods (or multiple calls to the same method) for any
        given ``Volume`` object.  This is executed before the discretized
        equation methods are evaluated for a each ``Volume`` object.

        **Parameter**

        ``vol``: ``Volume`` object for which values are to be calculated

        **Note**

        If intermediate values are used multiple times for multiple discretized
        equation methods but for the same ``Volume`` object, it may be simpler
        to combine the discretized equation methods into a single method that
        returns a NumPy ``ndarray``.
        """
        pass

    def abort_solver_and_reject_solution(self):
        return False

    def abort_solver_and_accept_solution(self):
        return False

    def process_solution(self):
        """Placeholder for optional user-defined method called after solutions
        are obtained but before data are written to output files.

        **Note**

        The number of the current simulation timestep is always available as
        ``self.timestep_index``

        The current simulation time is always available as
        ``self.time[0]``
        """
        print('WARNING: Optional method ``process_solution`` not defined by user.')

    def restore_from_timestep(self,timestep):

        self.input.restore_from_timestep(timestep,self.time)
        self.compute_grid_DOF_vectors()

    def compute_grid_DOF_vectors(self):
        if self.equation_scalar_counts == None:           
            return
        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            for i in range(grid_obj.stored_solution_count):
                # Fill out the DOF_vector members of each Grid
                self.grid_DOF_vector[grid_number][-i][:] = self.DOF_vector_from_fields(grid_number,grid_obj.field[-i])

    # This could be done at the Grid or Coordinator levels, but the Model objects are really responsible for describing the mathematical models to be solved and so are likely to hold the necessary physical information. One could query the Model objects for information to initialize the Grid objects, but the Model initialization routines currently require the Grid objects as inputs, so delayed initializations would be required.
    # In a coordinated simulation, it is assumed that each Grid is primarily owned by a single Model, or that if there are multiple Models writing to the same Grid, the user will make sure that they do not overwrite each other's initial condition specifications. 
    def set_initial_conditions(self):
        """Placeholder for required user-defined method that sets initial values
        on all ``Grid`` objects.

        This routine must set all values in the present solution value NumPy
        ``ndarray`` (index 0 in the ``self.field`` ``deque``) on all ``Grid``
        objects.

        The solution value ``ndarray`` objects are two-dimensional arrays, with
        the first index identifying the solution field variable (in the order
        defined by the ``'field_names'`` entry in the grid description provided
        at initialization), and the second index identifying the integer index
        label identifying the ``volume.Volume`` object (the same index used to access
        the object through the ``unit_with_number`` member of the ``Grid`` object)
        """
        print('ERROR: Required method ``set_initial_conditions`` not defined')
        raise AssertionError

    def set_initial_guess(self):
        """Placeholder for optional user-defined method that sets initial
        guess for solutions on this ``Grid`` object.

        This routine must set all values in the present solution value NumPy
        ``ndarray`` (index 0 in the ``self.field`` ``deque``).

        The solution value ``ndarray`` objects are two-dimensional arrays, with
        the first index identifying the solution field variable (in the order
        defined by the ``'field_names'`` entry in the grid description provided
        at initialization), and the second index identifying the integer index
        label identifying the ``volume.Volume`` object (the same index used to access
        the object through the ``unit_with_number`` member of the ``Grid`` object)
        """

        print('WARNING: Optional method ``set_initial_guess`` not defined, using zero')
        # Default guess is zero everywhere.
        self.field[0][:,:] = 0.
        
    def solve(self,residual_function,guess):

        [solution, infodict, ier, mesg] = scipy.optimize.fsolve(
            self.system_residuals,guess,full_output=True)

        return [solution, ier, mesg]
    
