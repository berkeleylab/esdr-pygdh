cimport numpy
from . cimport grid
#from grid cimport*
cdef class Model:
    cdef public int stored_solution_count, grid_count, timestep_index, DOF_count
    cdef public time, grid, grid_with_name, grid_number_with_name, oldest_time, timestep_size, inverse_timestep_size, DOF_count_array, input, equations, equation_scalar_counts, residual_start_index_for_unit_equation, residual_end_index_for_unit_equation, unknown, active_units, grid_DOF_count, grid_DOF_count_array, grid_DOF_vector, grid_candidate_DOF_vector, field_number_with_grid_and_DOF_numbers, unit_number_with_grid_and_DOF_numbers, grid_and_field_to_solution_index, grid_oldest_DOF_vector
