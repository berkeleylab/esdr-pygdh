from __future__ import print_function
from __future__ import absolute_import

import numpy
from . import grid
import os
import sys
if sys.version_info.major == 2:
    import cPickle as pickle
else:
    import pickle

## Objects of these classes should be created after the entire problem structure has been determined.

class Output:

    def sequence_to_numbered_string(self,string_sequence,starting_number):
        number = starting_number
        return_string = ''
        for string in string_sequence:
            return_string = return_string + str(number) + '. ' + string + ' | '
            number += 1
        return return_string

    def __init__(self,grid,output_filename_root,output_types):

        # Store a reference to the grid structure locally
        self.grid = grid
        self.grid_count = len(self.grid)

        ## Basic argument checks
        
        try:
            assert type(output_filename_root) == type('')
        except:
            print('ERROR: "output_filename_root" argument to io.__init__ should be a string')
            raise

        try:
            assert (type(output_types) == type([])
                    or type(output_types) == type(tuple()))
        except:
            print('ERROR: "output_types" argument to io.__init__ should be a list or tuple')
            raise

        try:
            for output_type in output_types:
                assert type(output_type) == type('')
        except:
            print('ERROR: Elements of "output_types" argument to io.__init__ should be strings')
            raise
        
        # Dictionary for mapping names of output types to output files
        self.output = { }

        for output_type in output_types:
            output_type_lower = output_type.lower()
            if output_type_lower == 'hdf' or output_type_lower == 'hdf5':
                
                # Importing here rather than at the start of the file because 
                # the user may have chosen not to install h5py
                import h5py
                
                output_filename = output_filename_root + '.h5'

                print('INFO: Opening HDF5 output file', output_filename)

                # Attempt to create file, and fail if the file exists, to avoid
                # accidentally overwriting earlier results
                hdf_file = h5py.File(output_filename,'w-')

                # HDF5 output will contain extra detail
                hdf_file.attrs['grid_count'] = self.grid_count

                # For convenience, create one group for each 'Grid' object
                self.grid_group = [ 
                    hdf_file.create_group('grid_' + str(grid_index)) 
                    for grid_index in range(self.grid_count) ]

                for grid_index in range(self.grid_count):
                    self.grid_group[grid_index].attrs['dimension_count'] = self.grid[grid_index].dimension_count
                    self.grid_group[grid_index].attrs['field_count'] = self.grid[grid_index].field_count
                    #self.grid_group.attrs['unit_count'] = self.grid[grid_index].unit_count
                    for field_index in range(self.grid[grid_index].field_count):
                        self.grid_group[grid_index].attrs['field_name_' + str(field_index)] = self.grid[grid_index].field_names[field_index]

                    # Store the arrays of coordinates along each dimension
                    # this should reference the appropriate grid
                    for dimension in range(self.grid[grid_index].dimension_count):
                        coordinate_dataset = self.grid_group[grid_index].create_dataset("coordinates_" + str(dimension),data=self.grid[grid_index].coordinates_list[dimension])

                # still have to store mappings, but these aren't yet available--maybe initialize this after that information becomes available?

                self.output['HDF5'] = hdf_file

            elif output_type_lower == 'csv':
                import csv
                output_filenames = [output_filename_root + '_' + self.grid[grid_index].name + '.csv' for grid_index in range(self.grid_count) ]
                output_files = [ open(output_filename,'w') for output_filename in output_filenames ]
                self.output['CSV'] = output_files
                self.csv_writer =  [ csv.writer(output_file,lineterminator='\n') for output_file in output_files ]
                for output_filename in output_filenames:
                    print('INFO: Opened CSV output file', output_filename)
                    
                for grid_number in range(self.grid_count):
                    grid_obj = self.grid[grid_number]
                    
                    if grid_obj.unit_count == 1:
                        self.csv_writer[grid_number].writerow(['time'] + grid_obj.field_names + grid_obj.output_fields.keys())
                    else:
                        self.csv_writer[grid_number].writerow(['timestep','time'] + grid_obj.coordinate_order + grid_obj.field_names + grid_obj.output_fields.keys())                        
                    
            elif output_type_lower == 'gnuplot':
                output_filenames = [ output_filename_root + '_' + self.grid[grid_index].name + '.gnuplot' for grid_index in range(self.grid_count) ]
                self.output['GNUPLOT'] = [ open(output_filename,'w') for output_filename in output_filenames ]
                for output_filename in output_filenames:
                    print('INFO: Opened GNUPLOT output file', output_filename)

                for grid_number in range(self.grid_count):
                    grid_obj = self.grid[grid_number]
                    
                    solutions_string = self.sequence_to_numbered_string(grid_obj.field_names,grid_obj.dimension_count+1)
                    if len(grid_obj.output_fields) == 0:
                        additional_fields_string = ''
                    else:

                        additional_fields_string = self.sequence_to_numbered_string(grid_obj.output_fields.keys(),grid_obj.dimension_count+grid_obj.field_count+1)
                    if grid_obj.unit_count == 1:
                        self.output['GNUPLOT'][grid_number].write('# time | ' + solutions_string + additional_fields_string + '\n')
                    else:
                        coordinates_string = self.sequence_to_numbered_string(grid_obj.coordinate_order,1)
                        self.output['GNUPLOT'][grid_number].write('# ' + coordinates_string + solutions_string + additional_fields_string + '\n')
                    
            elif output_type_lower == 'pickle':
                output_filename = output_filename_root + '.pkl'
                # Prevent accidentally overwriting old output file
                assert not os.access(output_filename,os.F_OK), 'ERROR: Pickle output file "' + output_filename + '" already exists.'
                self.output['PICKLE'] = open(output_filename,'wb')
                print('INFO: Opened Pickle output file', output_filename)
                pickle.dump(self.grid_count,self.output['PICKLE'],-1)

        # This indicates that output has not yet been written; later it will
        # store the number of the first timestep written out.
        self.initial_timestep_index = None

    def write_hdf5_fields(self,timestep,time):

        ## if timestep < self.initial_timestep_index:
        ##     self.initial_timestep_index = timestep
        ## elif self.final_timestep_index < timestep:
        ##     self.final_timestep_index = timestep
            
        timestep_string = str(timestep)
        
        for grid_index in range(self.grid_count):
            grid = self.grid[grid_index]
            #dataset = self.grid_group[grid_index].create_dataset('data_'+str(timestep),data=self.grid[grid_index].field[0])
            # I'm not sure that this is much of an improvement
            dataset = self.grid_group[grid_index].create_dataset('solution_'+timestep_string,data=grid.field[0])
            #dataset = self.write_data_return_dataset(self.grid[grid_index].field[0],grid_index,'data',timestep)
            dataset.attrs['time'] = time

            if timestep != 0:
                # This checks that only the initial conditions are written out.  Intermediates are most likely not computed prior to the first timestep.
                # Write additional fields
                for name,data in grid.output_fields.iteritems():
                    self.grid_group[grid_index].create_dataset(name+'_'+timestep_string,data=data)
        self.output['HDF5'].flush()

    def write_pickle_fields(self,timestep,time):

        timestep_string = str(timestep)

        pickle.dump((timestep,time),self.output['PICKLE'],-1)
        for grid_index in range(self.grid_count):
            grid = self.grid[grid_index]
            pickle.dump(grid.field[0],self.output['PICKLE'],-1)

            if timestep != 0:
                # This checks that only the initial conditions are written out.  Intermediates are most likely not computed prior to the first timestep.
                # Write additional fields
                for name,data in grid.output_fields.iteritems():
                    pickle.dump(grid.output_fields,self.output['PICKLE'],-1)

    def write_csv_fields(self,timestep,time):

        # Loop over all volumes on all grids
        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]

            if 'output_fields' in dir(grid_obj):
                additional_names_list = grid_obj.output_fields.keys()
            else:
                additional_names_list = []

            if len(additional_names_list) != 0:
                output_fields_list = [ grid_obj.output_fields[key] for key in additional_names_list ]
                output_fields_array = numpy.column_stack(output_fields_list)

            if grid_obj.unit_count == 1:
                # Single-volume grid is being used to solve ODEs

                ## Write out present field values for volume 0
                if len(additional_names_list) == 0:
                    output_row = numpy.concatenate(((timestep,),grid_obj.field[0][:,0]))
                else:
                    output_row = numpy.concatenate(((timestep,),grid_obj.field[0][:,0],output_fields_array[vol.number,:]))

                self.csv_writer[grid_number].writerow(output_row)

            else:

                if timestep != None:
                    self.csv_writer[grid_number].writerow((timestep,time))
                
                for vol in grid_obj.volume_from_number:

                    ## Write out present field values

                    if len(additional_names_list) == 0:
                        if grid_obj.dimension_count == 1:
                            output_row = numpy.concatenate(((None,None,vol.coordinate),grid_obj.field[0][:,vol.number]))
                        else:
                            output_row = numpy.concatenate(((None,None),vol.coordinate,grid_obj.field[0][:,vol.number]))
                    else:
                        if grid_obj.dimension_count == 1:                        
                            output_row = numpy.concatenate(((None,None,vol.coordinate),grid_obj.field[0][:,vol.number],output_fields_array[vol.number,:]))
                        else:
                            output_row = numpy.concatenate(((None,None),vol.coordinate,grid_obj.field[0][:,vol.number],output_fields_array[vol.number,:]))

                    self.csv_writer[grid_number].writerow(output_row)
                    
    def write_gnuplot_fields(self,timestep,time):
        time_string = str(time) + ' '
        # Write data for each grid
        for grid_number in range(self.grid_count):
            grid_obj = self.grid[grid_number]
            file_obj = self.output['GNUPLOT'][grid_number]

            if grid_obj.unit_count == 1:

                # Write the time value but do not terminate the line
                file_obj.write(time_string)

                # Write data from current solution
                for field in grid_obj.field[0]:
                    # Write data from the only volume (index 0)
                    file_obj.write(' ' + str(field[0]))

                for name,data in grid_obj.output_fields.iteritems():
                    data_point = data[0]                        
                    if data_point == None:
                        file_obj.write(' inf')
                    else:
                        file_obj.write(' ' + str(data_point))

                # Terminate the line
                file_obj.write('\n')
                # No line is skipped between timesteps            

            elif grid_obj.dimension_count == 1:

                file_obj.write('# timestep ' + str(timestep) + ', time ' + str(time) + '\n')
                # In one dimension, the volume index label also suggests volume position
                for vol_index in range(grid_obj.index_bounds[0]):

                    # Write the coordinate value but do not terminate the line

                    coord_string = str(grid_obj.coordinates_list[0][vol_index])
                    file_obj.write(coord_string)

                    # Write each current (index 0) solution field
                    for field in grid_obj.field[0]:
                        file_obj.write(' ' + str(field[vol_index]))
                    # should skip this if the dictionary doesn't exist.

                    for name,data in grid_obj.output_fields.iteritems():
                        data_point = data[vol_index]                        
                        if data_point == None:
                            file_obj.write(' inf')
                        else:
                            file_obj.write(' ' + str(data_point))

                    # Terminate the line
                    file_obj.write('\n')

                # Skip two lines between timesteps
                file_obj.write('\n\n')        

            elif grid_obj.dimension_count == 2:
                file_obj.write('# timestep ' + str(timestep) + ', time ' + str(time) + '\n')                
                for coord_index0 in range(len(grid_obj.coordinates_list[0])):
                    coord0 = grid_obj.coordinates_list[0][coord_index0]
                    coord0_str = str(coord0)
                    for coord_index1 in range(len(grid_obj.coordinates_list[1])):
                        coord1 = grid_obj.coordinates_list[1][coord_index1]
                        coord_string = coord0_str + ' ' + str(coord1)
                        vol_index = grid_obj.unit_number_with_index[coord_index0][coord_index1]
                        file_obj.write(coord_string)                        
                        if vol_index == -1:
                            # Write entries that GNUPLOT will skip without confusion
                            for field in grid_obj.field[0]:
                                file_obj.write(' inf')
                            for name,data in grid_obj.output_fields.iteritems():
                                file_obj.write(' inf')
                        else:
                            # Write data from current solution
                            for field in grid_obj.field[0]:
                                file_obj.write(' ' + str(field[vol_index]))
                            for name,data in grid_obj.output_fields.iteritems():
                                data_point = data[vol_index]
                                if data_point == None:
                                    file_obj.write(' inf')
                                else:
                                    file_obj.write(' ' + str(data_point))
                        # Terminate line
                        file_obj.write('\n')                        
                    file_obj.write('\n')
                # Skip a line between timesteps
                file_obj.write('\n')        

    def write_fields(self,timestep,time):
        # How about time-independent runs?
        if self.initial_timestep_index == None:
            self.initial_timestep_index = timestep
            self.final_timestep_index = timestep            
            #if timestep < self.initial_timestep_index:
            #self.initial_timestep_index = timestep
        elif self.final_timestep_index < timestep:
            self.final_timestep_index = timestep
            
        for key,value in self.output.iteritems():
            
            if key == 'HDF5':
                self.write_hdf5_fields(timestep,time)
                
            elif key == 'CSV':
                self.write_csv_fields(timestep,time)
                
            elif key == 'GNUPLOT':
                self.write_gnuplot_fields(timestep,time)

            elif key == 'PICKLE':
                self.write_pickle_fields(timestep,time)

    def write_mappings(self):
        if 'HDF5' in self.output:
            ## Write to output file
            for grid_number in range(self.grid_count):
                grid_obj = self.grid[grid_number]
                if grid_obj.dimension_count > 1:               
                    # Store the mappings of volume elements to the grid
                    index_from_volume_number = self.grid_group[grid_number].create_dataset('index_from_volume_number',(grid_obj.unit_count,2),'i')
                    for vol in grid_obj.unit_with_number:
                        index_from_volume_number[vol.number,0] = vol.index[0]
                        index_from_volume_number[vol.number,1] = vol.index[1]

    def close(self):
        for output_type,output in self.output.iteritems():
            if type(output) == type([]):
                for grid_output in output:
                    grid_output.close()
            elif output_type == 'HDF5':
                self.output['HDF5'].attrs['initial_timestep_index'] = self.initial_timestep_index
                self.output['HDF5'].attrs['final_timestep_index'] = self.final_timestep_index 
            else:
                output.close()
        print('INFO: All output files closed.')

    def write_data_return_dataset(self,data,grid_number,dataset_name_root,timestep):
        """Write data to the output file and return an object associated with the written data, if possible.

        *Parameters*:
        *Returns*:
        """
        if 'HDF5' in self.output:
            dataset = self.grid_group[grid_number].create_dataset(dataset_name_root+'_'+str(timestep),data=data)
            return dataset

class Input:
    def __init__(self,grid_list,input_filename_root):
        
        ## Argument type checks

        for current_grid in grid_list:
            assert isinstance(current_grid,grid.Grid), 'ERROR: Input.__init__: "grid" argument should be a sequence of "Grid" objects'
        
        assert (isinstance(input_filename_root,str)
                or isinstance(input_filename_root,unicode)), 'ERROR: Input.__init__: "input_filename_root" argument should be a string'

        # Store the 'grid' parameter
        self.grid = grid_list

        self.use_HDF5 = os.access(input_filename_root + '.h5',os.F_OK)
        if self.use_HDF5:
            # Import this library here, because it is optional
            import h5py

            ## Open HDF5 file containing solutions
            ## Unfortunately, the behavior when unsuccessful is not documented
        
            input_filename = input_filename_root + '.h5'
            self.hdf_file = h5py.File(input_filename)
            print('INFO: Opened HDF5 input file', input_filename)

            ## Check that the number of 'Grid' objects into which data will be
            ## read is the same as the number of 'Grid' objects stored in the file
        
            self.grid_count = self.hdf_file.attrs['grid_count']
            assert len(self.grid) == self.grid_count
            
            # self.hdf_file_grid = [ None for index in range(self.grid_count) ]
            # for grid_index in range(self.grid_count):
            #     self.hdf_file_grid[grid_index] = self.hdf_file['grid_' + str(grid_index)]

            # This is for convenient access to the stored 'Grid' information
            self.hdf_file_grid = [ self.hdf_file['grid_' + str(grid_number)]
                                   for grid_number in range(self.grid_count) ]

            self.initial_timestep_index = self.hdf_file.attrs['initial_timestep_index']
            self.final_timestep_index = self.hdf_file.attrs['final_timestep_index']

            print('INFO: HDF5 input file starts at timestep index', self.initial_timestep_index, 'and ends at index', self.final_timestep_index)

        elif os.access(input_filename_root + '.pkl',os.F_OK):

            input_filename = input_filename_root + '.pkl'
            self.pickle_file = open(input_filename,'rb')
            print('INFO: Opened pickle input file', input_filename)
            self.grid_count = pickle.load(self.pickle_file)

        else:
            print('ERROR: pygdh.Input.__init__: No HDF5 or Pickle file available.')
            raise AssertionError
        
        self.check_consistency()

        self.last_restored_timestep_index = None

    def check_consistency(self):
        pass

    def restore_from_timestep(self,timestep_index,time_deque):

        assert isinstance(timestep_index,int), 'ERROR: pygdh.Input.restore_from_timestep: "timestep_index" parameter should be an integer'

        if self.use_HDF5:
            # Allow indices with negative values, just as in Python sequences
            if timestep_index < 0:
                timestep_index = self.final_timestep_index + timestep_index + 1

            assert (timestep_index >= self.initial_timestep_index and timestep_index <= self.final_timestep_index), 'ERROR: pygdh.Input.restore_from_timestep: timestep not found in file'

            for grid in self.grid:
                print('INFO: Restoring solution on', '"' + grid.name + '"', 'at timestep', timestep_index)
                available_past_timestep_count = timestep_index - self.initial_timestep_index
                if available_past_timestep_count < grid.stored_solution_count-1:
                    print('ERROR: Initially restoring only', available_past_timestep_count, 'prior timestep(s)')
                    # Read in whatever is available
                    for past_solution_index in range(-available_past_timestep_count,0):
                        timestep_string = str(timestep_index+past_solution_index)
                        time_deque[past_solution_index] = self.hdf_file_grid[0]['solution_'+timestep_string].attrs['time']
                        for grid_number in range(self.grid_count):
                            grid = self.grid[grid_number]
                            grid.field[past_solution_index][:,:] = self.hdf_file_grid[grid_number]['solution_' + timestep_string]

                elif self.last_restored_timestep_index == timestep_index - 1:
                    # If the previous timestep was the one most recently restored, previous timesteps are already available, so just make room for the new timestep.
                    for grid in self.grid:
                        grid.field.rotate(-1)
                else:
                    # Read in past timestep values
                    for past_solution_index in range(-grid.stored_solution_count+1,0):
                        timestep_string = str(timestep_index+past_solution_index)
                        time_deque[past_solution_index] = self.hdf_file_grid[0]['solution_'+timestep_string].attrs['time']
                        for grid_number in range(self.grid_count):
                            grid = self.grid[grid_number]
                            grid.field[past_solution_index][:,:] = self.hdf_file_grid[grid_number]['solution_' + timestep_string]

            # Read in values for specified timestep
            timestep_string = str(timestep_index)
            time_deque[0] = self.hdf_file_grid[0]['solution_'+timestep_string].attrs['time']
            for grid_number in range(self.grid_count):
                grid = self.grid[grid_number]
                timestep_index_string = str(timestep_index)
                stored_grid = self.hdf_file_grid[grid_number]
                stored_solution = stored_grid['solution_' + timestep_index_string]
                grid.field[0][:,:] = stored_solution
                if timestep_index != 0:
                    for key in grid.output_fields.iterkeys():
                        grid.output_fields[key][:] = stored_grid[key+'_'+timestep_index_string]
        else:
            # Using pickle
            # Read timestep 0 information
            while True:
                time_deque.rotate(-1)
                try:
                    timestep, time_deque[0] = pickle.load(self.pickle_file)
                    for grid_index in range(self.grid_count):
                        grid = self.grid[grid_index]
                        grid.field.rotate(-1)
                        grid.field[0] = pickle.load(self.pickle_file)

                        if timestep != 0:
                            for name,data in grid.output_fields.iteritems():
                                grid.output_fields = pickle.load(self.pickle_file)
                    # Read until specified timestep is reached
                    if timestep == timestep_index:
                        break
                except:
                    print('ERROR: pygdh.Input.restore_from_timestep: specified timestep index does not exist in Pickle file')
                    raise

    def close(self):
        if self.use_HDF5:
            # This triggers an Exception in h5py when running under Python 3,
            # but that is unlikely to create problems for most users
            self.hdf_file.close()
        else:
            self.pickle_file.close()
