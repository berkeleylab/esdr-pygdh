# cython: profile=False
from __future__ import print_function
from __future__ import absolute_import

from . import unit
from . cimport unit
import numpy
cimport numpy
import math
from . import interpolant
from . cimport interpolant

cdef class Volume(unit.Unit):
    """Defines objects that describe the finite volumes making up the problem
    domain"""

    def __init__(self,grid,number,index,coordinate):
        """Parameters:
        grid: 'Grid' object to which the new 'Volume' object will belong

        number: unique integer label identifying the new 'Volume' object within
          the containing 'Grid' object

        index: integer coordinate(s) of the grid point indicating the position
          of the new 'Volume' object in the rectangular grid defined in the
          containing 'Grid' object

        coordinate: coordinate(s) of the grid point indicating the position of
          the new 'Volume' object in the coordinate space defined by the
          containing 'Grid' object"""

        # Pass all parameters to the base class __init__ method
        unit.Unit.__init__(self,grid,number,index,coordinate)

        # This will hold the 'volume' associated with this object
        self.volume = 0.0

        self.interior_neighbor = None
        
        if self.grid.dimension_count == 1:

            # These will hold the distances associated with this object
            self.length = 0
            self.distance_to_boundary = numpy.empty(3)

            # This is an alias for the 'Interpolant'-generating method
            self.default_interpolant = self.default_1D_interpolant

        elif self.grid.dimension_count == 2:

            # These will hold the distances associated with this object
            self.length = [0,0]
            self.distance_to_boundary = numpy.empty((3,3))

            # This is an alias for the 'Interpolant'-generating method
            self.default_interpolant = self.default_2D_interpolant

    ## Returns an integer indicating the sign of the parameter
    def sign(self,number):

        if number > 0:
            return 1
        elif number < 0:
            return -1
        else:
            return 0

    # Calculate and store distances from the grid point associated with the
    # present 'Volume' to the boundaries associated with the 'Volume'
    def calculate_boundary_distances(self):
        
        if self.grid.dimension_count == 1:
            
            if self.number == 0:
                # Left edge of domain
                self.distance_to_boundary[-1] = 0
            else:
                # Boundary is midway between adjacent grid points
                self.distance_to_boundary[-1] = 0.5*(
                    self.coordinate
                    - self.grid.coordinates_list[0][self.index[0]-1])

            if self.number == self.grid.unit_count-1:
                # Right edge of domain
                self.distance_to_boundary[1] = 0
            else:
                # Boundary is midway between adjacent grid points
                self.distance_to_boundary[1] = 0.5*(
                    self.grid.coordinates_list[0][self.index[0]+1]
                    - self.coordinate)

        elif self.grid.dimension_count == 2:
            
            if (self.index[0] == 0
                or self.grid.unit_with_index[self.index[0]-1,
                                             self.index[1]] == None):
                # No adjacent 'Volume' left of present 'Volume'
                self.distance_to_boundary[-1,0] = 0
            else:
                # Boundary is midway between adjacent grid points
                self.distance_to_boundary[-1,0] = 0.5*(
                    self.coordinate[0]
                    - self.grid.coordinates_list[0][self.index[0]-1])
                
            if (self.index[0] == self.grid.index_bounds[0] - 1
                or self.grid.unit_with_index[self.index[0]+1,
                                             self.index[1]] == None):
                # No adjacent 'Volume' right of present 'Volume'
                self.distance_to_boundary[1,0] = 0
            else:
                 # Boundary is midway between adjacent grid points
               self.distance_to_boundary[1,0] = 0.5*(
                    self.grid.coordinates_list[0][self.index[0]+1]
                    - self.coordinate[0])

            if (self.index[1] == 0
                or self.grid.unit_with_index[self.index[0],
                                             self.index[1]-1] == None):
                # No adjacent 'Volume' below present 'Volume'
                self.distance_to_boundary[0,-1] = 0
            else:
                # Boundary is midway between adjacent grid points
                self.distance_to_boundary[0,-1] = 0.5*(
                    self.coordinate[1]
                    - self.grid.coordinates_list[1][self.index[1]-1])
                
            if (self.index[1] == self.grid.index_bounds[1] - 1
                or self.grid.unit_with_index[self.index[0],
                                             self.index[1]+1] == None):
                # No adjacent 'Volume' above present 'Volume'
                self.distance_to_boundary[0,1] = 0
            else:
                # Boundary is midway between adjacent grid points
                self.distance_to_boundary[0,1] = 0.5*(
                    self.grid.coordinates_list[1][self.index[1]+1]
                    - self.coordinate[1])

    def calculate_volume_dimensions(self):

        if self.grid.dimension_count == 1:

            # In one dimension, the length of the 'Volume' along the axis and
            # and the "volume" of the 'Volume' are given by the same scalar
            # value
            self.length = (self.distance_to_boundary[1]
                           + self.distance_to_boundary[-1])
            self.volume = self.length

        elif self.grid.dimension_count == 2:

            # In two dimensions, the lengths of the 'Volume' taken along the
            # two axes are stored in a 2-vector, and the 'volume' is the area
            # obtained as the product of these lengths
            self.length = numpy.array(
                (self.distance_to_boundary[1,0]
                 + self.distance_to_boundary[-1,0],
                 self.distance_to_boundary[0,1]
                 + self.distance_to_boundary[0,-1]))
            self.volume = numpy.prod(self.length)

    ## Returns an integer giving a suitable starting position along an axis
    ## for a contiguous selection of 'Volume' elements needed for an
    ## interpolation formula
    def start_index(self,axis_number,length_in_elements,signs):
        
        ## Ensure that the domain has enough divisions for the requested
        ## interpolant accuracy

        try:
            assert (length_in_elements
                    <= self.grid.index_bounds[axis_number] + 1)
        except:
            print('ERROR: Volume.start_index: "Grid" with label', self.grid.number, 'is too small for interpolant accuracy requested for "Volume" at', self.index)
            raise AssertionError

        if (self.neighbor_count == 3
            or ((self.neighbor_count == 5 or self.neighbor_count == 6)
                and self.interior_direction_relative_index[axis_number] != 0)):

            # Corner volume and edge 'Volume', when the grid of data must be
            # shifted toward the interior

            if self.interior_direction_relative_index[axis_number] == 1:
                start = self.index[axis_number]
            elif self.interior_direction_relative_index[axis_number] == -1:
                start = self.index[axis_number] - length_in_elements + 1
                
        elif self.neighbor_count == 7:

            # "Internal corner" 'Volume'
            
            if self.interior_direction_relative_index[axis_number] == 1:
                start = (self.index[axis_number] 
                         + signs[axis_number]*(length_in_elements >> 1))
            else:
                start = (self.index[axis_number] - length_in_elements #+ 1
                         + signs[axis_number]*(length_in_elements >> 1))
        else:
            # This catches all 1D cases as well as interior elements, and edge
            # elements in the unconstrained direction
            
            if length_in_elements % 2 == 0:

                # When selecting an even number of elements, when approximately
                # centered around the current volume, there will be one more
                # "row" of elements on one side than the other
                start = self.index[axis_number] - (length_in_elements >> 1) + 1

                # Put the additional "row" on the side on which the interpolant
                # will be evaluated
                if signs[axis_number] < 0:
                    start -= 1
            else:
                # When selecting an odd number of elements, attempt to center
                # the selections around the current volume

                start = self.index[axis_number] - ((length_in_elements - 1) >> 1)

            ## If the starting position would place some selected elements
            ## outside of the domain, shift the position to move all elements
            ## inside

            if start < 0:
                start = 0
            elif (start + length_in_elements
                  > self.grid.index_bounds[axis_number]):
                start = (self.grid.index_bounds[axis_number]
                         - length_in_elements) #+ 1)

        return start

    def default_1D_interpolant(self,normalized_position,
                               output_derivative_order,
                               maximum_derivative_order):
        """Parameters:

        normalized_position: position at which returned object will interpolate
          values, relative to position of present 'Volume', and divided by
          distance from 'Volume' position to relevant 'Volume' boundary

        output_derivative_order: order of derivative that returned
          'Interpolant' object should compute
        
        maximum_derivative_order: determines Interpolant accuracy by indicating
          highest derivative order that can be computed by interpolating
          polynomial

        Returns 'Interpolant' object that approximates specified derivatives
          given function value data"""

        assert self.grid.dimension_count == 1, 'ERROR: Volume.default_1D_interpolant:This is for 1D domains'

        ## Ensure that normalized_position is a scalar

        if (isinstance(normalized_position,float)
            or isinstance(normalized_position,int)):
            # This is to avoid calling 'len' on a scalar
            pass
        else:
            assert len(normalized_position) == 1, 'ERROR: Volume.default_1D_interpolant: Expected scalar or sequence of one scalar as position.'
            normalized_position = normalized_position[0]
        
        if isinstance(output_derivative_order,int):
            # This is to avoid calling 'len' on a scalar
            pass
        else:
            assert len(output_derivative_order) == 1, 'ERROR: Volume.default_1D_interpolant: Expected scalar or sequence of one scalar as order of resulting derivative.'
            output_derivative_order = output_derivative_order[0]

        if isinstance(maximum_derivative_order,int):
            # This is to avoid calling 'len' on a scalar
            pass
        else:
            assert len(maximum_derivative_order) == 1, 'ERROR: Volume.default_1D_interpolant: Expected scalar or sequence of one scalar as maximum order of resulting derivative.'
            maximum_derivative_order = maximum_derivative_order[0]

        assert output_derivative_order <= maximum_derivative_order, 'ERROR: Volume.default_1D_interpolant: "output_derivative_order" must not be larger than "maximum_derivative_order"'

        position_sign = self.sign(normalized_position)

        if (position_sign != 0
            and position_sign == -self.interior_direction_relative_index):
            # For the volumes adjacent to the domain boundaries, all
            # 'normalized_position' values with signs that do not point toward
            # the interior are taken to indicate the position of the domain
            # boundary, which is at 'relative_position' 0 for volumes adjacent
            # to the domain boundaries.
            relative_position = 0.0
        else:
            # Otherwise, the 'relative_position' is the 'normalized_position'
            # multiplied by the distance to the volume boundary in the
            # direction indicated by the sign of 'normalized_position'
            relative_position = (self.distance_to_boundary[position_sign]
                                 * normalized_position)

        # This is the number of locations from which data will be drawn in the
        # interpolation formula
        element_count = maximum_derivative_order + 1

        ## Determine the numerical labels of the 'Volume' objects to be used by
        ## the interpolant

        # 1D case, so direction index is 0
        position_signs = (position_sign,)
        start = self.start_index(0,element_count,position_signs)
        end_plus_one = start + element_count

        volume_list = [ self.grid.unit_with_number[number]
                        for number in range(start,end_plus_one) ]

        # In the 1D case, the highest exponent considered in the interpolation
        # calculations is the same as the number of times that a polynomial
        # interpolating the data values can be differentiated generally without
        # yielding zero everywhere

        return interpolant.Interpolant(self.coordinate + relative_position,
                                       output_derivative_order,
                                       maximum_derivative_order,volume_list)

    def element_signs(self,vector):

        element_count = len(vector)
        signs = [0 for element in vector]

        # While I could reuse self.sign, this is slightly more efficient as the
        # vector values are already 0
        for direction in range(element_count):
            if vector[direction] > 0:
                signs[direction] = 1
            elif vector[direction] < 0:
                signs[direction] = -1

        return signs

    def default_2D_interpolant(self,modified_normalized_position,
                               output_derivative_orders,
                               maximum_derivative_orders):
        """Parameters:

        normalized_position: position at which returned object will interpolate
          values, relative to position of present 'Volume', and divided by
          distance from 'Volume' position to relevant 'Volume' boundary

        output_derivative_order: order of derivative that returned
          'Interpolant' object should compute
        
        maximum_derivative_order: determines Interpolant accuracy by indicating
          highest derivative order that can be computed by interpolating
          polynomial

        Returns 'Interpolant' object that approximates specified derivatives
          given function value data"""
        
        assert self.grid.dimension_count == 2, 'ERROR: Volume.default_2D_interpolant: This is for 2D domains'
        assert len(modified_normalized_position) == 2, 'ERROR: Volume.default_2D_interpolant: Expected sequence of two scalars as "modified_normalized_position"'
        assert len(output_derivative_orders) == 2, 'ERROR: Volume.default_2D_interpolant: Expected sequence of two integers as "output_derivative_orders"'
        assert len(maximum_derivative_orders) == 2, 'ERROR: Volume.default_2D_interpolant: Expected sequence of two integers as "maximum_derivative_orders"'        
        for element_number in range(self.grid.dimension_count):
            assert (isinstance(modified_normalized_position[element_number],float) or isinstance(modified_normalized_position[element_number],int)), 'ERROR: Volume.default_2D_interpolant: Expected sequence of two scalars as "modified_normalized_position"'
            assert isinstance(output_derivative_orders[element_number],int), 'ERROR: Volume.default_2D_interpolant: Expected sequence of two scalars as "output_derivative_orders"'            
            assert isinstance(maximum_derivative_orders[element_number],int), 'ERROR: Volume.default_2D_interpolant: Expected sequence of two scalars as "maximum_derivative_orders"'
            assert maximum_derivative_orders[element_number] >= output_derivative_orders[element_number], 'ERROR: Volume.default_2D_interpolant: elements of "output_derivative_orders" must be smaller than corresponding elements of "maximum_derivative_orders"'
        
        length_in_elements = [ maximum_derivative_order + 1
                               for maximum_derivative_order
                               in maximum_derivative_orders ]

        relative_position = self.relative_position_with_local_position(modified_normalized_position)
        
        # This is a list of integers indicating the signs of the corresponding
        # elements of the normalized_position vector

        signs = self.element_signs(relative_position)

        ## This selects compatible groups of exponents and 'Volume' objects,
        ## with the 'Volumes' having a rectangular arrangement. The number of
        ## objects along each axis is determined by the entries in
        ## 'length_in_elements'

        exponents_sequence = []
        for i in range(length_in_elements[0]):
            for j in range(length_in_elements[1]):
                exponents_sequence.append((i,j))
                
        startx = self.start_index(0,length_in_elements[0],signs)
        starty = self.start_index(1,length_in_elements[1],signs)
        endx_plus_one = startx + length_in_elements[0]
        endy_plus_one = starty + length_in_elements[1]

        volume_list = []
        for i in range(startx,endx_plus_one):
            for j in range(starty,endy_plus_one):
                candidate_volume = self.grid.unit_with_index[i,j]
                if isinstance(candidate_volume,Volume):
                    volume_list.append(candidate_volume)
                else:
                    print('ERROR: parameters_for_interpolation_on_rectangle: Reducing interpolant order for element with index', self.index, 'on Grid', self.grid.name, 'due to limited number of neighboring elements')
                    exponents_sequence.pop()

        return interpolant.Interpolant(self.coordinate + relative_position,
                                       output_derivative_orders,
                                       exponents_sequence,volume_list)

    def relative_position_with_local_position(self,local_position):
        """Parameter:
        local_position: in 1D, a scalar, and in 2D, a sequence of two scalars,
          describing a position in the convenient local coordinate system
          defined for the present 'Volume'.

        Returns a scalar in 1D, or a sequence of two scalars in 2D, which 
          give the position relative to the grid point of the present 'Volume'
          of 'local_position', using the scale of the coordinate system used
          by the containing 'Grid' object"""

        if self.grid.dimension_count == 1:
            
            return (local_position
                    * self.distance_to_boundary[self.sign(local_position)])
        
        elif self.grid.dimension_count == 2:

            if self.neighbor_count == 8 or self.neighbor_count == 7:
                # In the interior of the domain, or at an interior corner,
                # -1 and +1 correspond to the extreme opposite sides of a
                # 'Volume', in both directions
                normalized_position = local_position

            else:
                # At an edge or corner of the domain, the origin of the
                # 'local_position' is at the center of the 'Volume' in
                # normalized coordinates, and -1 and +1 correspond 
                # to opposite sides of the 'Volume', in both directions

                # Create a list with the same number of elements
                normalized_position = list(local_position)

                for dimension in range(self.grid.dimension_count):
                    if self.interior_direction_relative_index[dimension] != 0:
                        normalized_position[dimension] = 0.5*(
                            local_position[dimension]
                            + float(
                                self.interior_direction_relative_index[
                                    dimension]))

            # This is a list of integers indicating the signs of the
            # corresponding elements of the normalized_position vector

            #signs = self.element_signs(normalized_position)

            return numpy.array(
                (self.distance_to_boundary[self.sign(normalized_position[0]),0]
                 * normalized_position[0],
                 + self.distance_to_boundary[0,
                                             self.sign(normalized_position[1])]
                 * normalized_position[1]))

