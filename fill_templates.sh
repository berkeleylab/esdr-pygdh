#!/bin/sh
VERSION=`cat version.txt`
YEARS=`cat years.txt`
sed "s/VERSION/${VERSION}/" Makefile_template > Makefile
sed "s/VERSION/${VERSION}/" setup_template.py > setup.py 
sed "s/VERSION/${VERSION}/" cython_setup_template.py > cython_setup.py
sed "s/YEARS/${YEARS}/" license_template.txt > LICENSE
sed "s/YEARS/${YEARS}/" copyright_notice_template.txt > copyright_notice.txt
