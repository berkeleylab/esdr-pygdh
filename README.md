# README #

### What is this repository for? ###

* PyGDH is a Python 2 and Python 3 library that provides a highly flexible approach to expressing and solving optionally time-dependent equations by the finite volume method on one-dimensional and simple two-dimensional spatial domains.
* PyGDH is licensed under the [BSD-3-Clause-LBNL](https://opensource.org/BSD-3-Clause-LBNL) license, which is recognized by the Open Source Initiative (OSI).
* Official release packages and documentation are available on the [Downloads page](https://bitbucket.org/berkeleylab/esdr-pygdh/downloads/).
* When there is a file in the archive ending with `_template.py` or `_annotated.py`, please edit those files rather than those with similar names but ending only in `.py`, as the latter are automatically generated from the former.

### How do I get set up? ###

* Python 2 or Python 3, NumPy, and SciPy are required.
* Cython (along with a C compiler), GNUPLOT, matplotlib, and h5py / HDF5 are recommended and required to run all tests.
* Sphinx and some variant of Make are needed to build the documentation and Python packages.
* The release packages on the [Downloads page](https://bitbucket.org/berkeleylab/esdr-pygdh/downloads/) should be suitable for most users.

    * Detailed installation instructions for PyGDH are provided in the documentation. Briefly, the archives are built using the standard Python packaging system, so if PyGDH has been downloaded as an archive, one should first unpack the archive and run a command such as `python setup.py install`, and additionally `python cython_setup.py install` to generate the faster Cythonized version. Note that the Python 3 executable might be named `python3` on systems which use Python 2 by default; if you wish to use PyGDH with both Python 2 and Python 3, you will need to run the setup processes with both versions.

* If you prefer to work with the repository, run `git clone https://bitbucket.org/berkeleylab/esdr-pygdh.git` to clone the repository (or `git clone git@bitbucket.org:berkeleylab/esdr-pygdh.git` if you have write access to the repository). Please edit ``python_executable.txt`` if necessary so that it contains only the name of your prefered Python executable program; this will be used to run Sphinx.

    * Since the Sphinx `conf.py` is technically source code that we did not write, we are not distributing it to avoid licensing complications. If you wish to build the documentation, change to the `doc` subdirectory and run `sphinx-quickstart`, saying yes to separating source and build directories, using `PyGDH` for project name, `OWNER` for author name, `VERSION` for project release, naming the master document `DELETE` to avoid overwriting the file in the repository, and saying yes to either imgmath or mathjax. Then run the `sphinx_setup.sh` script (`./sphinx_setup.sh`).
    * Run the `fill_templates.sh` script in the root directory of the repository generates a `Makefile` as well as the Python setup scripts, which may then be used to install PyGDH. Edit `Makefile` to set `python_packaging_executable` to your preferred Python executable program for installing the PyGDH package and to remove references to the Python 2 or Python 3 if you only need to package one version. Then run `make` to build and install packages. Alternatively, you can run the `setup.py` and `cython_setup.py` scripts directly, as described earlier. Note that the automated tests fail when the Python 3 build happens before the Python 2 build for a reason that we don't yet understand; the process followed by the Makefile avoids this issue.
    * Then in the `pygdh` subdirectory, run the examples / tests with the `tests.py` program. Finally, in the `doc` subdirectory, the `make` command can be used to produce different output formats as described by the Makefile (for instance, run `make html` for HTML output).

### Contribution guidelines ###

* Please write to Kenneth Higa (khiga@lbl.gov) if you would like to contribute code to PyGDH, preferably before writing any code.
* If you wish to modify the PyGDH Python code and keep the corresponding Cython code up-to-date, run ``make freeze_py`` in the root directory of the archive, edit the Python files in the ``pygdh`` subdirectory, then return to the root directory and run ``make thaw_pyx`` to update the ``pyx`` files.
* Please use the Bitbucket [issue tracker](https://bitbucket.org/berkeleylab/esdr-pygdh/issues?status=new&status=open) to report problems.
* Unfortunately, we do not have the capacity to provide support in general, although we might make exceptions on a case-by-case basis.

### Citing ###

Please reference the accompanying paper, with [![DOI](https://joss.theoj.org/papers/10.21105/joss.02744/status.svg)](https://doi.org/10.21105/joss.02744).
